package cbr.geometry;

import cbr.IDisposable;
import cbr.serialize.xml.annotations.XMLNode;
import cbr.serialize.xml.annotations.XMLTransient;

@XMLNode("rectangle")
@XMLTransient({ "x", "y", "width", "height" })
public interface IRectangle extends IPoint, IDimension, ISizeChanged,
		ILocationChanged, IDisposable {

	public boolean isContains(IPoint point);

	public boolean isContains(int x, int y);

	public IPoint getCenter();

	@Override
	public IRectangle copy();

}
