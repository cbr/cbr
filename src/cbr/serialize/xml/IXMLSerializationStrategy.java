package cbr.serialize.xml;

import java.util.Map;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

public interface IXMLSerializationStrategy {

	public Element serialize(Document document, String name,
			String parentFieldName, Map<String, String> attributes);

	public XMLSerializableObject createXSObject(Node node);

	public String getFieldName(Node node);

	public Object parseObjectFromString(Class<?> type, String string);

	public Element getAppendToElement(Element element, String name,
			String parentFieldName);

}
