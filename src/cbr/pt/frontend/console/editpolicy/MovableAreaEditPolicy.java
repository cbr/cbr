package cbr.pt.frontend.console.editpolicy;

import cbr.geometry.IPoint;
import cbr.geometry.IRectangle;
import cbr.pt.frontend.command.CommandsFactory;
import cbr.pt.frontend.console.IAreaView;
import cbr.pt.frontend.console.service.MoveHandles;

public class MovableAreaEditPolicy extends
		DragSupportReactivatedCreationEditPolicy {

	public static final String EDIT_POLICY_MOVE = "Movable area edit policy";

	public MovableAreaEditPolicy() {
		super(EDIT_POLICY_MOVE);
	}

	@Override
	protected void tryToApplyServiceView(IPoint delta) {
		applyDelta(getServiceView().getBounds(), delta);
	}

	@Override
	protected void tryToCommit(IPoint delta) {
		IAreaView host = getHost();
		IRectangle bounds = host.getBounds().copy();
		IAreaView parent = host.getParent();
		if (parent != null)
			parent.translateToAbsolute(bounds);
		applyDelta(bounds, delta);
		if (parent != null)
			parent.translateToRelative(bounds);
		commit(bounds);
	}

	protected void commit(IRectangle bounds) {
		CommandsFactory.createAndExecute("SetConstraintCommand", getContext(),
				"Resize/move", getHost(), bounds);
	}

	@Override
	public IAreaView createServiceView() {
		return new MoveHandles();
	}

	protected void applyDelta(IRectangle bounds, IPoint delta) {
		bounds.setX(bounds.getX() + delta.getX());
		bounds.setY(bounds.getY() + delta.getY());
	}

}
