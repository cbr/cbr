package cbr.pt.frontend.console.editpolicy;

import cbr.Compass;
import cbr.geometry.IPoint;
import cbr.geometry.IRectangle;
import cbr.pt.frontend.command.CommandsFactory;
import cbr.pt.frontend.console.IAreaView;
import cbr.pt.frontend.console.IFeatureLocator;
import cbr.pt.frontend.console.IFeatureLocatorProvider;
import cbr.pt.frontend.console.IFeatureView;

public class MovableFeatureEditPolicy extends MovableAreaEditPolicy {

	private int side = Compass.UNKNOWN;

	protected void commit(IRectangle bounds) {
		CommandsFactory.createAndExecute("SetFeatureConstraintCommand",
				getContext(), "Resize/move", getHost(), bounds, side);
	}

	@Override
	protected void applyDelta(IRectangle bounds, IPoint delta) {
		super.applyDelta(bounds, delta);
		IAreaView host = getHost();
		host.translateToCenter(bounds.getLocation());
		IAreaView parentOfHost = host.getParent();
		if (parentOfHost instanceof IFeatureLocatorProvider) {
			IFeatureLocator locator = ((IFeatureLocatorProvider) parentOfHost)
					.getLocator(((IFeatureView) host).getClass());
			if (locator != null) {
				side = locator.locate(bounds.getLocation(), Compass.UNKNOWN);
			}
		}
		host.translateFromCenter(bounds.getLocation());
	}

}
