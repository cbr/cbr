package cbr.pt.frontend.tests;

import java.io.IOException;
import java.io.StringReader;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import cbr.FileUtils;
import cbr.pt.frontend.command.CommandsFactory;
import cbr.pt.frontend.command.CompoundCommand;
import cbr.pt.frontend.command.ICommand;
import cbr.pt.frontend.console.CBRContext;

public class TestUtils {

	public final static String EL_MODEL = "model";

	public final static String EL_COMMANDS = "commands";

	public final static String EL_COMMAND = "command";

	public final static String EL_NAME = "name";

	public static CommandTest loadTest(CBRContext context, String path,
			String modelPath) {
		try {
			String content = FileUtils.readAllFromTextFile(path);

			DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory
					.newInstance();
			DocumentBuilder documentBuilder = documentBuilderFactory
					.newDocumentBuilder();
			Document document = documentBuilder.parse(new InputSource(
					new StringReader(content)));

			Node rootNode = document.getDocumentElement();

			CompoundCommand compoundCommand = (CompoundCommand) CommandsFactory
					.createCommand("CompoundCommand", context, "Test");
			CommandTest commandTest = new CommandTest(context, compoundCommand,
					modelPath);

			if (rootNode.getNodeType() == Node.ELEMENT_NODE) {
				NodeList nodeList = rootNode.getChildNodes();
				for (int i = 0; i < nodeList.getLength(); i++) {
					Node childNode = nodeList.item(i);
					if (childNode.getNodeType() == Node.ELEMENT_NODE) {
						String nodeName = childNode.getNodeName();
						if (nodeName.equalsIgnoreCase(EL_COMMANDS)) {
							NodeList nodes = childNode.getChildNodes();
							for (int j = 0; j < nodes.getLength(); j++) {
								Node childChildNode = nodes.item(j);
								if (childChildNode.getNodeType() == Node.ELEMENT_NODE) {
									String childChildNodeName = childChildNode
											.getNodeName();
									if (childChildNodeName.equals(EL_COMMAND)) {
										String name = null;
										NamedNodeMap namedNodeMap = childChildNode
												.getAttributes();
										for (int k = 0; k < namedNodeMap
												.getLength(); k++) {
											Node attrNode = namedNodeMap
													.item(k);
											if (attrNode.getNodeType() == Node.ATTRIBUTE_NODE) {
												String attrNodeName = attrNode
														.getNodeName();
												if (attrNodeName
														.equals(EL_NAME)) {
													name = attrNode
															.getNodeValue();
												}
											}
										}
										if (name == null) {
											throw new UnsupportedOperationException(
													"Can't understand command with empty name!");
										} else {
											ICommand localCommand = CommandsFactory
													.parseCommand(name,
															context,
															childChildNode);
											if (localCommand != null)
												compoundCommand
														.addCommand(localCommand);
											else
												throw new UnsupportedOperationException(
														"Command: \""
																+ name
																+ "\" - Unimplemented or not supported yet!");
										}
									}
								}
							}
						} else if (nodeName.equalsIgnoreCase(EL_MODEL)) {
							commandTest.setModelPath(childNode.getFirstChild()
									.getNodeValue());
						}
					}
				}
			}
			return commandTest;
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		}
		return null;
	}

}
