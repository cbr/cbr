package cbr.pt.frontend.command;

import cbr.pt.frontend.console.CBRContext;
import cbr.pt.frontend.console.IAreaView;

public abstract class AbstractCommand implements ICommand {

	private String description;

	private CBRContext context;

	protected AbstractCommand(CBRContext context, String description) {
		this.description = description;
		this.context = context;
	}

	@Override
	public String getDescription() {
		return description;
	}

	@Override
	public abstract void execute();

	@Override
	public void redo() {
		execute();
	}

	@Override
	public abstract void undo();

	@Override
	public boolean canUndo() {
		return true;
	}

	@Override
	public boolean canExecute() {
		return true;
	}

	@Override
	public void dispose() {
		context = null;
	}

	public CBRContext getContext() {
		return context;
	}

	public void setContext(CBRContext context) {
		this.context = context;
	}

	@Override
	public void disconnectFromContext() {
		setContext(null);
	}

	@Override
	public void connectToContext(CBRContext context) {
		setContext(context);
	}

	public IAreaView findModelById(int id) {
		if (id == UNDEFINED)
			throw new UnsupportedOperationException("Model id is UNDEFINED!");
		else {
			IAreaView model = getContext().findModelById(id);
			if (model == null)
				throw new UnsupportedOperationException(
						"Can't find model by id: " + id);
			return model;
		}
	}

}
