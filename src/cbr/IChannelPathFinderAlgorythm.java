package cbr;

import cbr.channels.IChannel;

public interface IChannelPathFinderAlgorythm {

	abstract public void process(IChannel channel);

}
