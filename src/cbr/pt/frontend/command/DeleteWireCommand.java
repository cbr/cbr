package cbr.pt.frontend.command;

import cbr.geometry.IPoint;
import cbr.pt.frontend.console.CBRContext;
import cbr.pt.frontend.console.IAreaView;
import cbr.pt.frontend.console.IConnector;
import cbr.pt.frontend.console.IIdentifiedAnchor;
import cbr.pt.frontend.console.IWireView;

public class DeleteWireCommand extends AbstractCommand {

	private IWireView wire;

	private IAreaView parent;

	private IIdentifiedAnchor sourceAnchor;

	private IIdentifiedAnchor targetAnchor;

	private IPoint sourceLocation;

	private IPoint targetLocation;

	private int sourceSide;

	private int targetSide;

	private int wireId = UNDEFINED;

	protected DeleteWireCommand(CBRContext context, String description,
			IWireView wire) {
		super(context, description);
		this.wire = wire;
		fillAnchorsFields();
		sourceLocation = wire.getSourceConnector().getLocation().copy();
		targetLocation = wire.getTargetConnector().getLocation().copy();
		sourceSide = wire.getSourceConnector().getSide();
		targetSide = wire.getTargetConnector().getSide();
	}

	private void fillAnchorsFields() {
		sourceAnchor = wire.getSourceConnector().getAnchor();
		targetAnchor = wire.getTargetConnector().getAnchor();
	}

	@Override
	public void execute() {
		wire.getSourceConnector().disconnect();
		wire.getTargetConnector().disconnect();
		parent = wire.getParent();
		parent.removeChild(wire);
	}

	@Override
	public void undo() {
		IConnector sourceConnector = wire.getSourceConnector();
		IConnector targetConnector = wire.getTargetConnector();
		sourceConnector.setSide(sourceSide);
		targetConnector.setSide(targetSide);
		sourceConnector.setLocation(sourceLocation.copy());
		targetConnector.setLocation(targetLocation.copy());
		sourceConnector.connect(sourceAnchor);
		targetConnector.connect(targetAnchor);
		parent.addChild(wire);
		wire.update();
	}

	@Override
	public void disconnectFromContext() {
		wireId = wire.getId();
		super.disconnectFromContext();
	}

	@Override
	public void connectToContext(CBRContext context) {
		super.connectToContext(context);
		if (wireId == UNDEFINED)
			throw new UnsupportedOperationException(
					"Errors due to context connection. Undefined view id for DeleteWireCommand.");
		else {
			wire = (IWireView) findModelById(wireId);
			if (wire != null) {
				wireId = UNDEFINED;
				fillAnchorsFields();
			}
		}
	}

}
