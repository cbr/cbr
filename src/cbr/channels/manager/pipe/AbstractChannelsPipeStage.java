package cbr.channels.manager.pipe;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import cbr.channels.IChannel;

/**
 * 
 * Стадия конвеера. Для каждого канала выполняется processChannel.<br>
 * После этого канал удаляется из списка.<br>
 * Если есть следующая стадии, то обработанный канал добавляется в список<br>
 * следующей стадии.
 * 
 * @author Alexander Strakh
 * 
 */
public abstract class AbstractChannelsPipeStage {

	protected List<IChannel> channels = new ArrayList<IChannel>();

	protected AbstractChannelsPipeStage next;

	public AbstractChannelsPipeStage() {
		this(null);
	}

	public AbstractChannelsPipeStage(AbstractChannelsPipeStage next) {
		this.next = next;
	}

	public void add(IChannel channel) {
		if (!channels.contains(channel)) {
			channels.add(channel);
		}
	}

	public void process() {
		Iterator<IChannel> channelsIter = channels.iterator();
		while (channelsIter.hasNext()) {
			IChannel channel = channelsIter.next();
			processChannel(channel);
			channelsIter.remove();
			if (next != null) {
				next.add(channel);
			}
		}
	}

	public void clean() {
		channels.clear();
	}

	public void removeChannelFromQueue(IChannel channel) {
		channels.remove(channel);
	}

	abstract protected void processChannel(IChannel channel);

}
