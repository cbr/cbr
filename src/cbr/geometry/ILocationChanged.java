package cbr.geometry;

import cbr.serialize.xml.annotations.XMLElement;
import cbr.serialize.xml.annotations.XMLNode;

@XMLNode("locationChanged")
public interface ILocationChanged {

	@XMLElement
	public IPoint getLocation();

	public void setLocation(IPoint location);

}
