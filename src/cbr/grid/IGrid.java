package cbr.grid;

import cbr.channels.manager.AbstractChannelsManager;

/**
 * 
 * Grid
 * 
 * @author Alexander Strakh
 * 
 */
public interface IGrid<T, I> {

	public int getColumnsCount();

	public int getRowsCount();

	public void setColumnsCount(int columnsCount);

	public void setRowsCount(int rowsCount);

	public void setColumnsAndRowsCount(int columnsCount, int rowsCount);

	public AbstractChannelsManager<T, I> getChannelsManager();

	public void setChannelsManager(AbstractChannelsManager<T, I> channelsManager);

	/**
	 * 
	 * Возвращает ячейку на пересечении строки row и столбца column.<br>
	 * 
	 * @param columnNumber
	 *            - номер столбца
	 * @param rowNumber
	 *            - номер строки
	 * @return ячейка {@link ICell} из пересечения строки номер rowNumber<br>
	 *         с столбцом номер columnNumber. Если columnNumber или rowNumber<br>
	 *         выходят за границы сетки, то возвращается null.
	 */
	public ICell getCell(int columnNumber, int rowNumber);

	public int[] getColumnRow(ICell cell);

	public boolean setCell(int columnNumber, int rowNumber, ICell cell);

}
