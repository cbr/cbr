package cbr.geometry;

import cbr.IDisposable;
import cbr.serialize.xml.annotations.XMLElement;
import cbr.serialize.xml.annotations.XMLNode;
import cbr.serialize.xml.annotations.XMLTransient;

@XMLNode("bounded")
@XMLTransient({ "size", "location" })
public interface IBounded extends ISizeChanged, ILocationChanged, IDisposable {

	@XMLElement
	public IRectangle getBounds();

	public void setBounds(IRectangle bounds);

}
