package cbr.pt.frontend.command;

import cbr.IDisposable;
import cbr.pt.frontend.console.CBRContext;

public interface ICommand extends IDisposable {

	public static final int UNDEFINED = -1;

	public boolean canExecute();

	public boolean canUndo();

	public void undo();

	public void redo();

	public void execute();

	public String getDescription();

	public void disconnectFromContext();

	public void connectToContext(CBRContext context);

}
