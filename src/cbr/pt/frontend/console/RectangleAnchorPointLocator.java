package cbr.pt.frontend.console;

import cbr.Compass;
import cbr.geometry.IPoint;
import cbr.geometry.IRectangle;

public class RectangleAnchorPointLocator extends AbstractAnchorPointLocator {

	public RectangleAnchorPointLocator(IIdentifiedAnchorView owner) {
		super(owner);
	}

	@Override
	protected int processLocate(IRectangle bounds, IPoint location, int side) {
		if (side == Compass.UNKNOWN) {
			int[] det = new int[] {
					Compass.WEST,
					Math.abs(location.getX() - bounds.getX()),
					Compass.NORTH,
					Math.abs(location.getY() - bounds.getY()),
					Compass.EAST,
					Math.abs(location.getX() - bounds.getX()
							- bounds.getWidth()),
					Compass.SOUTH,
					Math.abs(location.getY() - bounds.getY()
							- bounds.getHeight()) };
			side = Compass.WEST;
			for (int i = 0; i < Compass.NORMAL_DIRECTIONS_COUNT; i++) {
				if (det[side * 2] > det[i * 2 + 1]) {
					side = det[i * 2];
				}
			}
			fixLocation(bounds, location, side);
		} else {
			fixLocation(bounds, location, side);
		}
		return side;
	}

	protected void fixLocation(IRectangle bounds, IPoint point, int side) {
		if (side == Compass.WEST) {
			if (point.getY() < bounds.getY()) {
				point.setY(bounds.getY());
			} else if (point.getY() >= bounds.getY() + bounds.getHeight()) {
				point.setY(bounds.getY() + bounds.getHeight() - 1);
			}
			point.setX(bounds.getX());
		} else if (side == Compass.EAST) {
			if (point.getY() < bounds.getY()) {
				point.setY(bounds.getY());
			} else if (point.getX() >= bounds.getY() + bounds.getHeight()) {
				point.setY(bounds.getY() + bounds.getHeight() - 1);
			}
			point.setX(bounds.getX() + bounds.getWidth() - 1);
		} else if (side == Compass.NORTH) {
			if (point.getX() < bounds.getX()) {
				point.setX(bounds.getX());
			} else if (point.getY() >= bounds.getX() + bounds.getWidth()) {
				point.setX(bounds.getX() + bounds.getWidth() - 1);
			}
			point.setY(bounds.getY());
		} else if (side == Compass.SOUTH) {
			if (point.getX() < bounds.getX()) {
				point.setX(bounds.getX());
			} else if (point.getY() >= bounds.getX() + bounds.getWidth()) {
				point.setX(bounds.getX() + bounds.getX() - 1);
			}
			point.setY(bounds.getY() + bounds.getHeight() - 1);
		}
	}

}
