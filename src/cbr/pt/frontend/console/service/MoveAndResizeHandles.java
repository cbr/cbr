package cbr.pt.frontend.console.service;

import java.awt.Color;

import cbr.Compass;
import cbr.geometry.Dimension;
import cbr.geometry.Geometry;
import cbr.geometry.IDimension;
import cbr.geometry.IPoint;
import cbr.pt.frontend.console.CBRGraphics;

public class MoveAndResizeHandles extends MoveHandles {

	private IDimension resizeToolSize = new Dimension(6, 6);

	@Override
	public void prePaint(CBRGraphics graphics) {
		super.prePaint(graphics);
		graphics.setColor(Color.red);
		paintResizeTool(graphics);
	}

	private boolean isSupportedResizeDirection(int direction) {
		return direction == Compass.WEST || direction == Compass.WEST_NORTH
				|| direction == Compass.NORTH
				|| direction == Compass.NORTH_EAST || direction == Compass.EAST
				|| direction == Compass.EAST_SOUTH
				|| direction == Compass.SOUTH
				|| direction == Compass.SOUTH_WEST;
	}

	private void paintResizeTool(CBRGraphics g) {
		if (isSupportedResizeDirection(Compass.WEST)) {
			g.fillRect(getLocation().getX(), getLocation().getY()
					+ getSize().getHeight() / 2 - resizeToolSize.getHeight()
					/ 2, resizeToolSize.getWidth(), resizeToolSize.getHeight());
		}
		if (isSupportedResizeDirection(Compass.WEST_NORTH)) {
			g.fillRect(getLocation().getX(), getLocation().getY(),
					resizeToolSize.getWidth(), resizeToolSize.getHeight());
		}
		if (isSupportedResizeDirection(Compass.NORTH)) {
			g.fillRect(getLocation().getX() + +getSize().getWidth() / 2
					- resizeToolSize.getWidth() / 2, getLocation().getY(),
					resizeToolSize.getWidth(), resizeToolSize.getHeight());
		}
		if (isSupportedResizeDirection(Compass.NORTH_EAST)) {
			g.fillRect(getLocation().getX() + getSize().getWidth()
					- resizeToolSize.getWidth(), getLocation().getY(),
					resizeToolSize.getWidth(), resizeToolSize.getHeight());
		}
		if (isSupportedResizeDirection(Compass.EAST)) {
			g.fillRect(getLocation().getX() + getSize().getWidth()
					- resizeToolSize.getWidth(), getLocation().getY()
					+ getSize().getHeight() / 2 - resizeToolSize.getHeight()
					/ 2, resizeToolSize.getWidth(), resizeToolSize.getHeight());
		}
		if (isSupportedResizeDirection(Compass.EAST_SOUTH)) {
			g.fillRect(getLocation().getX() + getSize().getWidth()
					- resizeToolSize.getWidth(), getLocation().getY()
					+ getSize().getHeight() - resizeToolSize.getHeight(),
					resizeToolSize.getWidth(), resizeToolSize.getHeight());
		}
		if (isSupportedResizeDirection(Compass.SOUTH)) {
			g.fillRect(getLocation().getX() + getSize().getWidth() / 2
					- resizeToolSize.getWidth() / 2, getLocation().getY()
					+ getSize().getHeight() - resizeToolSize.getHeight(),
					resizeToolSize.getWidth(), resizeToolSize.getHeight());
		}
		if (isSupportedResizeDirection(Compass.SOUTH_WEST)) {
			g.fillRect(getLocation().getX(), getLocation().getY()
					+ getSize().getHeight() - resizeToolSize.getHeight(),
					resizeToolSize.getWidth(), resizeToolSize.getHeight());
		}
	}

	public int getResizeSupportedDir(IPoint point) {
		int dir = getResizeToolDir(point);
		if (isSupportedResizeDirection(dir)) {
			return dir;
		}
		return Compass.UNKNOWN;
	}

	private int getResizeToolDir(IPoint point) {
		if (Geometry.isContains(point.getX(), point.getY(), getLocation()
				.getX(), getLocation().getY() + getSize().getHeight() / 2
				- resizeToolSize.getHeight() / 2, resizeToolSize.getWidth(),
				resizeToolSize.getHeight()))
			return Compass.WEST;
		if (Geometry.isContains(point.getX(), point.getY(), getLocation()
				.getX(), getLocation().getY(), resizeToolSize.getWidth(),
				resizeToolSize.getHeight()))
			return Compass.WEST_NORTH;
		if (Geometry.isContains(point.getX(), point.getY(), getLocation()
				.getX()
				+ +getSize().getWidth()
				/ 2
				- resizeToolSize.getWidth()
				/ 2, getLocation().getY(), resizeToolSize.getWidth(),
				resizeToolSize.getHeight()))
			return Compass.NORTH;
		if (Geometry.isContains(point.getX(), point.getY(), getLocation()
				.getX() + getSize().getWidth() - resizeToolSize.getWidth(),
				getLocation().getY(), resizeToolSize.getWidth(),
				resizeToolSize.getHeight()))
			return Compass.NORTH_EAST;
		if (Geometry.isContains(point.getX(), point.getY(), getLocation()
				.getX() + getSize().getWidth() - resizeToolSize.getWidth(),
				getLocation().getY() + getSize().getHeight() / 2
						- resizeToolSize.getHeight() / 2,
				resizeToolSize.getWidth(), resizeToolSize.getHeight()))
			return Compass.EAST;
		if (Geometry.isContains(
				point.getX(),
				point.getY(),
				getLocation().getX() + getSize().getWidth()
						- resizeToolSize.getWidth(),
				getLocation().getY() + getSize().getHeight()
						- resizeToolSize.getHeight(),
				resizeToolSize.getWidth(), resizeToolSize.getHeight()))
			return Compass.EAST_SOUTH;
		if (Geometry.isContains(point.getX(), point.getY(), getLocation()
				.getX()
				+ getSize().getWidth()
				/ 2
				- resizeToolSize.getWidth()
				/ 2, getLocation().getY() + getSize().getHeight()
				- resizeToolSize.getHeight(), resizeToolSize.getWidth(),
				resizeToolSize.getHeight()))
			return Compass.SOUTH;
		if (Geometry.isContains(point.getX(), point.getY(), getLocation()
				.getX(), getLocation().getY() + getSize().getHeight()
				- resizeToolSize.getHeight(), resizeToolSize.getWidth(),
				resizeToolSize.getHeight()))
			return Compass.SOUTH_WEST;
		return Compass.UNKNOWN;
	}

}
