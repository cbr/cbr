package cbr.pt.frontend.command;

import cbr.geometry.IBounded;
import cbr.geometry.IRectangle;
import cbr.pt.frontend.console.CBRContext;
import cbr.pt.frontend.console.IAreaView;
import cbr.pt.frontend.console.IIdentified;

public class SetConstraintCommand extends AbstractCommand {

	private IRectangle oldBounds;

	private IRectangle newBounds;

	private IBounded bounded;

	private int boundedId = UNDEFINED;

	protected SetConstraintCommand(CBRContext context, String description,
			IAreaView view, IRectangle bounds) {
		super(context, description);
		this.bounded = view;
		this.newBounds = bounds.copy();
		this.oldBounds = getOldBounds();
	}

	protected IBounded getBoundedView() {
		return bounded;
	}

	@Override
	public void execute() {
		bounded.setBounds(newBounds.copy());
	}

	private IRectangle getOldBounds() {
		if (oldBounds == null)
			oldBounds = bounded.getBounds().copy();
		return oldBounds;
	}

	@Override
	public void undo() {
		bounded.setBounds(getOldBounds().copy());
	}

	@Override
	public boolean canExecute() {
		return !getOldBounds().equals(newBounds);
	}

	@Override
	public void dispose() {
		bounded = null;
		newBounds.dispose();
		if (oldBounds != null) {
			oldBounds.dispose();
		}
	}

	@Override
	public void disconnectFromContext() {
		if (bounded instanceof IIdentified) {
			boundedId = ((IIdentified) bounded).getId();
		} else
			throw new UnsupportedOperationException(
					"Errors due to disconnect from context for SetConstraintCommand. Parent: \""
							+ bounded.getClass().getSimpleName()
							+ "\" should be implementation of IIdentified.");
		super.disconnectFromContext();
	}

	@Override
	public void connectToContext(CBRContext context) {
		super.connectToContext(context);
		bounded = (IBounded) findModelById(boundedId);
		if (bounded != null)
			boundedId = UNDEFINED;
	}

}
