package cbr.pt;

import java.io.File;

public class ResourcesUtils {

	public final static String FOLDER_NAME_RESOURCES = "resources";

	public final static String FOLDER_NAME_MODELS = "models";

	public final static String FOLDER_NAME_TESTS = "tests";

	public final static String FOLDER_NAME_CONFIG = "config";

	public final static String FOLDER_NAME_ICONS = "icons";

	public final static String FOLDER_NAME_DISABLED = "disabled";

	public final static String FOLDER_NAME_ENABLED = "enabled";

	public final static String FOLDER_NAME_32x32 = "32x32";

	public static final String FOLDER_RESOURCES = File.separator
			+ FOLDER_NAME_RESOURCES;

	public static final String FOLDER_TESTS = FOLDER_RESOURCES + File.separator
			+ FOLDER_NAME_TESTS;

	public static final String FOLDER_MODELS = FOLDER_RESOURCES
			+ File.separator + FOLDER_NAME_MODELS;

	public static final String FOLDER_CONFIG = FOLDER_RESOURCES
			+ File.separator + FOLDER_NAME_CONFIG;

	public static final String FOLDER_ICONS = FOLDER_RESOURCES + File.separator
			+ FOLDER_NAME_ICONS;

	public static final String FOLDER_DISABLED = FOLDER_ICONS + File.separator
			+ FOLDER_NAME_DISABLED;

	public static final String FOLDER_ENABLED = FOLDER_ICONS + File.separator
			+ FOLDER_NAME_ENABLED;

	public static final String FOLDER_ENABLED_32x32 = FOLDER_ENABLED
			+ File.separator + FOLDER_NAME_32x32;

	public static final String FOLDER_DISABLED_32x32 = FOLDER_DISABLED
			+ File.separator + FOLDER_NAME_32x32;

	public static String getXMLModelInResurcesPath(String name) {
		return FOLDER_MODELS + File.separator + name + ".xml";
	}

	public static String getXMLTestInResurcesPath(String name) {
		return FOLDER_TESTS + File.separator + name + ".xml";
	}

	public static String getXMLConfigInResurcesPath(String name) {
		return FOLDER_CONFIG + File.separator + name + ".xml";
	}

	public static String getIconInIconPath(String name, int size,
			boolean isEnabled) {
		return FOLDER_ICONS + File.separator
				+ (isEnabled ? "enabled" : "disabled") + File.separator + size
				+ "x" + size + File.separator + name;

	}

}
