package cbr.pt.router;

import cbr.IChannelPathFinderAlgorythm;
import cbr.IPathBuilderAlgorythm;
import cbr.channels.manager.AbstractChannelsManager;
import cbr.grid.IObstaclesBoundedGrid;
import cbr.pt.frontend.console.IAreaView;
import cbr.pt.frontend.console.IWireView;

public class CBRChannelsManager extends
		AbstractChannelsManager<IWireView, IAreaView> {

	private IObstaclesBoundedGrid grid;

	public CBRChannelsManager(IObstaclesBoundedGrid grid) {
		this.grid = grid;
	}

	public IObstaclesBoundedGrid getGrid() {
		return grid;
	}

	@Override
	protected IChannelPathFinderAlgorythm createFinderAlgorythm() {
		return new CBRChannelPathFinderAlgorythm(grid);
	}

	@Override
	protected IPathBuilderAlgorythm createBuilderAlgorythm() {
		return new CBRPathBuilderAlgorythm(grid);
	}

}
