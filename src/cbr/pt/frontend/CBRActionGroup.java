package cbr.pt.frontend;

import java.util.ArrayList;
import java.util.List;

public class CBRActionGroup implements IAppContextProvider {

	private String name;

	private CBRActionGroup parent;

	private List<CBRAction> actions = new ArrayList<CBRAction>();

	private List<CBRActionGroup> childs = new ArrayList<CBRActionGroup>();

	private List<CBRActionGroup> groups = new ArrayList<CBRActionGroup>();

	private CBRAppContext appContext;

	public CBRActionGroup(CBRAppContext appContext, String name) {
		this.name = name;
		this.appContext = appContext;
	}

	public String getName() {
		return name;
	}

	public boolean isRoot() {
		return getParent() == null;
	}

	public void addAction(CBRAction action) {
		actions.add(action);
	}

	public CBRActionGroup getParent() {
		return parent;
	}

	public void setParent(CBRActionGroup group) {
		this.parent = group;
	}

	public void addChildActionGroup(CBRActionGroup group) {
		getChildActionGroups().add(group);
		group.setParent(this);
	}

	public void addActionGroup(CBRActionGroup group) {
		getActionGroups().add(group);
		group.setParent(this);
	}

	public List<CBRAction> getActions() {
		return actions;
	}

	public List<CBRActionGroup> getActionGroups() {
		return groups;
	}

	public List<CBRActionGroup> getChildActionGroups() {
		return childs;
	}

	@Override
	public CBRAppContext getAppContext() {
		return appContext;
	}

	@Override
	public void setAppContext(CBRAppContext context) {
		this.appContext = context;
		for (CBRActionGroup group : getActionGroups()) {
			group.setAppContext(context);
		}
		for (CBRActionGroup group : getChildActionGroups()) {
			group.setAppContext(context);
		}
		for (CBRAction action : getActions()) {
			action.setAppContext(context);
		}
	}

	public boolean isEmpty() {
		return !(isHaveActions() || isHaveNonEmptyGroup());
	}

	public boolean isHaveActions() {
		return !actions.isEmpty();
	}

	public boolean isHaveNonEmptyGroup() {
		for (CBRActionGroup group : getActionGroups()) {
			if (!group.isEmpty()) {
				return true;
			}
		}
		for (CBRActionGroup group : getChildActionGroups()) {
			if (!group.isEmpty()) {
				return true;
			}
		}
		return false;
	}

}
