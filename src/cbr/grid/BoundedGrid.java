package cbr.grid;

import java.util.Collections;
import java.util.List;

import cbr.geometry.Geometry;
import cbr.geometry.IDimension;
import cbr.geometry.IPoint;
import cbr.geometry.IRectangle;
import cbr.geometry.ITranslatable;
import cbr.geometry.Point;
import cbr.geometry.Rectangle;

public class BoundedGrid<T, I> extends Grid<T, I> implements IBoundedGrid<T, I> {

	private static final IRectangle SINGLETON = new Rectangle();

	private IRectangle bounds;

	public BoundedGrid() {
		this(0, 0, 0, 0);
	}

	public BoundedGrid(Rectangle bounds) {
		this(bounds.getLocation(), bounds.getSize());
	}

	public BoundedGrid(IPoint location, IDimension size) {
		this(location.getX(), location.getY(), size.getWidth(), size
				.getHeight());
	}

	public BoundedGrid(int x, int y, int width, int height) {
		super(x, y, width, height);
		bounds = new Rectangle(x, y, width, height);
	}

	@Override
	public int getX() {
		return getBounds().getX();
	}

	@Override
	public int getY() {
		return getBounds().getY();
	}

	@Override
	public void setX(int x) {
		super.setLeftBounds(x);
		getBounds().setX(x);
		getBounds().setWidth(
				getCells()[getColumnsCount() - 1][getRowsCount() - 1].getX()
						+ getCells()[getColumnsCount() - 1][getRowsCount() - 1]
								.getWidth() - getCells()[0][0].getX());
	}

	@Override
	public void setY(int y) {
		super.setTopBounds(y);
		getBounds().setY(y);
		getBounds().setHeight(
				getCells()[getColumnsCount() - 1][getRowsCount() - 1].getY()
						+ getCells()[getColumnsCount() - 1][getRowsCount() - 1]
								.getHeight() - getCells()[0][0].getY());
	}

	@Override
	public int getWidth() {
		return getBounds().getWidth();
	}

	@Override
	public int getHeight() {
		return getBounds().getHeight();
	}

	@Override
	public void setWidth(int width) {
		super.setRightBounds(bounds.getX() + width);
		getBounds().setWidth(width);
	}

	@Override
	public void setHeight(int height) {
		super.setBottomBounds(bounds.getY() + height);
		getBounds().setHeight(height);
	}

	@Override
	public IPoint getLocation() {
		return getBounds().getLocation();
	}

	@Override
	public IDimension getSize() {
		return getBounds().getSize();
	}

	@Override
	public void setLocation(IPoint location) {
		setX(location.getX());
		setY(location.getY());
	}

	@Override
	public void setSize(IDimension size) {
		setWidth(size.getWidth());
		setHeight(size.getHeight());
	}

	@Override
	public IRectangle getBounds() {
		return bounds;
	}

	@Override
	public void setBounds(IRectangle bounds) {
		// exception with centered obstacle : top expand , shrink, left expand,
		// shrink
		if (bounds.getWidth() == getWidth() && bounds.getX() > getX()) {
			setWidth(getWidth() + bounds.getX() - getX());
			setX(bounds.getX());
			setWidth(bounds.getWidth());
		} else {
			int width = bounds.getWidth();
			setWidth(bounds.getX() + width);
			setX(bounds.getX());
			setWidth(width);
		}
		if (bounds.getHeight() == getHeight() && bounds.getY() > getY()) {
			setHeight(getHeight() + bounds.getY() - getY());
			setY(bounds.getY());
			setHeight(bounds.getHeight());
		} else {
			int height = bounds.getHeight();
			setHeight(bounds.getY() + height);
			setY(bounds.getY());
			setHeight(height);
		}
		updateSides();
	}

	protected int getColumnNumberContainsX(int x) {
		return x >= getX() && x < getX() + getWidth() ? super
				.getColumnNumberContainsX(x) : UNKNOWN;
	}

	protected int getRowNumberContainsY(int y) {
		return y >= getY() && y < getY() + getHeight() ? super
				.getRowNumberContainsY(y) : UNKNOWN;
	}

	@Override
	protected List<ICell> getCellsInRegion(IRectangle bounds) {
		int boundsLeft = bounds.getX();
		int boundsRight = boundsLeft + bounds.getWidth();
		int boundsTop = bounds.getY();
		int boundsBottom = boundsTop + bounds.getHeight();
		int gridLeft = getX();
		int gridRight = gridLeft + getWidth();
		int gridTop = getY();
		int gridBottom = gridTop + getHeight();
		if (boundsRight <= gridLeft || gridRight <= boundsLeft
				|| boundsBottom <= gridTop || gridBottom <= boundsTop)
			return Collections.emptyList();
		SINGLETON.setX(boundsLeft < gridLeft ? gridLeft : boundsLeft);
		SINGLETON.setY(boundsTop < gridTop ? gridTop : boundsTop);
		SINGLETON
				.setWidth(boundsRight > gridRight ? (bounds.getWidth() - (boundsRight - gridRight))
						: bounds.getWidth());
		SINGLETON
				.setHeight(boundsBottom > gridBottom ? (bounds.getHeight() - (boundsBottom - gridBottom))
						: bounds.getHeight());
		return super.getCellsInRegion(SINGLETON);
	}

	@Override
	public void dispose() {
		for (int i = 0; i < getColumnsCount(); i++) {
			for (int j = 0; j < getRowsCount(); j++) {
				ICell cell = grid[i][j];
				if (cell != null) {
					cell.dispose();
				}
				grid[i][j] = null;
			}
			grid[i] = null;
		}
		grid = null;
	}

	@Override
	public boolean isContains(IPoint point) {
		return isContains(point.getX(), point.getY());
	}

	@Override
	public boolean isContains(int x, int y) {
		return Geometry.isContains(x, y, getX(), getY(), getWidth(),
				getHeight());
	}

	@Override
	public void translateTo(ITranslatable translatable) {
		getBounds().translateTo(translatable);
	}

	@Override
	public void translateFrom(ITranslatable translatable) {
		getBounds().translateFrom(translatable);
	}

	@Override
	public IRectangle copy() {
		throw new UnsupportedOperationException(
				"Unsupported operation for class: "
						+ getClass().getSimpleName());
	}

	@Override
	public IPoint getCenter() {
		IPoint location = getLocation();
		IDimension size = getSize();
		return new Point(location.getX() + size.getWidth() / 2, location.getY()
				+ size.getHeight() / 2);
	}

}
