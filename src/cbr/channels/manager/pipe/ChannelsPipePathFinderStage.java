package cbr.channels.manager.pipe;

import cbr.IChannelPathFinderAlgorythm;
import cbr.channels.IChannel;
import cbr.pt.frontend.CBRApp;

/**
 * 
 * Стадия поиска пути для канала.
 * 
 * @author AlexanderStrakh
 * 
 */
public class ChannelsPipePathFinderStage extends AbstractChannelsPipeStage {

	private IChannelPathFinderAlgorythm algorythm;

	private static int count = 0;

	public ChannelsPipePathFinderStage(ChannelsPipePathBuilderStage next) {
		super(next);
	}

	public void setAlgorythm(IChannelPathFinderAlgorythm algorythm) {
		this.algorythm = algorythm;
	}

	@Override
	protected void processChannel(IChannel channel) {
		if (algorythm != null) {
			algorythm.process(channel);
			CBRApp.logger().info("Channel finder calls counter: " + ++count);
		}
	}

}
