package cbr.algs.astar;

import cbr.grid.ICell;

public class Point {

	public int x;
	public int y;
	public int g;
	public int h;
	public int f;
	public Point parent;
	public ICell cell;

	public Point(ICell cell, int x, int y, Point parent) {
		this.x = x;
		this.y = y;
		this.parent = parent;
		this.cell = cell;
	}

	public Point(ICell cell, int x, int y, Point parent, int g, int h) {
		this(cell, x, y, parent);
		this.g = g;
		this.h = h;
		this.f = g + h;
	}

	@Override
	public boolean equals(Object o) {
		return (o instanceof Point && ((Point) o).x == x && ((Point) o).y == y);
	}

	@Override
	public int hashCode() {
		return (x * y) ^ (x + y);
	}

}
