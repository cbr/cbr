package cbr.geometry.operators;

public interface IPointCoordsDependsOperator extends ICoordsDependsOperator {

	public void setCoord(int coord);

	public int getCoord();

	@Override
	public IPointCoordsDependsOperator change();

}
