package cbr.pt.frontend.console;

import cbr.geometry.IPoint;
import cbr.geometry.IRectangle;

public abstract class AbstractAnchorPointLocator implements IAnchorPointLocator {

	private IIdentifiedAnchorView owner;

	public AbstractAnchorPointLocator(IIdentifiedAnchorView owner) {
		this.owner = owner;
	}

	protected IIdentifiedAnchorView getOwner() {
		return owner;
	}

	private IRectangle getBounds() {
		return getOwner().getBounds();
	}

	@Override
	public int locate(IConnector connector) {
		IPoint location = connector.getLocation();
		int side = connector.getSide();
		IRectangle bounds = getBounds().copy();
		getOwner().translateFrom(bounds);
		side = processLocate(bounds, location, side);
		getOwner().translateTo(bounds);
		return side;
	}

	protected abstract int processLocate(IRectangle bounds, IPoint location,
			int side);

}
