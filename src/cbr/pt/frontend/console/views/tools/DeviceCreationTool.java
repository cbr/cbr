package cbr.pt.frontend.console.views.tools;

import cbr.pt.frontend.console.CBRContext;
import cbr.pt.frontend.console.tools.AreaCreationTool;
import cbr.pt.frontend.console.views.Connection;
import cbr.pt.frontend.console.views.Device;
import cbr.pt.frontend.console.views.Port;
import cbr.pt.frontend.console.views.Zone;

public class DeviceCreationTool extends AreaCreationTool {

	public DeviceCreationTool(CBRContext context) {
		super(context, Device.class);
		addToOpaqueList(Zone.class);
		addToOpaqueList(Device.class);
		addToOpaqueList(Connection.class);
		addToOpaqueList(Port.class);
		addToOverboardList(Port.class);
	}

}
