package cbr.grid;

public interface ISide {

	public ICell getNeighbour1();

	public ICell getNeighbour2();

	public ICell getNeighbour(ICell cell);

	public void setNeighbour1(ICell cell);

	public void setNeighbour2(ICell cell);
	
}
