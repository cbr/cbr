package cbr.pt.frontend.actions;

import java.awt.event.ActionEvent;

import cbr.pt.frontend.CBRAppContext;

public class CBRActionPasteView extends CBRActionSelectedListener {

	private static final long serialVersionUID = 1L;

	public final static String ACTION_NAME = "Paste view";

	public final static String ACTION_TIP = "Paste view";

	public final static String ACTION_ICON = "paste.png";

	public CBRActionPasteView(CBRAppContext appContext) {
		super(appContext, ACTION_NAME, ACTION_TIP, ACTION_ICON);
	}

	@Override
	public void actionPerformed(ActionEvent event) {
		throw new UnsupportedOperationException("Not implemented yet!");
	}

}
