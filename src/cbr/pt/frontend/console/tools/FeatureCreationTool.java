package cbr.pt.frontend.console.tools;

import cbr.geometry.IPoint;
import cbr.pt.frontend.console.CBRContext;
import cbr.pt.frontend.console.IAreaView;
import cbr.pt.frontend.console.editpolicy.AbstractEditPolicy;
import cbr.pt.frontend.console.editpolicy.FeatureCreationEditPolicy;

public class FeatureCreationTool extends ReactivationEditPolicyMoveSupportTool {

	private IAreaView feedback;

	private Class<? extends IAreaView> clazz;

	public FeatureCreationTool(CBRContext context,
			Class<? extends IAreaView> clazz) {
		super(context);
		this.clazz = clazz;
		addSupportedPolicyName(FeatureCreationEditPolicy.EDIT_POLICY_FEATURE_CREATION);
	}

	private IAreaView getFeedback() {
		if (feedback == null) {
			feedback = getContext().getFactory().createCreationFeedback(clazz);
		}
		return feedback;
	}

	protected void activateViewPolicy(AbstractEditPolicy policy) {
		policy.setFeedbackView(getFeedback());
		policy.activate();
	}

	@Override
	protected void clickViewPolicy(AbstractEditPolicy policy, IPoint location) {
		if (policy instanceof FeatureCreationEditPolicy)
			((FeatureCreationEditPolicy) policy).setCurrentClass(clazz);
		super.clickViewPolicy(policy, location);
	}

	@Override
	protected boolean isSupported(AbstractEditPolicy editPolicy) {
		return editPolicy instanceof FeatureCreationEditPolicy
				&& ((FeatureCreationEditPolicy) editPolicy).isSupported(clazz);
	}

}
