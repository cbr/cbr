package cbr.pt.frontend.console.views.actions;

import cbr.pt.frontend.CBRAppContext;
import cbr.pt.frontend.actions.CBRActionCreationTool;
import cbr.pt.frontend.console.CBRContext;
import cbr.pt.frontend.console.CBRModel;
import cbr.pt.frontend.console.IAreaView;
import cbr.pt.frontend.console.IModelObjectAddOrRemoveListener;
import cbr.pt.frontend.console.IZone;
import cbr.pt.frontend.console.tools.IToolChangeListener;
import cbr.pt.frontend.console.views.tools.ZoneCreationTool;

public class CBRActionSelectZoneCreationTool extends CBRActionCreationTool
		implements IToolChangeListener, IModelObjectAddOrRemoveListener {

	private static final long serialVersionUID = 1L;

	public final static String ACTION_NAME = "Create zone";

	public final static String ACTION_TIP = "Create zone";

	public final static String ACTION_ICON = "create_zone.png";

	public CBRActionSelectZoneCreationTool(CBRAppContext appContext) {
		super(appContext, ACTION_NAME, ACTION_TIP, ACTION_ICON,
				new ZoneCreationTool(appContext.getConfig().getContext()));
		getAppContext().getConfig().getContext()
				.addModelObjectAddOrRemoveListener(this);
		setEnablement();
	}

	private void setEnablement() {
		// TODO: needs to optimize, because called when feddbacks added or
		// removed instead of model objects
		CBRContext context = getAppContext().getConfig().getContext();
		if (context.isCBRModelExists()) {
			CBRModel model = context.getCBRModel();
			for (IAreaView view : model.getChildrens()) {
				if (view instanceof IZone) {
					setEnabled(false);
					return;
				}
			}
			setEnabled(true);
		} else {
			setEnabled(false);
		}
	}

	@Override
	public void addToModel(IAreaView view, IAreaView parent) {
		setEnablement();
	}

	@Override
	public void removeFromModel(IAreaView view, IAreaView parent) {
		setEnablement();
	}

	@Override
	public void initializeModel() {
		setEnablement();
	}

}
