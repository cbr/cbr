package cbr.serialize.xml;

import java.util.HashMap;
import java.util.Map;

public class XMLSerializationMapping {

	private Map<Class<?>, Class<?>> mapping = new HashMap<Class<?>, Class<?>>();

	private Map<String, Class<?>> elements = new HashMap<String, Class<?>>();

	public void addMapping(String name, Class<?> iface, Class<?> clazz) {
		mapping.put(iface, clazz);
		elements.put(name, iface);
	}

	public boolean isNameOfClass(String name) {
		return elements.containsKey(name);
	}

	public Class<?> getClass(String name) {
		Class<?> iface = getInterface(name);
		return iface == null ? null : mapping.get(iface);
	}

	public Class<?> getInterface(String name) {
		return elements.get(name);
	}
}
