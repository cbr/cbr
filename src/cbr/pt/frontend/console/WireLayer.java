package cbr.pt.frontend.console;

import java.util.HashSet;
import java.util.Set;

import cbr.Compass;
import cbr.grid.IObstaclesBoundedGrid;
import cbr.pt.router.CBRouter;

public class WireLayer extends AbstractView implements ILayer {

	private CBRouter router;

	private Set<IWireView> routedWires = new HashSet<IWireView>();

	public CBRouter getRouter() {
		if (router == null) {
			router = new CBRouter();
		}
		return router;
	}

	public IObstaclesBoundedGrid getGrid() {
		return getRouter().getGrid();
	}

	public void updateWire(IWireView wireView) {
		if (wireView.getParent() == null) {
			if (routedWires.contains(wireView)) {
				routedWires.remove(wireView);
				getRouter().removeWireByConstraint(wireView);
			}
		} else {
			if (!routedWires.contains(wireView)) {
				IConnector sourceConnector = wireView.getSourceConnector();
				IConnector targetConnector = wireView.getTargetConnector();
				if (sourceConnector != null && targetConnector != null
						&& Compass.isDeterminedSide(sourceConnector.getSide())
						&& Compass.isDeterminedSide(targetConnector.getSide())) {
					routedWires.add(wireView);
					getRouter().addWireByConstraint(wireView);
				}
			}
		}
	}

	@Override
	public void clear() {
		if (router != null) {
			router.dispose();
			router = null;
		}
		routedWires.clear();
	}

	public void switchOffRouter() {
		if (router != null) {
			router.switchOff();
		}
	}

}
