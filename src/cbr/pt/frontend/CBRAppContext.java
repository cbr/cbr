package cbr.pt.frontend;

import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Image;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.imageio.ImageIO;
import javax.swing.Icon;
import javax.swing.JFrame;

import cbr.pt.ResourcesUtils;
import cbr.pt.frontend.actions.groups.CBRFrontendRootActionGroup;
import cbr.pt.frontend.console.IConfigProvier;

public class CBRAppContext implements IConfigProvier {

	private Map<String, Icon> icons = new HashMap<String, Icon>();

	private CBRActionGroup rootActionGroup;

	private Config config;

	private JFrame frame;

	public CBRAppContext(Config config, JFrame frame) {
		this.config = config;
		this.frame = frame;
		rootActionGroup = new CBRFrontendRootActionGroup(this);
	}

	public JFrame getFrame() {
		return frame;
	}

	public CBRActionGroup getActionGroup() {
		return rootActionGroup;
	}

	@Override
	public Config getConfig() {
		return config;
	}

	@Override
	public void setConfig(Config config) {
		this.config = config;
	}

	public static Image getIconImage(String path) {
		try {
			return ImageIO.read(CBRAppContext.class.getResourceAsStream(path));
		} catch (IOException e) {
			CBRApp.logger().err("Can't load image: " + path);
			CBRApp.logger().err(e.getMessage());
		}
		return null;
	}

	public Icon getIcon(final String name, final boolean isEnabled,
			final int size) {
		Icon icon = icons
				.get((isEnabled ? "enabled" : "disabled") + ":" + name);
		if (icon == null) {
			icon = createIcon(name, size, isEnabled);
			icons.put(name, icon);
		}
		return icon;
	}

	public Icon getEnabledIcon(String name, int size) {
		return getIcon(name, true, size);
	}

	public Icon getDisabledIcon(String name, int size) {
		return getIcon(name, false, size);
	}

	public Icon getEnabledIcon(String name) {
		return getEnabledIcon(name, config.iconSize);
	}

	public Icon getDisabledIcon(String name) {
		return getDisabledIcon(name, config.iconSize);
	}

	protected Icon createIcon(String name, final int size, boolean isEnabled) {
		final String path = ResourcesUtils.getIconInIconPath(name, size,
				isEnabled);
		try {
			return new Icon() {

				private Image image = ImageIO.read(CBRAppContext.class
						.getResourceAsStream(path));

				@Override
				public void paintIcon(Component c, Graphics g, int x, int y) {
					g.drawImage(image, x, y, getIconWidth(), getIconHeight(),
							null);
				}

				@Override
				public int getIconWidth() {
					return size;
				}

				@Override
				public int getIconHeight() {
					return size;
				}

			};
		} catch (IOException e) {
			CBRApp.logger().err("Can't load image: " + path);
			CBRApp.logger().err(e.getMessage());
			CBRApp.logger().warn("Create dummy icon");
			return new Icon() {

				private Color color = new Color(100, 100, 100);

				@Override
				public void paintIcon(Component c, Graphics g, int x, int y) {
					g.setColor(color);
					g.fillRect(x, y, getIconWidth(), getIconHeight());
				}

				@Override
				public int getIconWidth() {
					return size;
				}

				@Override
				public int getIconHeight() {
					return size;
				}

			};
		}
	}

}
