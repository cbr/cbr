package cbr.pt.frontend.command;

import cbr.geometry.IRectangle;
import cbr.pt.frontend.console.CBRContext;
import cbr.pt.frontend.console.IAreaView;
import cbr.pt.frontend.console.IFeatureView;

public class SetFeatureConstraintCommand extends SetConstraintCommand {

	private int newSide;

	private int oldSide;

	protected SetFeatureConstraintCommand(CBRContext context,
			String description, IAreaView view, IRectangle bounds, int side) {
		super(context, description, view, bounds);
		this.newSide = side;
		this.oldSide = ((IFeatureView) view).getSide();
	}

	protected IFeatureView getBoundedView() {
		return (IFeatureView) super.getBoundedView();
	}

	@Override
	public void execute() {
		super.execute();
		getBoundedView().setSide(newSide);
	}

	@Override
	public void undo() {
		getBoundedView().setSide(oldSide);
		super.undo();
	}

}
