package cbr.geometry.operators;

import cbr.geometry.IPoint;

public class LocationXYCoordsDependsOperator extends
		AbstractXYCoordsDependsOperator implements IPointCoordsDependsOperator {

	private IPoint location;

	public LocationXYCoordsDependsOperator(boolean isX, IPoint location) {
		super(isX);
		this.location = location;
	}

	@Override
	public void setCoord(int coord) {
		if (isX)
			location.setX(coord);
		else
			location.setY(coord);
	}

	@Override
	public int getCoord() {
		return isX ? location.getX() : location.getY();
	}

	@Override
	public LocationXYCoordsDependsOperator change() {
		super.change();
		return this;
	}

}
