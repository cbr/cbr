package cbr.pt.frontend.console;

import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;

import cbr.geometry.IDimension;
import cbr.geometry.IPoint;
import cbr.geometry.IRectangle;

public class CBRGraphics {

	private Graphics graphics;

	private CBRContext context;

	public CBRGraphics(CBRContext context) {
		this.context = context;
	}

	public void setGraphics(Graphics graphics) {
		this.graphics = graphics;
	}

	public void setColor(Color color) {
		graphics.setColor(color);
	}

	public void translate(IPoint location) {
		graphics.translate(location.getX(), location.getY());
	}

	public void fillRect(IRectangle bounds) {
		IPoint location = bounds.getLocation();
		IDimension size = bounds.getSize();
		graphics.fillRect(location.getX(), location.getY(), size.getWidth(),
				size.getHeight());
	}

	public void setContext(CBRContext context) {
		this.context = context;
	}

	public CBRContext getContext() {
		return context;
	}

	public void drawRect(int x, int y, int width, int height) {
		graphics.drawRect(x, y, width, height);
	}

	public void drawString(String string, int x, int y) {
		graphics.drawString(string, x, y);
	}

	public void fillRect(int x, int y, int width, int height) {
		graphics.fillRect(x, y, width, height);
	}

	public void translateTo(IPoint location) {
		graphics.translate(location.getX(), location.getY());
	}

	public void translateFrom(IPoint location) {
		graphics.translate(-location.getX(), -location.getY());
	}

	public void drawRect(IRectangle bounds) {
		IPoint location = bounds.getLocation();
		IDimension size = bounds.getSize();
		graphics.drawRect(location.getX(), location.getY(), size.getWidth(),
				size.getHeight());
	}

	public void drawLine(IPoint start, IPoint end) {
		graphics.drawLine(start.getX(), start.getY(), end.getX(), end.getY());
	}

	public void drawLine(int x1, int y1, int x2, int y2) {
		graphics.drawLine(x1, y1, x2, y2);
	}

	public void setAlpha(float alpha) {
		if (graphics instanceof Graphics2D) {
			AlphaComposite composite = AlphaComposite.getInstance(
					AlphaComposite.SRC_OVER, alpha);
			((Graphics2D) graphics).setComposite(composite);
		}
	}

	public void fillRound(IPoint center, int diameter) {
		graphics.fillOval(center.getX() - diameter / 2, center.getY()
				- diameter / 2, diameter, diameter);
	}

}
