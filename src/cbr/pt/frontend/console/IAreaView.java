package cbr.pt.frontend.console;

import java.util.List;

import cbr.IDisposable;
import cbr.geometry.IBounded;
import cbr.geometry.IDimension;
import cbr.geometry.IPoint;
import cbr.geometry.ITranslatable;
import cbr.pt.frontend.console.editpolicy.AbstractEditPolicy;
import cbr.serialize.xml.annotations.XMLElement;
import cbr.serialize.xml.annotations.XMLNode;

@XMLNode("areaView")
public interface IAreaView extends IBounded, IDisposable, ITranslatable {

	public static final int ORDER_MIN = 0;

	@XMLElement
	public List<IAreaView> getChildrens();

	public int getOrder();

	public void setOrder(int order);

	public void setChildrens(List<IAreaView> childrensList);

	public void paint(CBRGraphics g);

	public IAreaView getContainsPointView(IPoint point,
			List<Class<? extends IAreaView>> opaqueList,
			List<Class<? extends IAreaView>> overboardList);

	public boolean useLocalCoordinates();

	public void repaint();

	public IAreaView getParent();

	public void setParent(IAreaView parent);

	public boolean isVisible();

	public CBRContext getContext();

	public void setContext(CBRContext context);

	public void setVisible(boolean isVisible);

	public void translateToAbsolute(ITranslatable translatable);

	public void translateToRelative(ITranslatable translatable);

	public IPoint getCenter();

	public void setCenter(IPoint point);

	public IPoint getCenterVector();

	public IAreaView findModelById(int id);

	public void translateToCenter(ITranslatable translatable);

	public void translateFromCenter(ITranslatable translatable);

	public void translateTo(ITranslatable translatable);

	public void translateFrom(ITranslatable translatable);

	public IAreaView getRoot();

	public boolean addChild(IAreaView view);

	public boolean removeChild(IAreaView view);

	public boolean isInBounds(IPoint point);

	public boolean isInBoundsOverboarded(IPoint point,
			List<Class<? extends IAreaView>> overboardList);

	public IDimension getMinimumSize();

	public void addEditPolicy(AbstractEditPolicy editPolicy);

	public void removeEditPolicy(AbstractEditPolicy editPolicy);

	public AbstractEditPolicy getEditPolicy(String editPolicyName);

	public void removeChangeBoundsListener(IAreaView selectionFeedback);

	public void addChangeBoundsListener(IAreaView selectionFeedback);

	public void recursiveByAddChildAction();

	public void recursiveAfterAddRootAction();
}
