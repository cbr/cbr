package cbr.pt.frontend.actions.groups;

import cbr.pt.frontend.CBRActionGroup;
import cbr.pt.frontend.CBRAppContext;
import cbr.pt.frontend.actions.CBRActionCreateModel;
import cbr.pt.frontend.actions.CBRActionOpenModel;

public class CBRNewOpenActionGroup extends CBRActionGroup {

	public CBRNewOpenActionGroup(CBRAppContext appContext) {
		super(appContext, "New/Open");
		addAction(new CBRActionCreateModel(appContext));
		addAction(new CBRActionOpenModel(appContext));
	}

}
