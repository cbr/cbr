package cbr.pt.frontend.console.tools;

import java.util.ArrayList;
import java.util.List;

import cbr.geometry.IPoint;
import cbr.geometry.Point;
import cbr.pt.frontend.console.CBRConsole;
import cbr.pt.frontend.console.CBRContext;
import cbr.pt.frontend.console.IAreaView;

public class AbstractTool {

	private CBRContext context;

	private List<Class<? extends IAreaView>> opaqueList = new ArrayList<Class<? extends IAreaView>>();

	private List<Class<? extends IAreaView>> overboardList = new ArrayList<Class<? extends IAreaView>>();

	public AbstractTool(CBRContext context) {
		this.context = context;
	}

	public CBRContext getContext() {
		return context;
	}

	public void setContext(CBRContext context) {
		this.context = context;
	}

	protected void addToOpaqueList(Class<? extends IAreaView> clazz) {
		opaqueList.add(clazz);
	}

	protected void addToOverboardList(Class<? extends IAreaView> clazz) {
		overboardList.add(clazz);
	}

	public CBRConsole getConsole() {
		return getContext().getConsole();
	}

	public IAreaView getViewContainsPoint(IPoint point) {
		return getConsole().getContainsPointView(point, opaqueList,
				overboardList);
	}

	public void repaint() {
		getContext().repaint();
	}

	public void mouseMoved(int x, int y) {
		IPoint point = new Point(x, y);
		IAreaView view = getViewContainsPoint(point);
		if (handleMouseMoved(view, point)) {
			repaint();
		}
	}

	public void mouseDragged(int x, int y) {
		IPoint point = new Point(x, y);
		IAreaView view = getViewContainsPoint(point);
		if (handleMouseDragged(view, point)) {
			repaint();
		}
	}

	public void mouseEntered(int x, int y) {
		IPoint point = new Point(x, y);
		IAreaView view = getViewContainsPoint(point);
		if (handleMouseEntered(view, point)) {
			repaint();
		}
	}

	public void mouseExited(int x, int y) {
		IPoint point = new Point(x, y);
		IAreaView view = getViewContainsPoint(point);
		if (handleMouseExited(view, point)) {
			repaint();
		}
	}

	public void mousePressed(int x, int y) {
		IPoint point = new Point(x, y);
		IAreaView view = getViewContainsPoint(point);
		if (handleMousePressed(view, point)) {
			repaint();
		}
	}

	public void mouseReleased(int x, int y) {
		IPoint point = new Point(x, y);
		IAreaView view = getViewContainsPoint(point);
		if (handleMouseReleased(view, point)) {
			repaint();
		}
	}

	public void mouseClicked(int x, int y) {
		IPoint point = new Point(x, y);
		IAreaView view = getViewContainsPoint(point);
		if (handleMouseClicked(view, point)) {
			repaint();
		}
	}

	public boolean handleMouseMoved(IAreaView view, IPoint point) {
		return false;
	}

	public boolean handleMouseDragged(IAreaView view, IPoint point) {
		return false;
	}

	public boolean handleMouseEntered(IAreaView view, IPoint point) {
		return false;
	}

	public boolean handleMouseExited(IAreaView view, IPoint point) {
		return false;
	}

	public boolean handleMousePressed(IAreaView view, IPoint point) {
		return false;
	}

	public boolean handleMouseReleased(IAreaView view, IPoint point) {
		return false;
	}

	public boolean handleMouseClicked(IAreaView view, IPoint point) {
		return false;
	}

	public void unselect() {
	}

	public void select() {
	}

}
