package cbr.pt.frontend.console.tools;

import cbr.geometry.IPoint;
import cbr.pt.frontend.console.CBRContext;
import cbr.pt.frontend.console.IAreaView;
import cbr.pt.frontend.console.editpolicy.AbstractEditPolicy;

public class ReactivationEditPolicyDragSupportTool extends
		ReactivationEditPolicyMoveSupportTool {

	public ReactivationEditPolicyDragSupportTool(CBRContext context) {
		super(context);
	}

	@Override
	public boolean handleMouseDragged(IAreaView view, IPoint location) {
		return processDrag(location);
	}

	@Override
	public boolean handleMousePressed(IAreaView view, IPoint location) {
		return startDrag(location);
	}

	@Override
	public boolean handleMouseReleased(IAreaView view, IPoint location) {
		return finishDrag(location);
	}

	@Override
	public boolean handleMouseClicked(IAreaView view, IPoint location) {
		return super.handleMouseClicked(view, location);
	}

	private boolean startDrag(IPoint location) {
		if (view == null)
			return false;
		boolean isNeedRepaint = false;
		for (String supportedPolicyName : supportedPoliciesNames) {
			AbstractEditPolicy supportedEditPolicy = view
					.getEditPolicy(supportedPolicyName);
			if (supportedEditPolicy != null && isSupported(supportedEditPolicy)) {
				startDrag(supportedEditPolicy, location);
				if (!isNeedRepaint)
					isNeedRepaint = true;
			}
		}
		return isNeedRepaint;
	}

	private boolean finishDrag(IPoint location) {
		if (view == null)
			return false;
		boolean isNeedRepaint = false;
		for (String supportedPolicyName : supportedPoliciesNames) {
			AbstractEditPolicy supportedEditPolicy = view
					.getEditPolicy(supportedPolicyName);
			if (supportedEditPolicy != null && isSupported(supportedEditPolicy)) {
				finishDrag(supportedEditPolicy, location);
				if (!isNeedRepaint)
					isNeedRepaint = true;
			}
		}
		return isNeedRepaint;
	}

	private boolean processDrag(IPoint location) {
		if (view == null)
			return false;
		boolean isNeedRepaint = false;
		for (String supportedPolicyName : supportedPoliciesNames) {
			AbstractEditPolicy supportedEditPolicy = view
					.getEditPolicy(supportedPolicyName);
			if (supportedEditPolicy != null && isSupported(supportedEditPolicy)) {
				processDrag(supportedEditPolicy, location);
				if (!isNeedRepaint)
					isNeedRepaint = true;
			}
		}
		return isNeedRepaint;
	}

	protected void processDrag(AbstractEditPolicy policy, IPoint location) {
		policy.processDrag(location);
	}

	protected void finishDrag(AbstractEditPolicy policy, IPoint location) {
		policy.finishDrag(location);
	}

	protected void startDrag(AbstractEditPolicy policy, IPoint location) {
		policy.startDrag(location);
	}

}
