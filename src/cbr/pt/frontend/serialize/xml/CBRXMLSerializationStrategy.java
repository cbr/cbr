package cbr.pt.frontend.serialize.xml;

import java.util.HashMap;
import java.util.Map;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import cbr.serialize.xml.IXMLSerializationStrategy;
import cbr.serialize.xml.XMLSerializableObject;

public class CBRXMLSerializationStrategy implements IXMLSerializationStrategy {

	@Override
	public Element serialize(Document document, String name,
			String parentFieldName, Map<String, String> attributes) {
		Element valueElement = document.createElement(name);
		for (String attrName : attributes.keySet()) {
			valueElement.setAttribute(attrName, attributes.get(attrName));
		}
		if (parentFieldName != null) {
			Element fieldElement = document.createElement(parentFieldName);
			fieldElement.appendChild(valueElement);
			return fieldElement;
		}
		return valueElement;
	}

	@Override
	public XMLSerializableObject createXSObject(Node node) {
		String name = node.getNodeName();
		NamedNodeMap namedNodeMap = node.getAttributes();
		Map<String, String> attributes = new HashMap<String, String>();
		for (int i = 0; i < namedNodeMap.getLength(); i++) {
			Node childNode = namedNodeMap.item(i);
			String attrName = childNode.getNodeName();
			String attrValue = childNode.getNodeValue();
			if (attrName != null && attrValue != null && attrName.length() > 0
					&& attrValue.length() > 0) {
				attributes.put(attrName, attrValue);
			}
		}
		XMLSerializableObject xsObject = new XMLSerializableObject(name,
				attributes);
		return xsObject;
	}

	@Override
	public String getFieldName(Node node) {
		return node.getNodeName();
	}

	@Override
	public Object parseObjectFromString(Class<?> type, String string) {
		if (type.equals(Integer.class) || type.equals(int.class)) {
			return Integer.parseInt(string);
		} else if (type.equals(String.class)) {
			return string;
		} else if (type.equals(Double.class) || type.equals(double.class)) {
			return Double.parseDouble(string);
		} else if (type.equals(Short.class) || type.equals(short.class)) {
			return Short.parseShort(string);
		} else if (type.equals(Byte.class) || type.equals(byte.class)) {
			return Byte.parseByte(string);
		} else if (type.equals(Boolean.class) || type.equals(boolean.class)) {
			return Boolean.parseBoolean(string);
		}
		throw new UnsupportedOperationException(
				"Can't determine Class type from string for \"" + string + "\"");
	}

	@Override
	public Element getAppendToElement(Element element, String name,
			String parentFieldName) {
		if (parentFieldName == null)
			return element;
		NodeList nodeList = element.getChildNodes();
		for (int i = 0; i < nodeList.getLength(); i++) {
			Node childNode = nodeList.item(i);
			if (childNode.getNodeType() == Node.ELEMENT_NODE
					&& childNode.getNodeName().equals(name)) {
				return (Element) childNode;
			}
		}
		throw new UnsupportedOperationException("Can't find child node " + name);
	}
}
