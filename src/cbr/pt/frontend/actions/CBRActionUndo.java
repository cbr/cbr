package cbr.pt.frontend.actions;

import java.awt.event.ActionEvent;

import cbr.pt.frontend.CBRAppContext;
import cbr.pt.frontend.command.CommandStack;

public class CBRActionUndo extends CBRActionCommandStackListener {

	private static final long serialVersionUID = 1L;

	public final static String ACTION_NAME = "Undo";

	public final static String ACTION_TIP = "Undo";

	public final static String ACTION_ICON = "undo.png";

	public CBRActionUndo(CBRAppContext appContext) {
		super(appContext.getConfig().getContext().getCommandStack(),
				appContext, ACTION_NAME, ACTION_TIP, ACTION_ICON);
		CommandStack commandStack = getAppContext().getConfig().getContext()
				.getCommandStack();
		setEnabled(commandStack.isDirty() && commandStack.canUndo());
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		getAppContext().getConfig().getContext().getCommandStack().undo();
	}

	@Override
	public void handleCommandStackChanged(CommandStack commandStack, int event) {
		setEnabled(commandStack.isDirty() && commandStack.canUndo());
	}
}
