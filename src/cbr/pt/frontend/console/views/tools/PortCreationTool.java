package cbr.pt.frontend.console.views.tools;

import cbr.pt.frontend.console.CBRContext;
import cbr.pt.frontend.console.tools.FeatureCreationTool;
import cbr.pt.frontend.console.views.Connection;
import cbr.pt.frontend.console.views.Port;
import cbr.pt.frontend.console.views.Zone;

public class PortCreationTool extends
		FeatureCreationTool {

	public PortCreationTool(CBRContext context) {
		super(context, Port.class);
		addToOpaqueList(Connection.class);
		addToOpaqueList(Port.class);
		addToOpaqueList(Zone.class);
		addToOverboardList(Port.class);
	}

}
