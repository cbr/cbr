package cbr.pt.frontend.console.clipboard;

import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.ClipboardOwner;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.xml.sax.SAXException;

import cbr.pt.frontend.console.CBRContext;

public class CBRClipboard implements ClipboardOwner {

	private CBRContext context;

	public CBRClipboard(CBRContext context) {
		this.context = context;
	}

	public void setContext(CBRContext context) {
		this.context = context;
	}

	public CBRContext getContext() {
		return context;
	}

	@Override
	public void lostOwnership(Clipboard aClipboard, Transferable aContents) {
	}

	public void setClipboardContents(Object object)
			throws IllegalAccessException, IllegalArgumentException,
			InvocationTargetException, ParserConfigurationException,
			TransformerException {
		String serializedModel = getContext().getSerializer()
				.serializeToString(object);
		StringSelection stringSelection = new StringSelection(serializedModel);
		Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
		clipboard.setContents(stringSelection, this);
	}

	public Object getClipboardContents() throws IllegalAccessException,
			IllegalArgumentException, InvocationTargetException,
			InstantiationException, ParserConfigurationException,
			TransformerException, SAXException, IOException {
		String result = "";
		Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
		Transferable contents = clipboard.getContents(null);
		boolean hasTransferableText = (contents != null)
				&& contents.isDataFlavorSupported(DataFlavor.stringFlavor);
		if (hasTransferableText) {
			try {
				result = (String) contents
						.getTransferData(DataFlavor.stringFlavor);
			} catch (UnsupportedFlavorException | IOException ex) {
				ex.printStackTrace();
			}
		}
		Object object = getContext().getSerializer().deserializeFromString(
				result);
		return object;
	}
}
