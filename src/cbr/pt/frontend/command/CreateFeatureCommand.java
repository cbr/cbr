package cbr.pt.frontend.command;

import cbr.geometry.IRectangle;
import cbr.pt.frontend.console.CBRContext;
import cbr.pt.frontend.console.IAreaView;
import cbr.pt.frontend.console.IFeatureView;

public class CreateFeatureCommand extends CreateAreaCommand {

	private int side;

	protected CreateFeatureCommand(CBRContext context, String description,
			IAreaView parent, Class<? extends IAreaView> clazz,
			IRectangle bounds, int side) {
		super(context, description, parent, clazz, bounds);
		this.side = side;
	}

	@Override
	public void execute() {
		((IFeatureView) getInstance()).setSide(side);
		super.execute();
	}

}
