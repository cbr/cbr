package cbr.algs.astar;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import cbr.grid.ICell;

public class AStar {

	protected HashSet<Point> openSet = new HashSet<Point>();
	protected HashSet<Point> closeSet = new HashSet<Point>();

	public static final int lineCost = 10;
	public static final int diagCost = 14;

	public List<ICell> routeAlgorythm(ICell field[][], int lastY, int lastX,
			int startX, int startY, int endX, int endY) {
		
		if(startX == endX && startY == endY)
			return new ArrayList<ICell>(0);
		
		int counter = 0;
		int limit = lastX*lastY; 
		
		openSet.clear();
		closeSet.clear();

		openSet.add(new Point(
				field[startX][startY],
				startX,
				startY,
				null,
				0,
				((endX > startX ? endX - startX : startX - endX) + (endY > startY ? endY - startY : startY - endY)) * lineCost));

		while (!openSet.isEmpty() && counter < limit) {
			counter++;
			Point curPoint = null;
			for (Point localPoint : openSet) {
				if (curPoint == null || localPoint.f < curPoint.f) {
					curPoint = localPoint;
				}
			}

			// get route if target has been achieved
			if (curPoint.x == endX && curPoint.y == endY) {
				List<Point> points = new ArrayList<Point>();

				// curPoint = new Point(field[endX][endY], endX, endY,
				// curPoint);
				points.add(curPoint);
				while (curPoint.parent != null) {
					curPoint = curPoint.parent;
					points.add(curPoint);
				}
				// curPoint.parent = new Point(field[startX][startY], startX,
				// startY, null);
				// points.add(curPoint.parent);

				List<ICell> path = new ArrayList<ICell>(points.size());
				for (Point point : points) {
					path.add(point.cell);
				}
				return path;
			}

			openSet.remove(curPoint);
			closeSet.add(curPoint);

			int shiftX = 0;
			int shiftY = 0;
			for (int i = 0; i < 8; i++) {
				switch (i) {
				// manhattan directions
				case 0:
					shiftX = 1;
					shiftY = 0;
					break;
				case 1:
					shiftX = 0;
					shiftY = 1;
					break;
				case 2:
					shiftX = -1;
					shiftY = 0;
					break;
				case 3:
					shiftX = 0;
					shiftY = -1;
					break;
				// 45 degrees directions
				// case 4: shiftX= 1; shiftY= 1; break;
				// case 5: shiftX=-1; shiftY=-1; break;
				// case 6: shiftX=-1; shiftY= 1; break;
				// case 7: shiftX= 1; shiftY=-1; break;
				}

				int curX = curPoint.x + shiftX;
				int curY = curPoint.y + shiftY;

				if (curX < 0 || curY < 0 || curX >= lastX || curY >= lastY
						|| field[curX][curY].isObstacle()) {
					continue;
				}

				for (Point point : closeSet) {
					if (point != null && point.x == curX && point.y == curY)
						continue;
				}

				Point existsPoint = null;
				for (Point point : openSet) {
					if (point != null && point.x == curX && point.y == curY) {
						existsPoint = point;
					}
				}

				int g = curPoint.g
						+ (shiftX == 0 || shiftY == 0 ? lineCost : diagCost);

				
				if (existsPoint != null) {

					if (g < existsPoint.g) {
						existsPoint.parent = curPoint;
						existsPoint.g = g;
						existsPoint.h = ((endX > curX ? endX - curX : curX
								- endX) + (endY > curY ? endY - curY : curY
								- endY))
								* lineCost;

						existsPoint.f = existsPoint.g + existsPoint.h;
					}
				} else {
					openSet.add(new Point(field[curX][curY], curX, curY,
							curPoint, g, ((endX > curX ? endX - curX : curX
									- endX) + (endY > curY ? endY - curY : curY
									- endY))
									* lineCost));
				}

			}

		}
		
		return new ArrayList<ICell>(0);
	}

}
