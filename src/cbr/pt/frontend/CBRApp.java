package cbr.pt.frontend;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Toolkit;

import javax.swing.JFrame;
import javax.swing.JMenuBar;
import javax.swing.JToolBar;

import cbr.pt.frontend.console.CBRPanel;
import cbr.pt.frontend.console.logger.Logger;

public class CBRApp extends JFrame implements IAppContextProvider {

	private static final long serialVersionUID = 1L;

	private CBRMenubar menuBar;

	private CBRToolbar toolBar;

	private CBRPanel panel;

	private CBRInfoPanel infoPanel;

	private CBRMainPanel mainPanel;

	private CBRAppContext appContext;

	private static CBRApp app;

	public static CBRApp getInstance() {
		if (app == null) {
			app = new CBRApp(new Config("default"));
		}
		return app;
	}

	public static Logger logger() {
		return getInstance().getLogger();
	}

	private Logger getLogger() {
		return getInfoPanel().getLogger();
	}

	private CBRApp(Config config) {
		appContext = new CBRAppContext(config, this);
		setTitle(config.toString());
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		addComponentsToPane();
		setSize(config.width, config.height);
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		setLocation((screenSize.width - config.width) / 2,
				(screenSize.height - config.height) / 2);
		setVisible(true);
	}

	public void init() {
		getContentPane().add(getPanel(), BorderLayout.CENTER);
		getPanel().init();
	}

	protected void addComponentsToPane() {
		setJMenuBar(getMenubar());
		addComponentsToContentPane(getContentPane());
	}

	protected void addComponentsToContentPane(Container container) {
		container.add(getToolbar(), BorderLayout.NORTH);
		container.add(getMainPanel(), BorderLayout.WEST);
		container.add(getInfoPanel(), BorderLayout.SOUTH);
	}

	protected CBRMainPanel getMainPanel() {
		if (mainPanel == null) {
			mainPanel = new CBRMainPanel(getAppContext());
		}
		return mainPanel;
	}

	protected CBRInfoPanel getInfoPanel() {
		if (infoPanel == null) {
			infoPanel = new CBRInfoPanel(getAppContext());
		}
		return infoPanel;
	}

	protected JToolBar getToolbar() {
		if (toolBar == null) {
			toolBar = new CBRToolbar(getAppContext());
		}
		return toolBar;
	}

	protected JMenuBar getMenubar() {
		if (menuBar == null) {
			menuBar = new CBRMenubar(getAppContext());
		}
		return menuBar;
	}

	protected CBRPanel getPanel() {
		if (panel == null) {
			panel = new CBRPanel(getAppContext());
		}
		return panel;
	}

	@Override
	public CBRAppContext getAppContext() {
		return appContext;
	}

	@Override
	public void setAppContext(CBRAppContext appContext) {
		this.appContext = appContext;
	}

}
