package cbr.pt.frontend.console;

import cbr.pt.frontend.Config;


public interface IConfigProvier {

	public Config getConfig();

	public void setConfig(Config config);

}
