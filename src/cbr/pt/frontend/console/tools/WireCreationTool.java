package cbr.pt.frontend.console.tools;

import cbr.geometry.IPoint;
import cbr.pt.frontend.console.CBRContext;
import cbr.pt.frontend.console.IAreaView;
import cbr.pt.frontend.console.IWireView;
import cbr.pt.frontend.console.editpolicy.AbstractEditPolicy;
import cbr.pt.frontend.console.editpolicy.WireCreationEditPolicy;
import cbr.pt.frontend.console.requests.WireCreationRequest;

public class WireCreationTool extends ReactivationEditPolicyMoveSupportTool {

	private WireCreationRequest request;

	private Class<? extends IAreaView> clazz;

	public WireCreationTool(CBRContext context, Class<? extends IAreaView> clazz) {
		super(context);
		addSupportedPolicyName(WireCreationEditPolicy.EDIT_POLICY_WIRE_CREATION);
		this.clazz = clazz;
	}

	@Override
	protected void activateViewPolicy(AbstractEditPolicy policy) {
		policy.setRequest(getRequest());
		policy.activate();
	}

	private WireCreationRequest getRequest() {
		if (request == null) {
			request = new WireCreationRequest(clazz);
		}
		return request;
	}

	@Override
	public void unselect() {
		super.unselect();
		request.dispose();
	}

	private boolean updateTargetLocation(IPoint location) {
		IWireView feedback = getRequest().getFeedback();
		if (feedback != null) {
			feedback.getTargetConnector().setLocation(location.copy());
			return true;
		}
		return false;
	}

	@Override
	public boolean handleMouseMoved(IAreaView view, IPoint location) {
		if (!super.handleMouseMoved(view, location)) {
			return updateTargetLocation(location);
		}
		return true;
	}

	@Override
	public boolean handleMouseEntered(IAreaView view, IPoint location) {
		if (!super.handleMouseEntered(view, location)) {
			return updateTargetLocation(location);
		}
		return true;
	}

	@Override
	public boolean handleMouseExited(IAreaView view, IPoint location) {
		if (!super.handleMouseExited(view, location)) {
			return updateTargetLocation(location);
		}
		return true;
	}

}
