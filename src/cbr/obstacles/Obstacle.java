package cbr.obstacles;

import cbr.geometry.IDimension;
import cbr.geometry.IPoint;
import cbr.geometry.IRectangle;
import cbr.geometry.Rectangle;

public class Obstacle extends Rectangle implements IObstacle {

	public Obstacle() {
		this(0, 0, 0, 0);
	}

	public Obstacle(IRectangle bounds) {
		this(bounds.getLocation(), bounds.getSize());
	}

	public Obstacle(IPoint location, IDimension size) {
		this(location.getX(), location.getY(), size.getWidth(), size
				.getHeight());
	}

	public Obstacle(int x, int y, int width, int height) {
		super(x, y, width, height);
	}

}
