package cbr.geometry.operators;

public interface IRectangleCoordsDependsOperator extends
		IPointCoordsDependsOperator, IDimensionCoordsDependsOperator {

	@Override
	public IRectangleCoordsDependsOperator change();

}
