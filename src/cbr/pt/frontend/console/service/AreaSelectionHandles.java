package cbr.pt.frontend.console.service;

import java.awt.Color;

import cbr.geometry.IDimension;
import cbr.geometry.IPoint;
import cbr.geometry.IRectangle;
import cbr.pt.frontend.console.AbstractView;
import cbr.pt.frontend.console.CBRGraphics;

public class AreaSelectionHandles extends AbstractView {

	@Override
	public void prePaint(CBRGraphics graphics) {
		graphics.setColor(Color.white);
		IRectangle bounds = getBounds();
		IPoint location = bounds.getLocation();
		IDimension size = bounds.getSize();
		graphics.drawRect(location.getX() + 1, location.getY() + 1,
				size.getWidth() - 2, size.getHeight() - 2);
		graphics.drawRect(location.getX() + 2, location.getY() + 2,
				size.getWidth() - 4, size.getHeight() - 4);
	}

}
