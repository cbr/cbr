package cbr.pt.frontend;

public interface IAppContextProvider {

	public CBRAppContext getAppContext();

	public void setAppContext(CBRAppContext context);

}
