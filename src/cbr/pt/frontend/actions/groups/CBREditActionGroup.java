package cbr.pt.frontend.actions.groups;

import cbr.pt.frontend.CBRActionGroup;
import cbr.pt.frontend.CBRAppContext;

public class CBREditActionGroup extends CBRActionGroup {

	public CBREditActionGroup(CBRAppContext appContext) {
		super(appContext, "Edit");
		addActionGroup(new CBRUndoRedoActionGroup(appContext));
		addActionGroup(new CBRRemoveCutPasteCopyGroup(appContext));
	}

}
