package cbr.grid;

import cbr.geometry.IRectangle;

/**
 * 
 * Grid bounded with rectangle.
 * 
 * @author Alexander Strakh
 * 
 */
public interface IBoundedGrid<T, I> extends IGrid<T, I>, IRectangle {

	public IRectangle getBounds();

	public void setBounds(IRectangle bounds);

}
