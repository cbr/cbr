package cbr.pt.frontend.console.views;

import cbr.grid.IObstaclesBoundedGrid;
import cbr.pt.frontend.console.CBRConsole;
import cbr.pt.frontend.console.CBRModel;
import cbr.pt.frontend.console.IAreaView;
import cbr.pt.frontend.console.IWireView;
import cbr.pt.frontend.console.WireLayer;
import cbr.pt.frontend.console.views.editpolicy.ModelSpecificExcludingSelectionEditPolicy;
import cbr.pt.frontend.console.views.editpolicy.ZoneAndDeviceCreationEditPolicy;
import cbr.pt.router.CBRouter;

public class Model extends CBRModel {

	public Model() {
		addEditPolicy(new ZoneAndDeviceCreationEditPolicy());
		addEditPolicy(new ModelSpecificExcludingSelectionEditPolicy());
	}

	public WireLayer getWireLayer() {
		return getParent() == null ? null : ((CBRConsole) getParent())
				.getWireLayer();
	}

	@Override
	public void setParent(IAreaView parent) {
		super.setParent(parent);
		for (IAreaView view : getChildrens()) {
			if (view instanceof Zone) {
				((Zone) view).updateGridByConstraintChanges();
			}
		}
	}

	public IObstaclesBoundedGrid getGrid() {
		WireLayer layer = getWireLayer();
		return layer == null ? null : layer.getGrid();
	}

	@Override
	protected boolean addChild(int index, IAreaView child) {
		if (super.addChild(index, child)) {
			if (child instanceof Device) {
				getRouter().addObstacleByConstraint(child);
			}
			return true;
		}
		return false;
	}

	@Override
	public boolean removeChild(IAreaView child) {
		if (super.removeChild(child)) {
			if (child instanceof Device) {
				getRouter().removeObstacleByConstraint(child);
			} else if (child instanceof IWireView) {
				getWireLayer().updateWire((IWireView) child);
			}
			return true;
		}
		return false;
	}

	public CBRouter getRouter() {
		WireLayer layer = getWireLayer();
		return layer == null ? null : layer.getRouter();
	}

}
