package cbr.geometry.operators;

public interface IDimensionCoordsDependsOperator extends ICoordsDependsOperator {

	public void setSize(int size);

	public int getSize();

	@Override
	public IDimensionCoordsDependsOperator change();

}
