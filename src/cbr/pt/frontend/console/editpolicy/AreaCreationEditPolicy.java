package cbr.pt.frontend.console.editpolicy;

import cbr.geometry.IDimension;
import cbr.geometry.IPoint;
import cbr.geometry.IRectangle;
import cbr.geometry.Rectangle;
import cbr.pt.frontend.command.CommandsFactory;
import cbr.pt.frontend.console.IAreaView;
import cbr.pt.frontend.console.service.AreaCreationHandles;

public class AreaCreationEditPolicy extends
		DragSupportReactivatedCreationEditPolicy {

	public static final String EDIT_POLICY_AREA_CREATION = "Area creation edit policy";

	public AreaCreationEditPolicy() {
		super(EDIT_POLICY_AREA_CREATION);
	}

	@Override
	public void startDrag(IPoint location) {
		super.startDrag(location);
		getServiceView().setBounds(
				new Rectangle(location.getX(), location.getY(), 0, 0));
		addServiceViewToLayer();
		showServiceView();
	}

	@Override
	public void finishDrag(IPoint location) {
		super.finishDrag(location);
		hideServiceView();
	}

	@Override
	protected void updateViewBounds(IAreaView view) {
	}

	@Override
	protected void tryToApplyServiceView(IPoint delta) {
		applyDelta(getServiceView().getBounds(), delta);
	}

	@Override
	protected void tryToCommit(IPoint delta) {
		applyDelta(getServiceView().getBounds(), delta);
		CommandsFactory.createAndExecute("CreateAreaCommand", getContext(),
				"Create area", getHost(), getCurrentClass(), getServiceView()
						.getBounds());
	}

	@Override
	public IAreaView createServiceView() {
		return new AreaCreationHandles();
	}

	private static void applyDelta(IRectangle bounds, IPoint delta) {
		IDimension size = bounds.getSize();
		if (delta.getX() > 0) {
			size.setWidth(delta.getX());
		}
		if (delta.getY() > 0) {
			size.setHeight(delta.getY());
		}
	}

}
