package cbr.pt.frontend.console;

import cbr.serialize.xml.annotations.XMLNode;
import cbr.serialize.xml.annotations.XMLTransient;

@XMLNode("model")
@XMLTransient("bounds")
public interface IModel extends IAreaView {

}
