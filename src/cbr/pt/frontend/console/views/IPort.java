package cbr.pt.frontend.console.views;

import cbr.pt.frontend.console.IFeatureView;
import cbr.serialize.xml.annotations.XMLNode;

@XMLNode("port")
public interface IPort extends IFeatureView {

}
