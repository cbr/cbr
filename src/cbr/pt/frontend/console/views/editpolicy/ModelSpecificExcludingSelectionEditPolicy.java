package cbr.pt.frontend.console.views.editpolicy;

import cbr.pt.frontend.console.editpolicy.SelectionEditPolicy;
import cbr.pt.frontend.console.views.Model;

public class ModelSpecificExcludingSelectionEditPolicy extends
		SelectionEditPolicy {

	public ModelSpecificExcludingSelectionEditPolicy() {
		addNotSelectedClass(Model.class);
	}

}
