package cbr.pt.frontend.console;

import java.awt.Color;

public class CBRZone extends AbstractView implements IZone {

	public static final int ORDER_ZONE = CBRModel.ORDER_MODEL + 1;

	public CBRZone() {
		this(0, 0, 0, 0);
	}

	public CBRZone(int x, int y, int width, int height) {
		setOrder(ORDER_ZONE);
		getLocation().setX(x);
		getLocation().setY(y);
		getSize().setWidth(width);
		getSize().setHeight(height);
		setColor(Color.red);
	}

	@Override
	public void postPaint(CBRGraphics g) {
		g.drawRect(getBounds());
	}

}
