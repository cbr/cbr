package cbr.geometry.operators;

public interface ICoordsDependsOperator {

	public ICoordsDependsOperator change();

}
