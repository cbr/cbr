package cbr.pt.frontend;

import javax.swing.Action;
import javax.swing.JToggleButton;
import javax.swing.border.EmptyBorder;

public class CBRToggleButton extends JToggleButton {

	private static final long serialVersionUID = 1L;

	public CBRToggleButton(Action act, String tip) {
		super(act);
		setText(null);
		setBorder(new EmptyBorder(2, 2, 2, 2));
		setToolTipText(tip);
		if (act instanceof CBRAction) {
			setDisabledIcon(((CBRAction) act).getDisabledIcon());
		}
	}
	
	protected void actionPropertyChanged(Action action, String propertyName) {
		super.actionPropertyChanged(action, propertyName);
		if(propertyName.equals(CBRAction.PROPERTY_ISACTIVE)) {
			if(action instanceof CBRAction) {
				setSelected(((CBRAction) action).isChecked());
			}
		}
	}

}
