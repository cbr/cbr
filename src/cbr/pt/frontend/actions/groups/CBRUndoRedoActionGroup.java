package cbr.pt.frontend.actions.groups;

import cbr.pt.frontend.CBRActionGroup;
import cbr.pt.frontend.CBRAppContext;
import cbr.pt.frontend.actions.CBRActionRedo;
import cbr.pt.frontend.actions.CBRActionUndo;

public class CBRUndoRedoActionGroup extends CBRActionGroup {

	public CBRUndoRedoActionGroup(CBRAppContext appContext) {
		super(appContext, "Undo/Redo");
		addAction(new CBRActionUndo(appContext));
		addAction(new CBRActionRedo(appContext));
	}

}
