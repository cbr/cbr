package cbr.pt.frontend.console.editpolicy;

import cbr.geometry.IPoint;
import cbr.geometry.IRectangle;
import cbr.geometry.Point;
import cbr.pt.frontend.console.IAreaView;

public abstract class DragSupportReactivatedCreationEditPolicy extends
		AbstractEditPolicy {

	private IPoint startDragLocation;

	private Class<? extends IAreaView> clazz;

	public DragSupportReactivatedCreationEditPolicy(String name) {
		super(name);
	}

	@Override
	public void processDrag(IPoint location) {
		tryToApplyServiceView(getDelta(location));
	}

	@Override
	public void finishDrag(IPoint location) {
		tryToCommit(getDelta(location));
	}

	@Override
	public void startDrag(IPoint location) {
		startDragLocation = location.copy();
	}

	@Override
	public void deactivate() {
		removeServiceViewFromLayer();
	}

	@Override
	public void activate() {
		addServiceViewToLayer();
	}

	@Override
	protected void updateViewBounds(IAreaView view) {
		super.updateViewBounds(view);
		IAreaView host = getHost();
		IRectangle bounds = host.getBounds().copy();
		IAreaView parent = host.getParent();
		if (parent != null)
			parent.translateToAbsolute(bounds);
		view.setBounds(bounds);
	}

	private IPoint getDelta(IPoint location) {
		return new Point(location.getX() - startDragLocation.getX(),
				location.getY() - startDragLocation.getY());
	}

	public void setCurrentClass(Class<? extends IAreaView> clazz) {
		this.clazz = clazz;
	}

	protected Class<? extends IAreaView> getCurrentClass() {
		return clazz;
	}

	protected abstract void tryToApplyServiceView(IPoint delta);

	protected abstract void tryToCommit(IPoint delta);

}
