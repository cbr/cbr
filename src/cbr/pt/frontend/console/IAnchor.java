package cbr.pt.frontend.console;

public interface IAnchor {

	public IAnchorPointLocator getLocator();

	public boolean addSourceConnector(IConnector connector);

	public boolean removeTargetConnector(IConnector connector);

	public boolean addTargetConnector(IConnector connector);

	public boolean removeSourceConnector(IConnector connector);

}
