package cbr.pt.frontend.console;

import cbr.serialize.xml.annotations.XMLAttribute;
import cbr.serialize.xml.annotations.XMLNode;

@XMLNode("featureView")
public interface IFeatureView extends IIdentifiedAnchorView {

	@XMLAttribute
	public int getSide();

	public void setSide(int side);

}
