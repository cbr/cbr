package cbr.pt.frontend.console;

import java.util.ArrayList;
import java.util.List;

public abstract class IdentifiedAnchorProvidedAbstractView extends
		IdentifiedAbstractView implements IIdentifiedAnchorView {

	private IAnchorPointLocator locator;

	private List<IConnector> sourceConnectors = new ArrayList<IConnector>();

	private List<IConnector> targetConnectors = new ArrayList<IConnector>();

	public IdentifiedAnchorProvidedAbstractView(int id) {
		super(id);
	}

	@Override
	public IAnchorPointLocator getLocator() {
		if (locator == null) {
			locator = createLocator();
		}
		return locator;
	}

	protected abstract IAnchorPointLocator createLocator();

	@Override
	public boolean addSourceConnector(IConnector connector) {
		if (!sourceConnectors.contains(connector)
				&& sourceConnectors.add(connector)) {
			connector.update();
			return true;
		}
		return false;
	}

	@Override
	public boolean removeTargetConnector(IConnector connector) {
		if (targetConnectors.remove(connector)) {
			return true;
		}
		return false;
	}

	@Override
	public boolean addTargetConnector(IConnector connector) {
		if (!targetConnectors.contains(connector)
				&& targetConnectors.add(connector)) {
			connector.update();
			return true;
		}
		return false;
	}

	@Override
	public boolean removeSourceConnector(IConnector connector) {
		if (sourceConnectors.remove(connector)) {
			return true;
		}
		return false;
	}

	@Override
	protected void boundsChanged() {
		super.boundsChanged();
		update();
	}

	protected static void updateAnchors(IAreaView view) {
		if (view instanceof IdentifiedAnchorProvidedAbstractView) {
			((IdentifiedAnchorProvidedAbstractView) view).updateConnections();
		}
		for (Object childView : view.getChildrens()) {
			updateAnchors((IAreaView) childView);
		}
	}

	private void updateConnections() {
		for (IConnector connector : sourceConnectors) {
			connector.update();
		}
		for (IConnector connector : targetConnectors) {
			connector.update();
		}
	}

	private void update() {
		updateAnchors(this);
	}

	public List<IConnector> getSourceConnectors() {
		return sourceConnectors;
	}

	public List<IConnector> getTargetConnectors() {
		return sourceConnectors;
	}

}
