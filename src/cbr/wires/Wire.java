package cbr.wires;

import java.util.ArrayList;
import java.util.List;

import cbr.geometry.IPoint;
import cbr.grid.ICell;
import cbr.grid.ISide;

public class Wire implements IWire {

	private int sourceSideIndex;

	private int targetSideIndex;

	private ICell sourceCell;

	private ICell targetCell;

	private IPoint sourcePoint;

	private IPoint targetPoint;

	private boolean isNeedUpdatePath = false;

	private List<IPoint> path = new ArrayList<IPoint>();

	public Wire() {
	}

	@Override
	public ISide getSourceSide() {
		return sourceCell.getSide(sourceSideIndex);
	}

	@Override
	public ISide getTargetSide() {
		return targetCell.getSide(targetSideIndex);
	}

	@Override
	public void dispose() {
		sourcePoint = null;
		targetPoint = null;
	}

	@Override
	public IPoint getSourcePoint() {
		return sourcePoint;
	}

	@Override
	public IPoint getTargetPoint() {
		return targetPoint;
	}

	public void setSourcePoint(IPoint sourcePoint) {
		this.sourcePoint = sourcePoint;
	}

	public void setTargetPoint(IPoint targetPoint) {
		this.targetPoint = targetPoint;
	}

	@Override
	public List<IPoint> getPath() {
		return path;
	}

	@Override
	public ICell getSourceCell() {
		return sourceCell;
	}

	@Override
	public ICell getTargetCell() {
		return targetCell;
	}

	@Override
	public void setNeedUpdatePath() {
		isNeedUpdatePath = true;
	}

	@Override
	public boolean isNeedUpdatePath() {
		return isNeedUpdatePath;
	}

	@Override
	public void pathUpdated() {
		this.isNeedUpdatePath = false;
	}

	@Override
	public void setSourceSideIndex(int sideIndex) {
		sourceSideIndex = sideIndex;
	}

	@Override
	public void setTargetSideIndex(int sideIndex) {
		targetSideIndex = sideIndex;
	}

	@Override
	public void setSourceCell(ICell cell) {
		sourceCell = cell;
	}

	@Override
	public void setTargetCell(ICell cell) {
		targetCell = cell;
	}

	@Override
	public int getSourceSideIndex() {
		return sourceSideIndex;
	}

	@Override
	public int getTargetSideIndex() {
		return targetSideIndex;
	}

	@Override
	public boolean isValid() {
		return sourceCell != null && targetCell != null && sourcePoint != null
				&& targetPoint != null;
	}

}
