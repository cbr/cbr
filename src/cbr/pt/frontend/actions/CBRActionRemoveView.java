package cbr.pt.frontend.actions;

import java.awt.event.ActionEvent;

import cbr.pt.frontend.CBRAppContext;
import cbr.pt.frontend.command.CommandsFactory;
import cbr.pt.frontend.console.CBRContext;
import cbr.pt.frontend.console.IAreaView;

public class CBRActionRemoveView extends CBRActionSelectedListener {

	private static final long serialVersionUID = 1L;

	public final static String ACTION_NAME = "Delete view";

	public final static String ACTION_TIP = "Delete view";

	public final static String ACTION_ICON = "delete.png";

	public CBRActionRemoveView(CBRAppContext appContext) {
		super(appContext, ACTION_NAME, ACTION_TIP, ACTION_ICON);
	}

	@Override
	public void actionPerformed(ActionEvent event) {
		CBRContext context = getAppContext().getConfig().getContext();
		IAreaView selectedArea = context.getSelection();
		context.removeSelection();
		CommandsFactory.createAndExecute("DeleteCommand", context,
				"Remove view", selectedArea);
	}

}
