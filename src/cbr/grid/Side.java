package cbr.grid;

public class Side implements ISide {

	private ICell neighbour1;

	private ICell neighbour2;

	public Side(ICell neighbour1, ICell neighbour2) {
		this.neighbour1 = neighbour1;
		this.neighbour2 = neighbour2;
	}

	@Override
	public ICell getNeighbour1() {
		return neighbour1;
	}

	@Override
	public ICell getNeighbour2() {
		return neighbour2;
	}

	@Override
	public void setNeighbour1(ICell cell) {
		this.neighbour1 = cell;
	}

	@Override
	public void setNeighbour2(ICell cell) {
		this.neighbour2 = cell;
	}

	@Override
	public ICell getNeighbour(ICell cell) {
		if (cell == null)
			throw new UnsupportedOperationException(
					"Can't determine neigbour for null cell!");
		if (cell == neighbour1)
			return neighbour2;
		if (cell == neighbour2)
			return neighbour1;
		throw new UnsupportedOperationException(
				"This side not belongs to specified cell.");
	}

}
