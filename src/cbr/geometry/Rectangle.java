package cbr.geometry;

public class Rectangle implements IRectangle {

	private IPoint location;

	private IDimension size;

	public Rectangle() {
		this(0, 0, 0, 0);
	}

	public Rectangle(IPoint location, IDimension size) {
		this(location.getX(), location.getY(), size.getWidth(), size
				.getHeight());
	}

	public Rectangle(int x, int y, int width, int height) {
		location = createPoint(x, y);
		size = createDimension(width, height);
	}

	@Override
	public int getX() {
		return getLocation().getX();
	}

	@Override
	public void setX(int x) {
		getLocation().setX(x);
	}

	@Override
	public int getY() {
		return getLocation().getY();
	}

	@Override
	public void setY(int y) {
		getLocation().setY(y);
	}

	@Override
	public int getWidth() {
		return getSize().getWidth();
	}

	@Override
	public void setWidth(int width) {
		getSize().setWidth(width);
	}

	@Override
	public int getHeight() {
		return getSize().getHeight();
	}

	@Override
	public void setHeight(int height) {
		getSize().setHeight(height);
	}

	@Override
	public IPoint getLocation() {
		return location;
	}

	@Override
	public IDimension getSize() {
		return size;
	}

	@Override
	public void setLocation(IPoint location) {
		this.location = location;
	}

	@Override
	public void setSize(IDimension size) {
		this.size = size;
	}

	@Override
	public String toString() {
		return "Rectangle(" + getX() + ", " + getY() + ", " + getWidth() + ", "
				+ getHeight() + ")";
	}

	@Override
	public void dispose() {
		// location = null;
		// size = null;
	}

	protected IPoint createPoint(int x, int y) {
		return new Point(x, y);
	}

	protected IDimension createDimension(int width, int height) {
		return new Dimension(width, height);
	}

	@Override
	public boolean isContains(IPoint point) {
		return isContains(point.getX(), point.getY());
	}

	@Override
	public boolean isContains(int x, int y) {
		return Geometry.isContains(x, y, getX(), getY(), getWidth(),
				getHeight());
	}

	@Override
	public void translateTo(ITranslatable translatable) {
		getLocation().translateTo(translatable);
	}

	@Override
	public void translateFrom(ITranslatable translatable) {
		getLocation().translateFrom(translatable);
	}

	@Override
	public boolean equals(Object o) {
		if (o instanceof IRectangle) {
			IRectangle p = (IRectangle) o;
			return p.getLocation().equals(getLocation())
					&& p.getSize().equals(getSize());
		}
		return false;
	}

	@Override
	public IRectangle copy() {
		return new Rectangle(getX(), getY(), getWidth(), getHeight());
	}

	@Override
	public IPoint getCenter() {
		IPoint location = getLocation();
		IDimension size = getSize();
		return new Point(location.getX() + size.getWidth() / 2, location.getY()
				+ size.getHeight() / 2);
	}

}
