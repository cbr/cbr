package cbr.pt.frontend.command;

public interface ICommandStackListener {

	public void handleCommandStackChanged(CommandStack commandStack, int event);

}
