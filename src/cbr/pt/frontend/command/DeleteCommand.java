package cbr.pt.frontend.command;

import java.util.ArrayList;
import java.util.List;

import cbr.pt.frontend.console.CBRContext;
import cbr.pt.frontend.console.IAreaView;
import cbr.pt.frontend.console.IConnector;
import cbr.pt.frontend.console.IWireView;
import cbr.pt.frontend.console.IdentifiedAnchorProvidedAbstractView;

public class DeleteCommand extends CompoundCommand {

	protected DeleteCommand(CBRContext context, String description,
			IAreaView view) {
		super(context, description);
		List<IWireView> wires = new ArrayList<IWireView>();
		List<IAreaView> areas = new ArrayList<IAreaView>();
		fill(view, wires, areas);
		for (IWireView wire : wires) {
			addCommand(new DeleteWireCommand(context, description, wire));
		}
		for (IAreaView area : areas) {
			addCommand(new DeleteAreaCommand(context, description, area));
		}
	}

	private static void fill(IAreaView view, List<IWireView> wires,
			List<IAreaView> areas) {
		if (view instanceof IWireView) {
			if (!wires.contains(view)) {
				wires.add((IWireView) view);
				return;
			}
		}
		if (view instanceof IdentifiedAnchorProvidedAbstractView) {
			for (IConnector connector : ((IdentifiedAnchorProvidedAbstractView) view)
					.getSourceConnectors()) {
				if (!wires.contains(connector.getOwner())) {
					wires.add(connector.getOwner());
				}
			}
			for (IConnector connector : ((IdentifiedAnchorProvidedAbstractView) view)
					.getTargetConnectors()) {
				if (!wires.contains(connector.getOwner())) {
					wires.add(connector.getOwner());
				}
			}
		}
		for (IAreaView childView : view.getChildrens()) {
			fill(childView, wires, areas);
		}
		if (!areas.contains(view)) {
			areas.add(view);
		}
	}

}
