package cbr.pt.router;

import cbr.AbstractChannelBasedRouter;
import cbr.channels.manager.AbstractChannelsManager;
import cbr.geometry.IRectangle;
import cbr.obstacles.AbstractObstaclesManager;
import cbr.pt.frontend.console.IAreaView;
import cbr.pt.frontend.console.IWireView;
import cbr.wires.AbstractWireManager;
import cbr.wires.IWire;

public class CBRouter extends AbstractChannelBasedRouter<IWireView, IAreaView> {

	@Override
	protected AbstractChannelsManager<IWireView, IAreaView> createChannelsManager() {
		return new CBRChannelsManager(getGrid());
	}

	@Override
	protected AbstractObstaclesManager<IAreaView, IWireView> createObstaclesManager() {
		return new CBRObstaclesManager(channelsManager, getGrid());
	}

	@Override
	protected AbstractWireManager<IWireView, IAreaView> createWireManager() {
		return new CBRWireManager(this, channelsManager);
	}

	@Override
	protected void updateWireConstraint(IWireView connection, IWire wire) {
		connection.getPath().clear();
		connection.getPath().addAll(wire.getPath());
	}

	@Override
	public IRectangle getBounds(IAreaView areaView) {
		return areaView.getBounds().copy();
	}

	@Override
	public void showErrorMessage(String message) {
		// TODO:
	}

}
