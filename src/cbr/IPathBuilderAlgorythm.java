package cbr;

import cbr.channels.IChannel;
import cbr.wires.IWire;

public interface IPathBuilderAlgorythm {

	abstract public void process(IChannel channel, IWire wire);

}
