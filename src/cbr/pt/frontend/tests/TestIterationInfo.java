package cbr.pt.frontend.tests;

public class TestIterationInfo {

	private long time;

	private boolean isProcessed;

	public TestIterationInfo(long time, boolean isProcessed) {
		this.time = time;
		this.isProcessed = isProcessed;
	}

	public boolean isProcessed() {
		return isProcessed;
	}

	public long getTime() {
		return time;
	}

}
