package cbr.pt.frontend.tests;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.xml.sax.SAXException;

import cbr.pt.frontend.CBRApp;
import cbr.pt.frontend.actions.CBRModelActionUtils;
import cbr.pt.frontend.command.ICommand;
import cbr.pt.frontend.console.CBRContext;

public class CommandTest {

	public static final int DEFAULT_TEST_REPEAT_COUNT = 10;

	private int repeatLimit = DEFAULT_TEST_REPEAT_COUNT;

	private List<TestIterationInfo> iterations = new ArrayList<TestIterationInfo>();

	private CBRContext context;

	private ICommand command;

	private String modelPath;

	public CommandTest(CBRContext context, ICommand command, String modelPath) {
		this.command = command;
		this.context = context;
		this.modelPath = modelPath;
	}

	public void run() {
		for (int i = 0; i < repeatLimit; i++) {
			CBRApp.logger().info("Test iteration: " + i);
			command.disconnectFromContext();
			CBRModelActionUtils.disposeModel(context.getAppContext());
			command.connectToContext(context);
			boolean processed = false;
			long startTime = System.currentTimeMillis();
			try {
				context.getConfig().filePath = modelPath;
				context.restoreCBRModel();
				processed = process();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (InvocationTargetException e) {
				e.printStackTrace();
			} catch (InstantiationException e) {
				e.printStackTrace();
			} catch (ParserConfigurationException e) {
				e.printStackTrace();
			} catch (TransformerException e) {
				e.printStackTrace();
			} catch (SAXException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			long endTime = System.currentTimeMillis();
			iterations
					.add(new TestIterationInfo(endTime - startTime, processed));
			CBRApp.logger().info(
					"Test iteration " + i + " finished after "
							+ (endTime - startTime) + "ms");
		}
	}

	protected boolean process() {
		context.getCommandStack().execute(command);
		return true;
	}

	public long getAverageTime() {
		long time = 0;
		int successTests = 0;
		for (TestIterationInfo testIterationInfo : iterations) {
			if (testIterationInfo.isProcessed()) {
				successTests++;
				time += testIterationInfo.getTime();
			}
		}
		return time / ((long) successTests);
	}

	public void setModelPath(String modelPath) {
		this.modelPath = modelPath;
	}

}
