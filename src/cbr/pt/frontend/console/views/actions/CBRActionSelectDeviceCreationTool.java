package cbr.pt.frontend.console.views.actions;

import cbr.pt.frontend.CBRAppContext;
import cbr.pt.frontend.actions.CBRActionCreationTool;
import cbr.pt.frontend.console.views.tools.DeviceCreationTool;

public class CBRActionSelectDeviceCreationTool extends CBRActionCreationTool {

	private static final long serialVersionUID = 1L;

	public final static String ACTION_NAME = "Create device";

	public final static String ACTION_TIP = "Create device";

	public final static String ACTION_ICON = "create_device.png";

	public CBRActionSelectDeviceCreationTool(CBRAppContext appContext) {
		super(appContext, ACTION_NAME, ACTION_TIP, ACTION_ICON,
				new DeviceCreationTool(appContext.getConfig().getContext()));
	}

}
