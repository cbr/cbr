package cbr.pt.frontend.console.editpolicy;

public class MoveSupportReactivatedCreationEditPolicy extends
		AbstractEditPolicy {

	public MoveSupportReactivatedCreationEditPolicy(String name) {
		super(name);
	}

	@Override
	public void activate() {
		addFeedbackViewToLayer();
		addServiceViewToLayer();
	}

	@Override
	public void deactivate() {
		removeFeedbackFromLayer();
		removeServiceViewFromLayer();
		disposeFeedbackView();
		disposeServiceView();
	}

}
