package cbr.pt.frontend.command;

import cbr.geometry.IRectangle;
import cbr.pt.frontend.console.CBRContext;
import cbr.pt.frontend.console.IAreaView;
import cbr.pt.frontend.console.IIdentified;

public class CreateAreaCommand extends AbstractCommand {

	private IAreaView parent;

	private IRectangle bounds;

	private Class<? extends IAreaView> clazz;

	private IAreaView child;

	private int parentId = UNDEFINED;

	protected CreateAreaCommand(CBRContext context, String description,
			IAreaView parent, Class<? extends IAreaView> clazz,
			IRectangle bounds) {
		super(context, description);
		this.clazz = clazz;
		this.bounds = bounds.copy();
		this.parent = parent;
		parent.translateToRelative(this.bounds);
	}

	protected IAreaView getInstance() {
		if (child == null) {
			child = getContext().getFactory().create(clazz);
			child.setBounds((IRectangle) bounds.copy());
		}
		return child;
	}

	@Override
	public void execute() {
		parent.addChild(getInstance());
	}

	@Override
	public void undo() {
		parent.removeChild(getInstance());
	}

	@Override
	public void dispose() {
		parent = null;
		bounds = null;
		clazz = null;
		child = null;
	}

	@Override
	public void disconnectFromContext() {
		if (parent instanceof IIdentified) {
			parentId = ((IIdentified) parent).getId();
		} else
			throw new UnsupportedOperationException(
					"Errors due to disconnect from context for CreateAreaCommand. Parent: \""
							+ parent.getClass().getSimpleName()
							+ "\" should be implementation of IIdentified.");
		super.disconnectFromContext();
	}

	@Override
	public void connectToContext(CBRContext context) {
		super.connectToContext(context);
		parent = findModelById(parentId);
		if (parent != null)
			parentId = UNDEFINED;
	}

}
