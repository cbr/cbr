package cbr.pt.frontend.console.views.editpolicy;

import cbr.pt.frontend.console.editpolicy.AreaCreationEditPolicy;
import cbr.pt.frontend.console.views.Device;
import cbr.pt.frontend.console.views.Zone;

public class ZoneAndDeviceCreationEditPolicy extends AreaCreationEditPolicy {

	public ZoneAndDeviceCreationEditPolicy() {
		addSupportedClasses(Device.class);
		addSupportedClasses(Zone.class);
	}

}
