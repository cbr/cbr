package cbr.pt.frontend;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.Icon;

public class CBRAction extends AbstractAction implements IAppContextProvider {

	public static String PROPERTY_ISACTIVE = "isActive";

	private static final long serialVersionUID = 1L;

	private CBRAppContext appContext;

	private String tip;

	private Icon disabledIcon;

	private boolean isChecked = false;

	private boolean isCheckAction = false;

	public CBRAction(CBRAppContext appContext, String name, String tip,
			String iconPath) {
		super(name, appContext.getEnabledIcon(iconPath));
		disabledIcon = appContext.getDisabledIcon(iconPath);
		this.tip = tip;
		this.appContext = appContext;
	}

	public void setChecked(boolean isChecked) {
		if (this.isChecked != isChecked) {
			this.isChecked = isChecked;
			firePropertyChange(PROPERTY_ISACTIVE, !isChecked, isChecked);
		}
	}

	public boolean isChecked() {
		return isChecked;
	}

	protected void setCheckAction() {
		this.isCheckAction = true;
	}

	public boolean isCheckAction() {
		return isCheckAction;
	}

	public Icon getDisabledIcon() {
		return disabledIcon;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (isCheckAction()) {
			setChecked(!isChecked());
			checkPerformed();
		}
	}

	protected void checkPerformed() {

	}

	@Override
	public CBRAppContext getAppContext() {
		return appContext;
	}

	@Override
	public void setAppContext(CBRAppContext appContext) {
		this.appContext = appContext;
	}

	public String getTip() {
		return tip;
	}

}
