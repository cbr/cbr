package cbr.pt.frontend;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import cbr.FileUtils;
import cbr.pt.ResourcesUtils;
import cbr.pt.frontend.console.AbstractViewFactory;
import cbr.pt.frontend.console.CBRContext;
import cbr.pt.frontend.console.Version;
import cbr.pt.frontend.console.views.ViewFactory;

public class Config {

	public static final String EL_TITLE = "title";

	public static final String EL_AUTO_LOAD_MODEL = "autoLoadModel";

	public String title = "Channel Based Router test";

	public Version version;

	public String filePath = null;

	public String autoLoadModel = null;

	public String configPath = null;

	public List<String> models = new ArrayList<String>();

	private CBRContext context;

	public int width = 1024;

	public int height = 768;

	public int iconSize = 32;

	public Config(String configPath) {
		this(null, configPath);
	}

	public Config(AbstractViewFactory factory, String configPath) {
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		width = screenSize.width / 3 * 2;
		height = screenSize.height / 3 * 2;
		this.configPath = ResourcesUtils.getXMLConfigInResurcesPath(configPath);
		init();
		context = new CBRContext(this, factory == null ? new ViewFactory()
				: factory);
		models.addAll(getInnerModels());
	}

	public static String getTestPath(String modelName) {
		return getPath(modelName,
				ResourcesUtils.getXMLTestInResurcesPath(modelName));
	}

	public static String getModelPath(String modelName) {
		return getPath(modelName,
				ResourcesUtils.getXMLModelInResurcesPath(modelName));
	}

	public static String getPath(String modelName, String absPath) {
		File file = new File(absPath);
		String model = file.getAbsolutePath();

		File currDir = new File(".");
		String curAbsPath = currDir.getAbsolutePath();
		model = currDir.getAbsolutePath().substring(0, curAbsPath.length() - 1)
				+ "bin" + File.separator + model;
		return model;
	}

	public static List<String> getInnerModels() {
		URL url = CBRAppContext.class.getResource("../../.."
				+ ResourcesUtils.FOLDER_MODELS);
		if (url == null) {
			throw new UnsupportedOperationException("Unimplemeneted yet!");
		} else {
			try {
				File dir = new File(url.toURI());
				List<String> models = new ArrayList<String>();
				for (File nextFile : dir.listFiles()) {
					String modelName = nextFile.getName().substring(0,
							nextFile.getName().length() - 4);
					models.add(modelName);
				}
				return models;
			} catch (URISyntaxException e) {
				e.printStackTrace();
			}
		}
		return Collections.emptyList();
	}

	public CBRContext getContext() {
		return context;
	}

	public String toString() {
		return title + " (" + version.toString() + ")";
	}

	private void init() {
		try {
			DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory
					.newInstance();
			DocumentBuilder documentBuilder = documentBuilderFactory
					.newDocumentBuilder();
			String configStr = FileUtils.readStream(CBRAppContext.class
					.getResourceAsStream(configPath));
			Document document = documentBuilder.parse(new InputSource(
					new StringReader(configStr)));
			Node rootNode = document.getDocumentElement();

			if (rootNode.getNodeType() == Node.ELEMENT_NODE) {
				NodeList nodeList = rootNode.getChildNodes();
				for (int i = 0; i < nodeList.getLength(); i++) {
					Node childNode = nodeList.item(i);
					if (childNode.getNodeType() == Node.ELEMENT_NODE) {
						String nodeName = childNode.getNodeName();
						if (nodeName.equalsIgnoreCase(Version.VERSION)) {
							NamedNodeMap namedNodeMap = childNode
									.getAttributes();
							int major = -1;
							int minor = -1;
							boolean alpha = true;
							boolean beta = false;
							for (int j = 0; j < namedNodeMap.getLength(); j++) {
								Node attrNode = namedNodeMap.item(j);
								if (attrNode.getNodeType() == Node.ATTRIBUTE_NODE) {
									String attrNodeName = attrNode
											.getNodeName();
									if (attrNodeName.equals(Version.MAJOR)) {
										major = Integer.parseInt(attrNode
												.getNodeValue());
									} else if (attrNodeName
											.equals(Version.MINOR)) {
										minor = Integer.parseInt(attrNode
												.getNodeValue());
									} else if (attrNodeName
											.equals(Version.ALPHA)) {
										alpha = Boolean.parseBoolean(attrNode
												.getNodeValue());
									} else if (attrNodeName
											.equals(Version.BETA)) {
										beta = Boolean.parseBoolean(attrNode
												.getNodeValue());
									}
								}
							}
							version = new Version(major, minor, alpha, beta);
						} else if (nodeName.equalsIgnoreCase(EL_TITLE)) {
							title = childNode.getFirstChild().getNodeValue();
						} else if (nodeName
								.equalsIgnoreCase(EL_AUTO_LOAD_MODEL)) {
							autoLoadModel = childNode.getFirstChild()
									.getNodeValue();
						}
					}
				}
			}

		} catch (IOException e) {
			e.printStackTrace();
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		}
	}

	public List<String> getModels() {
		return models;
	}

	public static List<String> getInnerTests() {
		URL url = CBRAppContext.class.getResource("../../.."
				+ ResourcesUtils.FOLDER_TESTS);
		if (url == null) {
			throw new UnsupportedOperationException("Unimplemeneted yet!");
		} else {
			try {
				File dir = new File(url.toURI());
				List<String> tests = new ArrayList<String>();
				for (File nextFile : dir.listFiles()) {
					String testName = nextFile.getName().substring(0,
							nextFile.getName().length() - 4);
					tests.add(testName);
				}
				return tests;
			} catch (URISyntaxException e) {
				e.printStackTrace();
			}
		}
		return Collections.emptyList();
	}

}
