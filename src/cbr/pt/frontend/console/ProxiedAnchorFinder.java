package cbr.pt.frontend.console;

import java.util.ArrayList;
import java.util.List;

public class ProxiedAnchorFinder implements IIdentifiedAnchor {

	private int id;

	private IConnector connector;

	private List<IConnector> sourceConnectors = new ArrayList<IConnector>();

	private List<IConnector> targetConnectors = new ArrayList<IConnector>();

	private IAnchorPointLocator dummyAnchorPointLocator = new IAnchorPointLocator() {
		@Override
		public int locate(IConnector connector) {
			return connector.getSide();
		}
	};

	public ProxiedAnchorFinder(IConnector connector) {
		this.connector = connector;
	}

	public ProxiedAnchorFinder(IConnector connector, int id) {
		this.id = id;
		this.connector = connector;
	}

	private IIdentifiedAnchorView getAndReplaceOriginalAnchor() {
		if (connector == null || connector.getOwner() == null)
			return null;
		IIdentifiedAnchorView anchor = getAnchorById(connector.getOwner()
				.getRoot(), getId());
		if (anchor != null) {
			connector.disconnect();
			connector.connect(anchor);
			return anchor;
		}
		return null;
	}

	private IIdentifiedAnchorView getAnchorById(IAreaView view, int id) {
		if (view instanceof IIdentifiedAnchorView
				&& ((IIdentifiedAnchorView) view).getId() == id)
			return (IIdentifiedAnchorView) view;
		for (Object childView : view.getChildrens()) {
			IIdentifiedAnchorView anchorView = getAnchorById(
					(IAreaView) childView, id);
			if (anchorView != null)
				return anchorView;
		}
		return null;
	}

	@Override
	public IAnchorPointLocator getLocator() {
		IIdentifiedAnchorView anchor = getAndReplaceOriginalAnchor();
		return anchor == null ? dummyAnchorPointLocator : anchor.getLocator();
	}

	@Override
	public int getId() {
		return id;
	}

	@Override
	public void setId(int id) {
		this.id = id;
	}

	@Override
	public boolean addSourceConnector(IConnector connector) {
		IIdentifiedAnchorView anchor = getAndReplaceOriginalAnchor();
		if (anchor == null) {
			if (!sourceConnectors.contains(anchor)) {
				return sourceConnectors.add(connector);
			}
			return false;
		}
		for (IConnector sourceConnector : sourceConnectors) {
			anchor.addSourceConnector(sourceConnector);
		}
		return anchor.addSourceConnector(connector);
	}

	@Override
	public boolean addTargetConnector(IConnector connector) {
		IIdentifiedAnchorView anchor = getAndReplaceOriginalAnchor();
		if (anchor == null) {
			if (!targetConnectors.contains(anchor)) {
				return targetConnectors.add(connector);
			}
			return false;
		}
		for (IConnector targetConnector : targetConnectors) {
			anchor.addTargetConnector(targetConnector);
		}
		return anchor.addTargetConnector(connector);
	}

	@Override
	public boolean removeSourceConnector(IConnector connector) {
		return sourceConnectors.remove(connector);
	}

	@Override
	public boolean removeTargetConnector(IConnector connector) {
		return targetConnectors.remove(connector);
	}

}
