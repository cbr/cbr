package cbr.pt.frontend.console;

/**
 * 
 * Tools to create some dialogs
 * 
 * 
 * @author Alexander Strakh
 *
 */
public class ConsoleUtils {

	public static void showMessageDialog(String message) {
		// Default info message
		/*
		 * JOptionPane.showMessageDialog(frame,
		 * "Eggs are not supposed to be green.");
		 */
		// Warning message
		/*
		 * JOptionPane.showMessageDialog(frame,
		 * "Eggs are not supposed to be green.", "Inane warning",
		 * JOptionPane.WARNING_MESSAGE);
		 */
		// Error message
		/*
		 * JOptionPane.showMessageDialog(frame,
		 * "Eggs are not supposed to be green.", "Inane error",
		 * JOptionPane.ERROR_MESSAGE);
		 */
	}

}
