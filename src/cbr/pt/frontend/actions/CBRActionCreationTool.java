package cbr.pt.frontend.actions;

import cbr.pt.frontend.CBRAction;
import cbr.pt.frontend.CBRAppContext;
import cbr.pt.frontend.console.CBRContext;
import cbr.pt.frontend.console.tools.AbstractTool;
import cbr.pt.frontend.console.tools.IToolChangeListener;

public class CBRActionCreationTool extends CBRAction implements
		IToolChangeListener {

	private static final long serialVersionUID = 1L;

	private AbstractTool tool;

	public CBRActionCreationTool(CBRAppContext appContext, String actionName,
			String actionTip, String actionIcon, AbstractTool tool) {
		super(appContext, actionName, actionTip, actionIcon);
		CBRContext context = appContext.getConfig().getContext();
		context.addToolChangeListener(this);
		setCheckAction();
		this.tool = tool;
	}

	@Override
	public void checkPerformed() {
		if (isChecked()) {
			getAppContext().getConfig().getContext().setActiveTool(tool);
		} else {
			getAppContext().getConfig().getContext().selectDefaultTool();
		}
	}

	@Override
	public void toolChanged() {
		setChecked(getAppContext().getConfig().getContext().getActiveTool()
				.equals(tool));
	}

}
