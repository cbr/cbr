package cbr.channels;

import java.util.List;

import cbr.IDisposable;
import cbr.grid.ICell;
import cbr.wires.IWire;

public interface IChannel extends IDisposable {

	public List<IWire> getWires();

	public ICell getSource();

	public ICell getTarget();

	public void addWire(IWire wire);

	public boolean removeWire(IWire wire);

	public boolean isInvertedWire(IWire wire);

	public boolean isEmpty();

	public boolean contains(ICell cell);

	public void setPath(List<ICell> path);

	public List<ICell> getPath();

}
