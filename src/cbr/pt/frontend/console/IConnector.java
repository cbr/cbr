package cbr.pt.frontend.console;

import cbr.geometry.ILocationChanged;
import cbr.serialize.xml.annotations.XMLAttribute;
import cbr.serialize.xml.annotations.XMLElement;
import cbr.serialize.xml.annotations.XMLNode;
import cbr.serialize.xml.annotations.XMLReference;

@XMLNode("connector")
public interface IConnector extends ILocationChanged {

	public static final String EL_CONNECTOR = "connector";

	@XMLAttribute
	public int getSide();

	@XMLElement
	@XMLReference
	public IIdentifiedAnchor getAnchor();

	public boolean connect(IIdentifiedAnchor anchor);

	public boolean disconnect();

	public void setAnchor(IIdentifiedAnchor anchor);

	public IWireView getOwner();

	public void setOwner(IWireView connection);

	public void setSide(int side);

	public void update();

}
