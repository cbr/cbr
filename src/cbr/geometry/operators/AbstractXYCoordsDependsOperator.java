package cbr.geometry.operators;

public abstract class AbstractXYCoordsDependsOperator implements
		ICoordsDependsOperator {

	protected boolean isX;

	public AbstractXYCoordsDependsOperator(boolean isX) {
		this.isX = isX;
	}

	@Override
	public ICoordsDependsOperator change() {
		isX = !isX;
		return this;
	}

}
