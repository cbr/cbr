package cbr.pt.frontend.console;

import cbr.geometry.IPoint;
import cbr.geometry.IRectangle;

public class CentralPointLocator extends AbstractAnchorPointLocator {

	public CentralPointLocator(IIdentifiedAnchorView owner) {
		super(owner);
	}

	@Override
	protected int processLocate(IRectangle bounds, IPoint location, int side) {
		location.setX(bounds.getX() + bounds.getWidth() / 2);
		location.setY(bounds.getY() + bounds.getHeight() / 2);
		return side;
	}

}
