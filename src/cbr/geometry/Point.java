package cbr.geometry;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "point")
public class Point implements IPoint {

	private int x;

	private int y;

	public Point(int x, int y) {
		this.x = x;
		this.y = y;
	}

	public Point() {
		this(0, 0);
	}

	@Override
	public int getX() {
		return x;
	}

	@Override
	public int getY() {
		return y;
	}

	@Override
	public void setX(int x) {
		this.x = x;
	}

	@Override
	public void setY(int y) {
		this.y = y;
	}

	@Override
	public String toString() {
		return "Point(" + x + ", " + y + ")";
	}

	@Override
	public void translateTo(ITranslatable translatable) {
		IPoint location = translatable.getLocation();
		location.setX(location.getX() + getX());
		location.setY(location.getY() + getY());
	}

	@Override
	public void translateFrom(ITranslatable translatable) {
		IPoint location = translatable.getLocation();
		location.setX(location.getX() - getX());
		location.setY(location.getY() - getY());
	}

	@Override
	public IPoint getLocation() {
		return this;
	}

	@Override
	public boolean equals(Object o) {
		if (o instanceof IPoint) {
			IPoint p = (IPoint) o;
			return p.getX() == x && p.getY() == y;
		}
		return false;
	}

	@Override
	public IPoint copy() {
		return new Point(x, y);
	}

}
