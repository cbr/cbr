package cbr.pt.frontend.actions;

import cbr.pt.frontend.CBRAction;
import cbr.pt.frontend.CBRAppContext;
import cbr.pt.frontend.console.ISelectedListener;

public class CBRActionSelectedListener extends CBRAction implements
		ISelectedListener {

	private static final long serialVersionUID = 1L;

	public CBRActionSelectedListener(CBRAppContext appContext, String name,
			String tip, String icon) {
		super(appContext, name, tip, icon);
		appContext.getConfig().getContext().addSelectedListener(this);
		setEnablement();
	}

	private void setEnablement() {
		setEnabled(getAppContext().getConfig().getContext().getSelection() != null);
	}

	@Override
	public void selectionChanged() {
		setEnablement();
	}

}
