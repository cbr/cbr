package cbr.pt.frontend.console.editpolicy;

import java.util.ArrayList;
import java.util.List;

import cbr.geometry.IPoint;
import cbr.pt.frontend.console.CBRContext;
import cbr.pt.frontend.console.IAreaView;
import cbr.pt.frontend.console.requests.AbstractRequest;

public class AbstractEditPolicy implements IEditPolicy {

	private CBRContext context;

	private String name;

	private IAreaView host;

	private IAreaView serviceView;

	private IAreaView feedbackView;

	private AbstractRequest request;

	private Class<? extends IAreaView> clazz;

	private List<Class<? extends IAreaView>> classes = new ArrayList<Class<? extends IAreaView>>();

	protected Class<? extends IAreaView> getCurrentClass() {
		return clazz;
	}

	public void setCurrentClass(Class<? extends IAreaView> clazz) {
		this.clazz = clazz;
	}

	public AbstractEditPolicy(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public CBRContext getContext() {
		return context;
	}

	public void setContext(CBRContext context) {
		this.context = context;
	}

	public IAreaView getHost() {
		return host;
	}

	public void setHost(IAreaView host) {
		this.host = host;
	}

	public void addSupportedClasses(Class<? extends IAreaView> clazz) {
		classes.add(clazz);
	}

	public boolean isSupported(Class<? extends IAreaView> clazz) {
		for (Class<? extends IAreaView> supClazz : classes) {
			if (supClazz == clazz)
				return true;
		}
		return false;
	}

	public IAreaView getServiceView() {
		if (serviceView == null) {
			serviceView = createServiceView();
		}
		if (serviceView != null)
			updateServiceView(serviceView);
		return serviceView;
	}

	public IAreaView getFeedbackView() {
		if (feedbackView == null) {
			feedbackView = createFeedbackView();
		}
		if (feedbackView != null)
			updateFeedbackView(feedbackView);
		return feedbackView;
	}

	protected void hideServiceView() {
		IAreaView serviceView = getServiceView();
		if (serviceView != null) {
			serviceView.setVisible(false);
		}
	}

	protected void showServiceView() {
		IAreaView serviceView = getServiceView();
		if (serviceView != null) {
			serviceView.setVisible(true);
		}
	}

	protected void hideFeedbackView() {
		IAreaView feedbackView = getFeedbackView();
		if (feedbackView != null) {
			feedbackView.setVisible(false);
		}
	}

	protected void showFeedbackView() {
		IAreaView feedbackView = getFeedbackView();
		if (feedbackView != null) {
			feedbackView.setVisible(true);
		}
	}

	protected void addServiceViewToLayer() {
		IAreaView serviceView = getServiceView();
		if (serviceView != null) {
			addToServiceLayer(serviceView);
		}
	}

	protected void removeServiceViewFromLayer() {
		IAreaView serviceView = getServiceView();
		if (serviceView != null) {
			removeFromServiceLayer(serviceView);
		}
	}

	protected void addFeedbackViewToLayer() {
		IAreaView feedbackView = getFeedbackView();
		if (feedbackView != null) {
			addToFeedbackLayer(feedbackView);
		}
	}

	protected void disposeFeedbackView() {
		feedbackView = null;
	}

	protected void disposeServiceView() {
		serviceView = null;
	}

	protected void removeFeedbackFromLayer() {
		IAreaView feedbackView = getFeedbackView();
		if (feedbackView != null) {
			removeFromFeedbackLayer(feedbackView);
		}
	}

	protected void addToFeedbackLayer(IAreaView view) {
		getContext().getConsole().getFeedbackLayer().addChild(view);
	}

	protected void removeFromFeedbackLayer(IAreaView view) {
		getContext().getConsole().getFeedbackLayer().removeChild(view);
	}

	protected void addToServiceLayer(IAreaView view) {
		getContext().getConsole().getServiceLayer().addChild(view);
	}

	protected void removeFromServiceLayer(IAreaView view) {
		getContext().getConsole().getServiceLayer().removeChild(view);
	}

	protected void updateServiceView(IAreaView serviceView) {
		updateViewBounds(serviceView);
	}

	protected void updateFeedbackView(IAreaView feedbackView) {
		updateViewBounds(feedbackView);
	}

	protected void updateViewBounds(IAreaView view) {
	}

	public IAreaView createServiceView() {
		return null;
	}

	public IAreaView createFeedbackView() {
		return null;
	}

	@Override
	public void dispose() {

	}

	public void processDrag(IPoint location) {
	}

	public void finishDrag(IPoint location) {
	}

	public void startDrag(IPoint location) {
	}

	public void interruptDrag(IPoint location) {
	}

	public void deactivate() {

	}

	public void activate() {
	}

	public void move(IPoint location) {
	}

	public void click(IPoint location) {
	}

	public void setFeedbackView(IAreaView feedbackView) {
		this.feedbackView = feedbackView;
	}

	public void setServiceView(IAreaView serviceView) {
		this.serviceView = serviceView;
	}

	public void deactivateTool() {
		hideFeedbackView();
		removeFeedbackFromLayer();
		disposeFeedbackView();
		hideServiceView();
		removeServiceViewFromLayer();
		disposeServiceView();
	}

	public void activateTool() {

	}

	public void setRequest(AbstractRequest request) {
		this.request = request;
	}

	protected AbstractRequest getRequest() {
		return request;
	}

}
