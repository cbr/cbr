package cbr.geometry;

import cbr.ICopiable;
import cbr.serialize.xml.annotations.XMLAttribute;
import cbr.serialize.xml.annotations.XMLNode;

@XMLNode("dimension")
public interface IDimension extends ICopiable {

	public static final String EL_DIMENSION = "dimension";
	public static final String ATTR_WIDTH = "width";
	public static final String ATTR_HEIGHT = "height";

	@XMLAttribute
	public int getWidth();

	@XMLAttribute
	public int getHeight();

	public void setWidth(int width);

	public void setHeight(int height);

	@Override
	public IDimension copy();

}
