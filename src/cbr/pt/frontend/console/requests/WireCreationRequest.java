package cbr.pt.frontend.console.requests;

import cbr.pt.frontend.console.IAreaView;
import cbr.pt.frontend.console.IIdentifiedAnchor;
import cbr.pt.frontend.console.IWireView;

public class WireCreationRequest extends AbstractRequest {

	private Class<? extends IAreaView> clazz;

	private IIdentifiedAnchor source;

	private IIdentifiedAnchor target;

	private IWireView feedback;

	public WireCreationRequest(Class<? extends IAreaView> clazz) {
		this.clazz = clazz;
	}

	public Class<? extends IAreaView> getClazz() {
		return clazz;
	}

	public IIdentifiedAnchor getSource() {
		return source;
	}

	public void setSource(IIdentifiedAnchor source) {
		this.source = source;
	}

	public IIdentifiedAnchor getTarget() {
		return target;
	}

	public void setTarget(IIdentifiedAnchor target) {
		this.target = target;
	}

	public IWireView getFeedback() {
		return feedback;
	}

	public void setFeedback(IWireView feedback) {
		this.feedback = feedback;
	}

	@Override
	public void dispose() {
		if (feedback != null)
			feedback.dispose();
		feedback = null;
		source = null;
		target = null;
	}

}
