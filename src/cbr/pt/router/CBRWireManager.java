package cbr.pt.router;

import cbr.AbstractChannelBasedRouter;
import cbr.channels.manager.AbstractChannelsManager;
import cbr.geometry.IPoint;
import cbr.grid.ICell;
import cbr.grid.ISide;
import cbr.pt.frontend.console.IAreaView;
import cbr.pt.frontend.console.IConnector;
import cbr.pt.frontend.console.IWireView;
import cbr.wires.AbstractWireManager;
import cbr.wires.IWire;
import cbr.wires.Wire;

public class CBRWireManager extends AbstractWireManager<IWireView, IAreaView> {

	public CBRWireManager(AbstractChannelBasedRouter<IWireView, IAreaView> cbr,
			AbstractChannelsManager<IWireView, IAreaView> channelsManager) {
		super(cbr, channelsManager);
	}

	protected void refreshWire(IWire wire, IWireView connection,
			boolean isSource) {
		IConnector connector = isSource ? connection.getSourceConnector()
				: connection.getTargetConnector();
		IPoint point = connector.getLocation().copy();
		((IAreaView) connector.getAnchor()).translateToAbsolute(point);
		int sideIndex = connector.getSide();
		ICell cell = getGrid().getCellByPoint(point.getX(), point.getY());
		// TODO: If can't find cell (when anchor placed over bounds) then show
		// dialog with error message
		ISide side = cell == null ? null : cell.getSide(sideIndex);
		ICell clearCell = cell == null ? null : (cell.isObstacle() ? side
				.getNeighbour(cell) : cell);
		if (isSource) {
			wire.setSourceCell(clearCell);
			wire.setSourceSideIndex(sideIndex);
			wire.setSourcePoint(point);
		} else {
			wire.setTargetCell(clearCell);
			wire.setTargetSideIndex(sideIndex);
			wire.setTargetPoint(point);
		}
	}

	@Override
	protected void refreshWireSourceAndTarget(IWire wire, IWireView connection) {
		// TODO: Определять канал как пару вход->выход, где входом и
		// выходом будут являтся близжайшие свободные к выходу или входу ячейки
		refreshWire(wire, connection, true);
		refreshWire(wire, connection, false);
	}

	@Override
	protected IWire createNewWireByConstraint(IWireView connection) {
		return new Wire();
	}
}
