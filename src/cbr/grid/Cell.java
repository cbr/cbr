package cbr.grid;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import cbr.Compass;
import cbr.channels.IChannel;
import cbr.geometry.Rectangle;
import cbr.obstacles.IObstacle;

public class Cell extends Rectangle implements ICell {

	private Set<IChannel> channels = new HashSet<IChannel>();

	private Set<IObstacle> obstacles;

	private ISide[] sides = new ISide[COUNT_OF_SIDES];

	private IGrid grid;

	private int column;

	private int row;

	//private boolean isDirtyToBuildPath = false;

	public Cell(int column, int row, IGrid grid) {
		this.grid = grid;
		this.column = column;
		this.row = row;
	}

	public Cell(int column, int row, IGrid grid, int x, int y, int width,
			int height) {
		super(x, y, width, height);
		this.grid = grid;
		this.column = column;
		this.row = row;
	}

	public Cell(int column, int row, IGrid grid, int x, int y, int width,
			int height, Set<IObstacle> obstacles) {
		super(x, y, width, height);
		if (!(obstacles == null || obstacles.isEmpty())) {
			this.obstacles = new HashSet<IObstacle>();
			this.obstacles.addAll(obstacles);
		}
		this.grid = grid;
		this.column = column;
		this.row = row;
	}

	@Override
	public void dispose() {
		super.dispose();
		if (obstacles != null) {
			obstacles.clear();
			obstacles = null;
		}
		if (channels != null) {
			channels.clear();
			channels = null;
		}
		sides = null;
		grid = null;
	}

	@Override
	public IGrid getGrid() {
		return grid;
	}

	@Override
	public boolean isObstacle() {
		return obstacles != null && !obstacles.isEmpty();
	}

	@Override
	public Set<IObstacle> getObstalces() {
		if (obstacles == null)
			return Collections.emptySet();
		return obstacles;
	}

	@Override
	public ICell[] divideByX(int x) {
		if (x < getX() || x >= getX() + getWidth())
			throw new UnsupportedOperationException(
					"Cell must be contains division coordinate x. Cell.x = "
							+ getX() + ", Cell.width = " + getWidth()
							+ ". Division coordinate x = " + x + ".");
		ICell firstDividedCell = new Cell(getColumn(), getRow(), getGrid(),
				getX(), getY(), x - getX(), getHeight(), obstacles);
		ICell secondDividedCell = new Cell(getColumn() + 1, getRow(),
				getGrid(), x, getY(), getX() + getWidth() - x, getHeight(),
				obstacles);
		grid.getChannelsManager().divideCellsByX(this, firstDividedCell,
				secondDividedCell);
		return new ICell[] { firstDividedCell, secondDividedCell };
	}

	@Override
	public ICell[] divideByY(int y) {
		if (y < getY() || y >= getY() + getHeight())
			throw new UnsupportedOperationException(
					"Cell must be contains division coordinate y. Cell.y = "
							+ getY() + ", Cell.height = " + getHeight()
							+ ". Division coordinate y = " + y + ".");
		ICell firstDividedCell = new Cell(getColumn(), getRow(), getGrid(),
				getX(), getY(), getWidth(), y - getY(), obstacles);
		ICell secondDividedCell = new Cell(getColumn(), getRow() + 1,
				getGrid(), getX(), y, getWidth(), getY() + getHeight() - y,
				obstacles);
		grid.getChannelsManager().divideCellsByY(this, firstDividedCell,
				secondDividedCell);
		return new ICell[] { firstDividedCell, secondDividedCell };
	}

	@Override
	public void addObstacle(IObstacle obstacle) {
		if (obstacles == null) {
			obstacles = new HashSet<IObstacle>();
		}
		obstacles.add(obstacle);
	}

	@Override
	public void removeObstacle(IObstacle obstacle) {
		if (obstacles != null) {
			obstacles.remove(obstacle);
		}
	}

	@Override
	public ICell union(ICell cell) {
		int x1 = getX();
		int x2 = cell.getX();
		int y1 = getY();
		int y2 = cell.getY();
		int width1 = getWidth();
		int width2 = cell.getWidth();
		int height1 = getHeight();
		int height2 = cell.getHeight();

		int right = Math.max(x1 + width2, x2 + width2);
		int bottom = Math.max(y1 + height2, y2 + height2);
		x1 = Math.min(x1, x2);
		y1 = Math.min(y1, y2);
		width1 = right - Math.min(x1, x2);
		height1 = bottom - y1;

		ICell unionedCell = new Cell(getColumn(), getRow(), getGrid(), x1, y1,
				width1, height1, obstacles);
		for (IObstacle obstacle : cell.getObstalces()) {
			unionedCell.addObstacle(obstacle);
		}
		grid.getChannelsManager().unionCells(this, cell, unionedCell);
		return unionedCell;
	}

	@Override
	public ISide getWestSide() {
		return sides[Compass.WEST];
	}

	@Override
	public ISide getNorthSide() {
		return sides[Compass.NORTH];
	}

	@Override
	public ISide getEastSide() {
		return sides[Compass.EAST];
	}

	@Override
	public ISide getSouthSide() {
		return sides[Compass.SOUTH];
	}

	@Override
	public ICell getWestCell() {
		ISide side = getWestSide();
		return side == null ? null : side.getNeighbour1();
	}

	@Override
	public ICell getNorthCell() {
		ISide side = getNorthSide();
		return side == null ? null : side.getNeighbour1();
	}

	@Override
	public ICell getEastCell() {
		ISide side = getEastSide();
		return side == null ? null : side.getNeighbour2();
	}

	@Override
	public ICell getSouthCell() {
		ISide side = getSouthSide();
		return side == null ? null : side.getNeighbour2();
	}

	@Override
	public boolean contains(IObstacle obstacle) {
		return getObstalces().contains(obstacle);
	}

	@Override
	public void setWestSide(ISide side) {
		sides[Compass.WEST] = side;
	}

	@Override
	public void setNorthSide(ISide side) {
		sides[Compass.NORTH] = side;
	}

	@Override
	public void setEastSide(ISide side) {
		sides[Compass.EAST] = side;
	}

	@Override
	public void setSouthSide(ISide side) {
		sides[Compass.SOUTH] = side;
	}

	@Override
	public int getColumn() {
		return column;
	}

	@Override
	public int getRow() {
		return row;
	}

	@Override
	public int[] getColumnRow() {
		return new int[] { getColumn(), getRow() };
	}

	@Override
	public Set<IChannel> getChannels() {
		return channels;
	}

	@Override
	public void addChannel(IChannel channel) {
		channels.add(channel);
	}

	@Override
	public void removeChannel(IChannel channel) {
		channels.remove(channel);
	}

	@Override
	public void setColumn(int column) {
		this.column = column;
	}

	@Override
	public void setRow(int row) {
		this.row = row;
	}

	/*@Override
	public boolean isDirtyToBuildPath() {
		return isDirtyToBuildPath;
	}

	@Override
	public void dirtyToBuildPath() {
		isDirtyToBuildPath = true;
	}

	@Override
	public void cleanedAfterBuildPath() {
		isDirtyToBuildPath = false;
	}*/

	@Override
	public ISide getSide(int index) {
		return sides[index];
	}

}
