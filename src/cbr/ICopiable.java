package cbr;

public interface ICopiable {

	public Object copy();

}
