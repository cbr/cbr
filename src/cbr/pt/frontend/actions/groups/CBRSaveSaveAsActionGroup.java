package cbr.pt.frontend.actions.groups;

import cbr.pt.frontend.CBRActionGroup;
import cbr.pt.frontend.CBRAppContext;
import cbr.pt.frontend.actions.CBRActionSaveModel;

public class CBRSaveSaveAsActionGroup extends CBRActionGroup {

	public CBRSaveSaveAsActionGroup(CBRAppContext appContext) {
		super(appContext, "Save/Save as");
		addAction(new CBRActionSaveModel(appContext));
	}

}
