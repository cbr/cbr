package cbr;

import cbr.channels.manager.AbstractChannelsManager;
import cbr.geometry.IRectangle;
import cbr.grid.Grid;
import cbr.grid.IObstaclesBoundedGrid;
import cbr.grid.ObstaclesDynamicBoundedGrid;
import cbr.obstacles.AbstractObstaclesManager;
import cbr.wires.AbstractWireManager;
import cbr.wires.IWire;
import cbr.wires.Wire;

/**
 * 
 * Роутер состоит из трех частей:<br>
 * <ul>
 * <li>Сетка {@link Grid}</li>
 * <li>Менеджер соединений {@link AbstractWireManager}</li>
 * <li>Менеджер каналов {@link AbstractChannelsManager}</li>
 * </ul>
 * <br>
 * Общий принцип работы:<br>
 * <ul>
 * <li>В зависимости от типа запрос отправляется либо сетке {@link Grid} либо
 * менеджеру соединений {@link AbstractWireManager}</li>
 * <li>Менеджер соединений/сетка отправляют запросы на обновление каналов в
 * {@link AbstractChannelsManager}</li>
 * <li>Менеджер каналов обрабатывает все списки запросов</li>
 * <li>Берем из менеджера каналов обновленные соединения {@link Wire} и
 * обновляем соответствующие constraint-соединения</li>
 * <li>Оцищается менеджер каналов</li>
 * </ul>
 * <br>
 * Запросы, котрые отправляются на обработку сетке {@link Grid}:
 * <ul>
 * <li>Изменени размеров контейнера</li>
 * <li>Добавление нового препятствия</li>
 * <li>Удаление препятствия</li>
 * <li>Обновление препятствия</li>
 * </ul>
 * <br>
 * Запросы, которые отправляются на обработку менеджеру соединений:<br>
 * <ul>
 * <li>Добавление нового соединения</li>
 * <li>Удаление соединения</li>
 * <li>Обновление соединения</li>
 * </ul>
 * 
 * @author Alexander Strakh
 * 
 * @param <T>
 */
public abstract class AbstractChannelBasedRouter<T, I> {

	protected boolean isActive = true;

	protected AbstractChannelsManager<T, I> channelsManager;

	protected AbstractWireManager<T, I> wiresManager;

	protected AbstractObstaclesManager<I, T> obstaclesManager;

	protected ObstaclesDynamicBoundedGrid<T, I> grid = new ObstaclesDynamicBoundedGrid<T, I>();

	public AbstractChannelBasedRouter() {
		channelsManager = createChannelsManager();
		obstaclesManager = createObstaclesManager();
		obstaclesManager.setGrid(grid);
		wiresManager = createWireManager();
		wiresManager.setGrid(grid);
		wiresManager.setObstaclesManager(obstaclesManager);
		channelsManager.setWireManager(wiresManager);
		channelsManager.configure();
		grid.setChannelsManager(channelsManager);
	}

	abstract protected AbstractChannelsManager<T, I> createChannelsManager();

	abstract protected AbstractObstaclesManager<I, T> createObstaclesManager();

	abstract protected AbstractWireManager<T, I> createWireManager();

	abstract protected void updateWireConstraint(T constraint, IWire wire);
	
	abstract public void showErrorMessage(String message);

	public void switchOff() {
		isActive = false;
	}

	private void process() {
		channelsManager.process();
		for (IWire wire : channelsManager.getCompletedWires()) {
			if (wire.isNeedUpdatePath()) {
				updateWireConstraint(wiresManager.getWireConstraint(wire), wire);
				wire.pathUpdated();
			}
		}
		channelsManager.clean();
	}

	// ======= callback's for top-level router (for wire manager) =========//

	public void addWireByConstraint(T object) {
		if (isActive) {
			wiresManager.addNewWireByConstraint(object);
			process();
		}
	}

	public void removeWireByConstraint(T object) {
		if (isActive) {
			wiresManager.removeWireByConstraint(object);
			process();
		}
	}

	// FIXME: call implementation for pt
	public void updateWireByConstraint(T object) {
		if (isActive) {
			wiresManager.updateWireByConstraint(object);
			process();
		}
	}

	// ======= callback's for top-level router (for grid) =================//

	/*
	 * УДАЛЕНИЕ и ДОБАВЛЕНИЕ ПРЕПЯТСВИЙ ПРОВЕРЯТЬ НА ТО, ЧТО ОНИ НАХОДЯТСЯ В
	 * РАБОЧЕЙ ЗОНЕ
	 */
	public void addObstacleByConstraint(I object) {
		if (isActive) {
			getObstaclesManager().addObstacleByConstraint(object);
			process();
		}
	}

	public void removeObstacleByConstraint(I object) {
		if (isActive) {
			getObstaclesManager().removeObstacleByConstraint(object);
			process();
		}
	}

	public void updateObstacleByConstraintChanges(I object) {
		if (isActive) {
			getObstaclesManager().updateObstacleByConstraintChanges(object);
			process();
		}
	}

	public void uddateGridByContainerConstraintChanges(I object) {
		if (isActive) {
			getGrid().setBounds(getBounds(object));
			process();
		}
	}

	abstract public IRectangle getBounds(I Object);

	public IObstaclesBoundedGrid getGrid() {
		return grid;
	}

	public AbstractObstaclesManager<I, T> getObstaclesManager() {
		return obstaclesManager;
	}

	public void dispose() {
		grid.dispose();
		channelsManager.dispose();
		obstaclesManager.dispose();
		wiresManager.dispose();
		return;
	}

}
