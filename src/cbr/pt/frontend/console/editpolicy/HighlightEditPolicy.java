package cbr.pt.frontend.console.editpolicy;

import cbr.geometry.IPoint;
import cbr.geometry.IRectangle;
import cbr.pt.frontend.console.IAreaView;
import cbr.pt.frontend.console.service.AreaHighlightHandles;

public class HighlightEditPolicy extends
		MoveSupportReactivatedCreationEditPolicy {

	public static final String EDIT_POLICY_HIGHLIGHT = "Highlight edit policy";

	public HighlightEditPolicy() {
		super(EDIT_POLICY_HIGHLIGHT);
	}

	@Override
	public IAreaView createServiceView() {
		return new AreaHighlightHandles();
	}

	@Override
	public void finishDrag(IPoint location) {
		getServiceView();
	}

	@Override
	protected void updateViewBounds(IAreaView view) {
		super.updateViewBounds(view);
		IAreaView host = getHost();
		IRectangle bounds = host.getBounds().copy();
		IAreaView parent = host.getParent();
		if (parent != null)
			parent.translateToAbsolute(bounds);
		view.setBounds(bounds);
	}
}
