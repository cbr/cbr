package cbr.pt.frontend.actions;

import java.awt.event.ActionEvent;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;

import javax.swing.JFileChooser;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.xml.sax.SAXException;

import cbr.pt.frontend.CBRAction;
import cbr.pt.frontend.CBRAppContext;
import cbr.pt.frontend.Config;

public class CBRActionOpenModel extends CBRAction {

	private static final long serialVersionUID = 1L;

	public final static String ACTION_NAME = "Open model";

	public final static String ACTION_TIP = "Open model";

	public final static String ACTION_ICON = "open.png";

	public CBRActionOpenModel(CBRAppContext appContext) {
		super(appContext, ACTION_NAME, ACTION_TIP, ACTION_ICON);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		try {
			if (CBRModelActionUtils.isNeedsToSaveAndTryToSave(getAppContext())) {
				if (CBRModelActionUtils.fileChooser
						.showOpenDialog(getAppContext().getFrame()) == JFileChooser.APPROVE_OPTION) {
					CBRModelActionUtils.disposeModel(getAppContext());
					Config config = getAppContext().getConfig();
					File file = CBRModelActionUtils.fileChooser
							.getSelectedFile();
					CBRModelActionUtils.loadModel(config,
							file.getAbsolutePath());
				}
			}
		} catch (IllegalAccessException e1) {
			e1.printStackTrace();
		} catch (IllegalArgumentException e1) {
			e1.printStackTrace();
		} catch (InvocationTargetException e1) {
			e1.printStackTrace();
		} catch (InstantiationException e1) {
			e1.printStackTrace();
		} catch (ParserConfigurationException e1) {
			e1.printStackTrace();
		} catch (TransformerException e1) {
			e1.printStackTrace();
		} catch (SAXException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
	}

}
