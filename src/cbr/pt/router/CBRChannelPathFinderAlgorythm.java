package cbr.pt.router;

import java.util.List;

import cbr.IChannelPathFinderAlgorythm;
import cbr.algs.astar.AStar;
import cbr.channels.IChannel;
import cbr.grid.ICell;
import cbr.grid.IObstaclesBoundedGrid;
import cbr.pt.frontend.CBRApp;

public class CBRChannelPathFinderAlgorythm implements
		IChannelPathFinderAlgorythm {

	private IObstaclesBoundedGrid grid;

	private AStar aStar = new AStar();

	public CBRChannelPathFinderAlgorythm(IObstaclesBoundedGrid grid) {
		this.grid = grid;
	}

	/**
	 * 
	 * Случаи преобразования канала:<br>
	 * <ul>
	 * <li>исходная и/или целевая ячейки могут быть не актуальны (преобразованы
	 * в группу/объеденены/удалены и так далее)</li>
	 * </ul>
	 * 
	 * 
	 */
	@Override
	public void process(IChannel channel) {
		ICell sourceCell = channel.getSource();
		ICell targetCell = channel.getTarget();
		int[] sourceColumnRow = sourceCell.getColumnRow();
		int[] targetColumnRow = targetCell.getColumnRow();
		CBRApp.logger().info(
				"Process channel: " + "(" + sourceColumnRow[0] + ", "
						+ sourceColumnRow[1] + ") -> (" + targetColumnRow[0]
						+ ", " + targetColumnRow[1] + ")");
		// if target and/or source row changed => have to replace it
		// TODO: update path after transformations
		List<ICell> cells = aStar.routeAlgorythm(grid.getCells(),
				grid.getColumnsCount(), grid.getRowsCount(),
				sourceColumnRow[0], sourceColumnRow[1], targetColumnRow[0],
				targetColumnRow[1]);
		// После работы алгоритма поиска пути - все ячейки будут новые для
		// отрисовки пути
		// for(ICell cell: cells) cell.dirtyToBuildPath();
		channel.setPath(cells);
	}

}
