package cbr.pt.frontend.actions;

import java.awt.event.ActionEvent;
import java.lang.reflect.InvocationTargetException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import cbr.pt.frontend.CBRAppContext;
import cbr.pt.frontend.command.CommandsFactory;
import cbr.pt.frontend.console.CBRContext;
import cbr.pt.frontend.console.IAreaView;

public class CBRActionCutView extends CBRActionSelectedListener {

	private static final long serialVersionUID = 1L;

	public final static String ACTION_NAME = "Cut view";

	public final static String ACTION_TIP = "Cut view";

	public final static String ACTION_ICON = "cut.png";

	public CBRActionCutView(CBRAppContext appContext) {
		super(appContext, ACTION_NAME, ACTION_TIP, ACTION_ICON);
	}

	@Override
	public void actionPerformed(ActionEvent event) {
		try {
			CBRAppContext appContext = getAppContext();
			IAreaView selectedArea = CBRModelActionUtils
					.removeSelectFromSelectionAndSendToClipboard(getAppContext());
			CBRContext context = appContext.getConfig().getContext();
			CommandsFactory.createAndExecute("DeleteCommand", context,
					"Remove view", selectedArea);
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (TransformerException e) {
			e.printStackTrace();
		}
	}

}
