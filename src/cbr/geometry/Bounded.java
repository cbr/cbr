package cbr.geometry;

public class Bounded implements IBounded {

	private IRectangle bounds;

	public Bounded() {
		bounds = createRectangle();
	}

	@Override
	public IDimension getSize() {
		return getBounds().getSize();
	}

	@Override
	public void setSize(IDimension size) {
		getBounds().setSize(size);
	}

	@Override
	public void setLocation(IPoint location) {
		getBounds().setLocation(location);
	}

	@Override
	public IPoint getLocation() {
		return getBounds().getLocation();
	}

	@Override
	public IRectangle getBounds() {
		return bounds;
	}

	@Override
	public void setBounds(IRectangle bounds) {
		this.bounds = bounds;
	}

	protected IRectangle createRectangle() {
		return new Rectangle(0, 0, 0, 0);
	}

	@Override
	public void dispose() {
		getBounds().dispose();
	}

	@Override
	public String toString() {
		return "Bounded(" + getBounds().getX() + ", " + getBounds().getY()
				+ ", " + getBounds().getWidth() + ", "
				+ getBounds().getHeight() + ")";
	}

}
