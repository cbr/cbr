package cbr.pt.frontend.actions;

import java.awt.event.ActionEvent;
import java.lang.reflect.InvocationTargetException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import cbr.pt.frontend.CBRAppContext;
import cbr.pt.frontend.command.CommandStack;

public class CBRActionSaveModel extends CBRActionCommandStackListener {

	private static final long serialVersionUID = 1L;

	public final static String ACTION_NAME = "Save model";

	public final static String ACTION_TIP = "Save model";

	public final static String ACTION_ICON = "save.png";

	public CBRActionSaveModel(CBRAppContext appContext) {
		super(appContext.getConfig().getContext().getCommandStack(),
				appContext, ACTION_NAME, ACTION_TIP, ACTION_ICON);
		setEnabled(getAppContext().getConfig().getContext().getCommandStack()
				.isDirty());
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		try {
			CBRModelActionUtils.saveModel(getAppContext());
		} catch (IllegalAccessException e1) {
			e1.printStackTrace();
		} catch (IllegalArgumentException e1) {
			e1.printStackTrace();
		} catch (InvocationTargetException e1) {
			e1.printStackTrace();
		} catch (ParserConfigurationException e1) {
			e1.printStackTrace();
		} catch (TransformerException e1) {
			e1.printStackTrace();
		}
	}

	@Override
	public void handleCommandStackChanged(CommandStack commandStack, int event) {
		setEnabled(commandStack.isDirty());
	}
}
