package cbr.channels.manager;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import cbr.IChannelPathFinderAlgorythm;
import cbr.IPathBuilderAlgorythm;
import cbr.channels.Channel;
import cbr.channels.IChannel;
import cbr.channels.manager.pipe.ChannelsPipe;
import cbr.grid.ICell;
import cbr.wires.AbstractWireManager;
import cbr.wires.IWire;

/**
 * 
 * Управляет каналами.<br>
 * Случаи обновления каналов:<br>
 * <ul>
 * <li>добавление соединения</li>
 * <li>удаление соединения</li>
 * <li>обновление соединения</li>
 * </ul>
 * 
 * @author Alexander Strakh
 * 
 */
public abstract class AbstractChannelsManager<T, I> {

	private Set<IChannel> channelsToFullRefresh = new HashSet<IChannel>();

	private ChannelsPipe pipe = new ChannelsPipe();

	private List<IChannel> channels = new ArrayList<IChannel>();

	private AbstractWireManager<T, I> wireManager;

	abstract protected IChannelPathFinderAlgorythm createFinderAlgorythm();

	abstract protected IPathBuilderAlgorythm createBuilderAlgorythm();

	public void setChannelFinderAlgorythm(IChannelPathFinderAlgorythm algorythm) {
		pipe.configurePathFinderStage(algorythm);
	}

	public void setPathBuilderAlgorythm(IPathBuilderAlgorythm algorythm) {
		pipe.configurePathFinderStage(algorythm);
	}

	public void process() {
		updateChannelsToFullRefresh();
		pipe.process();
	}

	public void clean() {
		pipe.clean();
	}

	public Set<IWire> getCompletedWires() {
		return pipe.getCompletedWires();
	}

	public void setWireManager(AbstractWireManager<T, I> wireManager) {
		this.wireManager = wireManager;
	}

	/**
	 * 
	 * Вызывается, когда было добавлено новое соединение.
	 * 
	 * @param wire
	 */
	public void addWire(IWire wire) {
		IChannel channel = getAppropriatedChannel(wire);
		if (channel == null) {
			channel = new Channel(wire.getSourceCell(), wire.getTargetCell());
			channel.addWire(wire);
			channels.add(channel);
			pipe.addToFindPathStage(channel);
		} else {
			channel.addWire(wire);
			pipe.addToBuildPathStage(channel);
		}
	}

	public void addToBuildPathStageIvalidWire(IWire wire) {
		pipe.addToBuildPathStageIvalidWire(wire);
	}

	/**
	 * 
	 * Вызывается после удаления соединения.
	 * 
	 * @param wire
	 */
	public void removeWire(IWire wire) {
		IChannel channel = getExistsChannel(wire);
		channel.removeWire(wire);
		if (channel.isEmpty()) {
			removeChannel(channel);
		} else {
			pipe.addToBuildPathStage(channel);
		}
	}

	/**
	 * 
	 * Должна вызываться только если действительно<br>
	 * с соединением произошли измененния.<br>
	 * Соединение должно уже быть внутри роутера.<br>
	 * Т.е. было доабвлено спомощью AddWire().<br>
	 * 
	 * Вызываться должна только в следующих случаях:<br>
	 * <ul>
	 * <li>Смена ячейки у source или target соединения - смена канала</li>
	 * <li>Смена стороны ячейки у source или target соединения - смена канала</li>
	 * <li>Смена координат у source или target соединения - этот<br>
	 * случай обрабатывается, если два первых не подошли.</li>
	 * </ul>
	 * 
	 */
	public void updateWire(IWire wire) {
		IChannel channel = getExistsChannel(wire);
		boolean isInvertedWire = channel.isInvertedWire(wire);
		ICell wireSource = isInvertedWire ? wire.getTargetCell() : wire
				.getSourceCell();
		ICell wireTarget = isInvertedWire ? wire.getSourceCell() : wire
				.getTargetCell();
		ICell channelSource = channel.getSource();
		ICell channelTarget = channel.getTarget();
		if (channelSource == wireSource && channelTarget == wireTarget) {
			/* обовление координат */
			pipe.addToBuildPathStage(channel);
		} else {
			/* смена канала */
			channel.removeWire(wire);
			if (channel.isEmpty()) {
				removeChannel(channel);
			} else {
				pipe.addToBuildPathStage(channel);
			}
			addWire(wire);
		}
	}

	private void removeChannel(IChannel channel) {
		pipe.removeChannelFromPipe(channel);
		channels.remove(channel);
		channel.dispose();
	}

	private IChannel getExistsChannel(IWire wire) {
		for (IChannel channel : channels) {
			if (channel.getWires().contains(wire)) {
				return channel;
			}
		}
		throw new UnsupportedOperationException(
				"getExistsChannel() called for unexists wire!");
	}

	private IChannel getAppropriatedChannel(IWire wire) {
		for (IChannel channel : channels) {
			if ((channel.getSource() == wire.getSourceCell() && channel
					.getTarget() == wire.getTargetCell())
					|| (channel.getTarget() == wire.getSourceCell() && channel
							.getSource() == wire.getTargetCell())) {
				return channel;
			}
		}
		return null;
	}

	public void remove(ICell cell) {
		for (IChannel channel : channels) {
			if (channel.getSource() == cell || channel.getTarget() == cell) {
				for (IWire wire : channel.getWires()) {
					wireManager.removeWire(wire);
				}
			} else if (channel.contains(cell)) {
				pipe.addToBuildPathStage(channel);
			}
		}
	}

	public void configure() {
		setChannelFinderAlgorythm(createFinderAlgorythm());
		setPathBuilderAlgorythm(createBuilderAlgorythm());
	}

	public void dispose() {
		channels.clear();
		channels = null;
		pipe = null;
	}

	public void updateChannelsToFullRefresh() {
		Set<IWire> indigentWires = new HashSet<IWire>();
		for (IChannel channel : channelsToFullRefresh) {
			indigentWires.addAll(channel.getWires());
			removeChannel(channel);
		}
		channelsToFullRefresh.clear();
		wireManager.refreshIndigentAndInvalidWires(indigentWires);
	}

	public void unionCells(ICell cell1, ICell cell2, ICell unionedCell) {
		for (IChannel channel : cell1.getChannels()) {
			channelsToFullRefresh.add(channel);
		}

		for (IChannel channel : cell2.getChannels()) {
			channelsToFullRefresh.add(channel);
		}
	}

	public void divideCellsByY(ICell cell, ICell firstDividedCell,
			ICell secondDividedCell) {
		for (IChannel channel : cell.getChannels()) {
			channelsToFullRefresh.add(channel);
		}
	}

	/**
	 * 
	 * Если ячейка оказалась принадлежайщей какому либо каналу, путь канала
	 * обновляется в<br>
	 * соответствуие с одинм из следующих условий.<br>
	 * <ul>
	 * <li>Часть пути в который входит ячейка является горизонтальным (y и
	 * y+height всех ячеек равны соответственно). <br>
	 * Cостоит из трех и более ячеек, а делимая ячейка не является первой или
	 * последней. В этом случае ячейка просто заменяется<br>
	 * на соответсвующие производные, полученные при делении. Путь не требует
	 * последующего запуска алгоритма поиска повторно. Соединения требуют<br>
	 * перерисовки на участке делимой ячейки - производные ячейки помечаются
	 * соответсвующим флагом<br>
	 * </li>
	 * <li>Всеь путь состоял из одной ячейки и являлся горизонтальным (начальная
	 * и конечная точка соединений лежали в пределах x,x+height<br>
	 * ). В этом случае изменениыя такие же как и в предыдущем случае.</li>
	 * <li>Весь путь состоял из двух ячеек и являлся горизонтальным. (начальная
	 * и конечная точка соединений лежали в пределах x,x+height<br>
	 * ). В этом случае изменениыя такие же как и в предыдущем случае.</li>
	 * <li>В остальных случаях определяем коридор вначале и конце пути...
	 * Заменяем, отправляем на обновление...</li>
	 * </ul>
	 * 
	 * @param cell
	 * @param cell2
	 * @param unionedCell
	 */
	public void divideCellsByX(ICell cell, ICell firstDividedCell,
			ICell secondDividedCell) {
		nextChannel: for (IChannel channel : cell.getChannels()) {
			/*
			 * List<ICell> path = channel.getPath(); // for case 1 if
			 * (path.size() > 2) { for (int i = 0; i < path.size(); i++) { ICell
			 * curCell = path.get(i); if (curCell == cell) { if (i > 0 && i + 1
			 * < path.size()) { ICell prevCell = path.get(i - 1); ICell nextCell
			 * = path.get(i + 1); // проверяем, что вся тройка - горизонтальный
			 * путь if (prevCell.getX() == cell.getX() && prevCell.getX() +
			 * prevCell.getWidth() == cell .getX() + cell.getWidth() &&
			 * nextCell.getX() == cell.getX() && nextCell.getX() +
			 * nextCell.getWidth() == cell .getX() + cell.getWidth()) { //
			 * меняем разбитую ячейку на две новые c // отметкой соответствующих
			 * ячеек как dirty firstDividedCell.dirtyToBuildPath(); path.set(i,
			 * firstDividedCell); secondDividedCell.dirtyToBuildPath();
			 * path.add(i + 1, secondDividedCell);
			 * pipe.addToBuildPathStage(channel); continue nextChannel; } } } }
			 * }
			 */
			// List<IChannel newChannels =
			// fullRefreshChannelsAfterDivision(channel);
			// set drity - rebuild path with replace source and target points
			// full find path - apply find algorythm again*/
			channelsToFullRefresh.add(channel);
		}
	}
}
