package cbr.pt.frontend.console;

public interface IFeatureLocatorProvider {

	public IFeatureLocator getLocator(IFeatureView feature);

	public IFeatureLocator getLocator(Class<? extends IFeatureView> featureClass);

}
