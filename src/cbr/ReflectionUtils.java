package cbr;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class ReflectionUtils {

	public static List<Method> getAllAnnotatedMethodsOnlyForCurrentClassWith(
			Class<?> clazz, Class<?> annotationClass) {
		Class<? extends Annotation> annotationExtClass = annotationType(annotationClass);
		List<Method> methods = new ArrayList<Method>();
		for (Method method : getAllMethodsOnlyForCurrentClass(clazz)) {
			if (method.isAnnotationPresent(annotationExtClass)) {
				methods.add(method);
			}
		}
		return methods;
	}

	public static List<Method> getAllAnnotatedMethodsOnlyForCurrentClass(
			Class<?> clazz) {
		List<Method> methods = new ArrayList<Method>();
		for (Method method : getAllMethodsOnlyForCurrentClass(clazz)) {
			if (method.getAnnotations().length > 0) {
				methods.add(method);
			}
		}
		return methods;
	}

	public static List<Method> getAllMethodsOnlyForCurrentClass(Class<?> clazz) {
		List<Method> methods = new ArrayList<Method>();
		for (Method method : clazz.getMethods()) {
			methods.add(method);
		}
		return methods;
	}

	public static List<Class<?>> getAllTypeAnnotatedInterfacesWith(
			Object object, Class<?> annotationClass) {
		return getAllTypeAnnotatedInterfacesWith(object.getClass(),
				annotationClass);
	}

	@SuppressWarnings("unchecked")
	public static Class<? extends Annotation> annotationType(Class<?> annotation) {
		return (Class<? extends Annotation>) annotation;
	}

	public static List<Class<?>> getAllTypeAnnotatedInterfacesWith(
			Class<?> clazz, Class<?> annotationClass) {
		Class<? extends Annotation> annotationExtClass = annotationType(annotationClass);
		List<Class<?>> annotatedIfaces = new ArrayList<Class<?>>();
		for (Class<?> curClass : getAllTypeAnnotatedInterfaces(clazz)) {
			if (curClass.isAnnotationPresent(annotationExtClass)) {
				annotatedIfaces.add(curClass);
			}
		}
		return annotatedIfaces;
	}

	public static List<Class<?>> getAllTypeAnnotatedInterfaces(Object object) {
		return getAllTypeAnnotatedInterfaces(object.getClass());
	}

	public static List<Class<?>> getAllTypeAnnotatedInterfaces(Class<?> clazz) {
		List<Class<?>> annoIfaces = new ArrayList<Class<?>>();
		for (Class<?> curClass : getAllInterfaces(clazz)) {
			if (curClass.getAnnotations().length > 0) {
				annoIfaces.add(curClass);
			}
		}
		return annoIfaces;
	}

	public static List<Class<?>> getAllSuperclasses(Object object) {
		return getAllSuperclasses(object.getClass());
	}

	public static List<Class<?>> getAllSuperclasses(Class<?> clazz) {
		List<Class<?>> classes = new ArrayList<Class<?>>();
		classes.add(clazz);
		Class<?> superclass = clazz.getSuperclass();
		while (superclass != null) {
			classes.add(superclass);
			superclass = superclass.getSuperclass();
		}
		return classes;
	}

	public static List<Class<?>> getAllInterfaces(Object object) {
		return getAllInterfaces(object.getClass());
	}

	public static List<Class<?>> sortInterfacesByInheritance(
			List<Class<?>> classes) {
		Map<Class<?>, List<Class<?>>> map = new HashMap<Class<?>, List<Class<?>>>();
		Iterator<Class<?>> classIter = classes.iterator();
		while (classIter.hasNext()) {
			Class<?> clazz = classIter.next();
			List<Class<?>> inherited = getAllTypeAnnotatedInterfaces(clazz);
			inherited.remove(clazz);
			map.put(clazz, inherited);
		}

		List<Class<?>> sortedClasses = new ArrayList<Class<?>>();
		while (map.size() != 0) {
			classIter = classes.iterator();
			while (classIter.hasNext()) {
				Class<?> clazz = classIter.next();
				boolean isSuperinterface = true;
				for (List<Class<?>> list : map.values()) {
					if (list.contains(clazz)) {
						isSuperinterface = false;
						break;
					}
				}
				if (isSuperinterface) {
					sortedClasses.add(clazz);
					classIter.remove();
					map.remove(clazz);
				}
			}
		}

		return sortedClasses;
	}

	public static List<Class<?>> getAllInterfaces(Class<?> clazz) {
		List<Class<?>> list = new ArrayList<Class<?>>();
		if (clazz.isInterface()) {
			list.add(clazz);
		}
		while (clazz != null) {
			Class<?>[] interfaces = clazz.getInterfaces();
			for (int i = 0; i < interfaces.length; i++) {
				if (list.contains(interfaces[i]) == false) {
					list.add(interfaces[i]);
				}
				List<Class<?>> superInterfaces = getAllInterfaces(interfaces[i]);
				for (Iterator<Class<?>> it = superInterfaces.iterator(); it
						.hasNext();) {
					@SuppressWarnings("unchecked")
					Class<Class<?>> intface = (Class<Class<?>>) it.next();
					if (list.contains(intface) == false) {
						list.add(intface);
					}
				}
			}
			clazz = clazz.getSuperclass();
		}
		return list;
	}

	public static Method getLastGetMethod(Object object, String fieldName) {
		return getLastMethod(object, fieldName, false);
	}

	public static Method getLastSetMethod(Object object, String fieldName) {
		return getLastMethod(object, fieldName, true);
	}

	public static Method getLastMethod(Object object, String fieldName,
			boolean isSet) {
		String methodName = getMethodName(fieldName, isSet);
		Field field = isSet ? getField(object, fieldName) : null;
		Class<?> type = isSet ? field.getType() : null;
		Method method = getMethod(object, methodName, type);
		return method;
	}

	public static void invokeLastSetMethod(Object owner, Object value,
			String fieldName) {
		invokeLastMethod(owner, value, fieldName, true);
	}

	public static Object invokeLastGetMethod(Object owner, String fieldName) {
		return invokeLastMethod(owner, null, fieldName, false);
	}

	public static Object invokeLastMethod(Object owner, Object value,
			String fieldName, boolean isSet) {
		Method method = getLastMethod(owner, fieldName, isSet);
		try {
			return method.invoke(owner, value == null ? new Object[] {}
					: new Object[] { value });
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		}
		return method;
	}

	public static String getGetMethodName(String fieldName) {
		return getMethodName(fieldName, false);
	}

	public static String getSetMethodName(String fieldName) {
		return getMethodName(fieldName, true);
	}

	public static String getMethodName(String fieldName, boolean isSet) {
		return (isSet ? "s" : "g")
				+ "et"
				+ fieldName.substring(0, 1).toUpperCase()
				+ (fieldName.length() > 1 ? fieldName.substring(1,
						fieldName.length()) : "");
	}

	public static Field getField(Object object, String fieldName) {
		Class<?> clazz = object.getClass();
		while (clazz != null) {
			try {
				return clazz.getDeclaredField(fieldName);
			} catch (NoSuchFieldException e) {
			} catch (SecurityException e) {
				e.printStackTrace();
			}
			clazz = clazz.getSuperclass();
		}
		return null;
	}

	public static Class<?> getGetMethodReturnType(Object object,
			String fieldName) {
		return getLastGetMethod(object, fieldName).getReturnType();
	}

	public static Method getMethod(Object object, String methodName,
			Class<?>... parameterTypes) {
		Class<?> clazz = object.getClass();
		while (clazz != null) {
			try {
				return clazz
						.getDeclaredMethod(
								methodName,
								parameterTypes != null
										&& parameterTypes.length == 1
										&& parameterTypes[0] == null ? new Class<?>[] {}
										: parameterTypes);
			} catch (SecurityException e) {
				e.printStackTrace();
			} catch (NoSuchMethodException e) {
			}
			clazz = clazz.getSuperclass();
		}
		return null;
	}

	public static void invokeListSetMethod(Object list, int i, Object value) {
		Method method = getMethod(list, "set", new Class[] { int.class,
				Object.class });
		try {
			method.invoke(list, new Object[] { i, value });
		} catch (IllegalAccessException | IllegalArgumentException
				| InvocationTargetException e) {
			e.printStackTrace();
		}
	}

	public static void invokeListAddMethod(Object list, Object value) {
		Method method = getMethod(list, "add", new Class[] { Object.class });
		try {
			method.invoke(list, new Object[] { value });
		} catch (IllegalAccessException | IllegalArgumentException
				| InvocationTargetException e) {
			e.printStackTrace();
		}
	}
}
