package cbr.pt.frontend.console.editpolicy;

import cbr.geometry.IPoint;
import cbr.pt.frontend.command.CommandsFactory;
import cbr.pt.frontend.console.IConnector;
import cbr.pt.frontend.console.IIdentifiedAnchor;
import cbr.pt.frontend.console.IWireView;
import cbr.pt.frontend.console.requests.WireCreationRequest;

public class WireCreationEditPolicy extends AbstractEditPolicy {

	public static final String EDIT_POLICY_WIRE_CREATION = "Wire creation edit policy";

	public WireCreationEditPolicy() {
		super(EDIT_POLICY_WIRE_CREATION);
	}

	@Override
	public void deactivate() {
		setRequest(null);
		super.deactivate();
	}

	@Override
	public WireCreationRequest getRequest() {
		return (WireCreationRequest) super.getRequest();
	}

	@Override
	public void move(IPoint location) {
		WireCreationRequest request = getRequest();
		if (request.getSource() != null && request.getTarget() == null) {
			IPoint locatedLocation = location.copy();
			getHost().translateToRelative(locatedLocation);
			IConnector connector = request.getFeedback().getTargetConnector();
			connector.setLocation(locatedLocation);
			IIdentifiedAnchor anchorLocator = (IIdentifiedAnchor) getHost();
			anchorLocator.getLocator().locate(connector);
			getHost().translateToAbsolute(locatedLocation);
		}
	}

	@Override
	public void click(IPoint location) {
		WireCreationRequest request = getRequest();
		IIdentifiedAnchor anchor = (IIdentifiedAnchor) getHost();
		if (request.getSource() == null) {
			request.setSource(anchor);
			IWireView feedback = (IWireView) getContext().getFactory()
					.createCreationFeedback(request.getClazz());
			request.setFeedback(feedback);
			IPoint locatedLocation = location.copy();
			getHost().translateToRelative(locatedLocation);
			IConnector sourceConnector = feedback.getSourceConnector();
			sourceConnector.setLocation(locatedLocation);
			anchor.getLocator().locate(sourceConnector);
			getHost().translateToAbsolute(locatedLocation);
			feedback.getTargetConnector().setLocation((IPoint) location.copy());
			addToFeedbackLayer(feedback);
		} else if (request.getTarget() == null) {
			IIdentifiedAnchor sourceLocator = request.getSource();
			if (sourceLocator == anchor) {
				request.setSource(null);
				removeFromFeedbackLayer(request.getFeedback());
				request.setFeedback(null);
			} else {
				request.setTarget(anchor);
				IWireView feedback = request.getFeedback();
				CommandsFactory.createAndExecute("CreateWireCommand",
						getContext(), "Create wire",
						getContext().getCBRModel(), request.getClazz(),
						request.getSource(), request.getTarget(),
						(IPoint) feedback.getSourceConnector().getLocation()
								.copy(), (IPoint) feedback.getTargetConnector()
								.getLocation().copy());
				request.dispose();
				removeFromFeedbackLayer(feedback);
			}
		}
	}

}
