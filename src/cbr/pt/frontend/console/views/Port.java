package cbr.pt.frontend.console.views;

import cbr.pt.frontend.console.FeatureView;
import cbr.pt.frontend.console.editpolicy.HighlightEditPolicy;
import cbr.pt.frontend.console.editpolicy.MovableFeatureEditPolicy;
import cbr.pt.frontend.console.views.editpolicy.ConnectionCreationEditPolicy;
import cbr.pt.frontend.console.views.editpolicy.ModelSpecificExcludingSelectionEditPolicy;

public class Port extends FeatureView implements IPort {

	public Port() {
		setSize(getMinimumSize().copy());
		addEditPolicy(new HighlightEditPolicy());
		addEditPolicy(new ModelSpecificExcludingSelectionEditPolicy());
		addEditPolicy(new MovableFeatureEditPolicy());
		addEditPolicy(new ConnectionCreationEditPolicy());
	}

}
