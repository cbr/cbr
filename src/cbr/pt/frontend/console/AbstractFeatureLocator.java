package cbr.pt.frontend.console;

import cbr.geometry.IPoint;

public abstract class AbstractFeatureLocator implements IFeatureLocator {

	private IAreaView owner;

	public AbstractFeatureLocator(IAreaView owner) {
		this.owner = owner;
	}

	protected IAreaView getOwner() {
		return owner;
	}

	@Override
	public int locate(IPoint location, int side) {
		IAreaView translatedArea = getOwner().getParent();
		if (translatedArea != null)
			translatedArea.translateToRelative(location);
		int retSide = localLocate(location, side);
		if (translatedArea != null)
			translatedArea.translateToAbsolute(location);
		return retSide;
	}

	protected abstract int localLocate(IPoint location, int side);

}
