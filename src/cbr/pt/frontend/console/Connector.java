package cbr.pt.frontend.console;

import cbr.Compass;
import cbr.geometry.IPoint;
import cbr.geometry.IRectangle;
import cbr.geometry.Point;

public class Connector implements IConnector {

	private IPoint location = new Point(0, 0);

	private IIdentifiedAnchor anchor;

	private IWireView owner;

	private int side = Compass.UNKNOWN;

	public Connector() {
	}

	public Connector(IWireView owner) {
		this.owner = owner;
	}

	@Override
	public IPoint getLocation() {
		return location;
	}

	@Override
	public void setLocation(IPoint location) {
		if (!this.location.equals(location)) {
			this.location = location;
			update();
		}
	}

	@Override
	public IIdentifiedAnchor getAnchor() {
		if (anchor == null) {
			anchor = new ProxiedAnchorFinder(this);
		}
		return anchor;
	}

	@Override
	public IWireView getOwner() {
		return owner;
	}

	@Override
	public void setOwner(IWireView owner) {
		if (this.owner != owner) {
			this.owner = owner;
			update();
		}
	}

	@Override
	public int getSide() {
		if (side == -1) {
			IAnchor anchor = getAnchor();
			if (!(anchor instanceof ProxiedAnchorFinder)
					&& (anchor instanceof IAreaView)) {
				IAreaView iAnchor = (IAreaView) anchor;

				IAreaView parent = iAnchor.getParent();
				IRectangle bounds = parent.getBounds();

				iAnchor.translateTo(location);
				parent.translateTo(location);

				side = Compass.WEST;
				int delta = location.getX() - bounds.getX();
				delta = delta < 0 ? -delta : delta;
				int curDelta = bounds.getX() + bounds.getWidth()
						- location.getX();
				curDelta = curDelta < 0 ? -curDelta : curDelta;
				if (delta > curDelta && curDelta >= 0) {
					side = Compass.EAST;
					delta = curDelta;
				}
				curDelta = location.getY() - bounds.getY();
				curDelta = curDelta < 0 ? -curDelta : curDelta;
				if (delta > curDelta && curDelta >= 0) {
					side = Compass.NORTH;
					delta = curDelta;
				}
				curDelta = bounds.getY() + bounds.getHeight() - location.getY();
				curDelta = curDelta < 0 ? -curDelta : curDelta;
				if (delta > curDelta && curDelta >= 0) {
					side = Compass.SOUTH;
				}

				parent.translateFrom(location);
				iAnchor.translateFrom(location);
			}
		}
		return side;
	}

	@Override
	public void setSide(int side) {
		if (this.side != side) {
			this.side = side;
			update();
		}
	}

	@Override
	public void update() {
		setSide(getAnchor().getLocator().locate(this));
		if (getOwner() != null)
			getOwner().repaint();
	}

	@Override
	public void setAnchor(IIdentifiedAnchor anchor) {
		this.anchor = anchor;
	}

	@Override
	public boolean connect(IIdentifiedAnchor anchor) {
		if (getOwner().getSourceConnector() == this) {
			if (anchor.addSourceConnector(this)) {
				setAnchor(anchor);
				update();
				return true;
			}
		} else if (getOwner().getTargetConnector() == this) {
			if (anchor.addTargetConnector(this)) {
				setAnchor(anchor);
				update();
				return true;
			}
		}
		return false;
	}

	@Override
	public boolean disconnect() {
		if (getOwner().getSourceConnector() == this) {
			if (getAnchor().removeSourceConnector(this)) {
				setAnchor(null);
				update();
				return true;
			}
		} else if (getOwner().getTargetConnector() == this) {
			if (getAnchor().removeTargetConnector(this)) {
				setAnchor(null);
				update();
				return true;
			}
		}
		return false;
	}
}
