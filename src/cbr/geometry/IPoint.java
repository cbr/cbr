package cbr.geometry;

import cbr.ICopiable;
import cbr.serialize.xml.annotations.XMLAttribute;
import cbr.serialize.xml.annotations.XMLNode;

@XMLNode("point")
public interface IPoint extends ITranslatable, ICopiable {

	@XMLAttribute
	public int getX();

	@XMLAttribute
	public int getY();

	public void setX(int x);

	public void setY(int y);

	@Override
	public IPoint copy();

}
