package cbr.pt.frontend.console.editpolicy;

import cbr.Compass;
import cbr.geometry.IDimension;
import cbr.geometry.IPoint;
import cbr.geometry.IRectangle;
import cbr.geometry.operators.BoundsXYCoordsDependsOperator;
import cbr.geometry.operators.IDimensionCoordsDependsOperator;
import cbr.geometry.operators.IPointCoordsDependsOperator;
import cbr.geometry.operators.IRectangleCoordsDependsOperator;
import cbr.geometry.operators.LocationXYCoordsDependsOperator;
import cbr.geometry.operators.SizeXYCoordsDependsOperator;
import cbr.pt.frontend.command.CommandsFactory;
import cbr.pt.frontend.console.IAreaView;
import cbr.pt.frontend.console.service.MoveAndResizeHandles;

public class MovableAndResizableAreaEditPolicy extends MovableAreaEditPolicy {

	public static final String EDIT_POLICY_MOVE_RESIZE = "Movable and resizable area edit policy";

	private int resizeDirection;

	public MovableAndResizableAreaEditPolicy() {
		setName(EDIT_POLICY_MOVE_RESIZE);
	}

	@Override
	public MoveAndResizeHandles getServiceView() {
		return (MoveAndResizeHandles) super.getServiceView();
	}

	@Override
	public void startDrag(IPoint location) {
		super.startDrag(location);
		resizeDirection = getServiceView().getResizeSupportedDir(location);
	}

	@Override
	protected void tryToApplyServiceView(IPoint delta) {
		if (resizeDirection == Compass.UNKNOWN) {
			super.tryToApplyServiceView(delta);
		} else {
			resize(getServiceView().getBounds(), getHost().getMinimumSize(),
					delta, resizeDirection);
		}
	}

	@Override
	protected void tryToCommit(IPoint delta) {
		if (resizeDirection == Compass.UNKNOWN) {
			super.tryToCommit(delta);
		} else {
			IAreaView host = getHost();
			IRectangle bounds = host.getBounds().copy();
			IAreaView parent = host.getParent();
			if (parent != null)
				parent.translateToAbsolute(bounds);
			resize(bounds, getHost().getMinimumSize(), delta, resizeDirection);
			if (parent != null)
				parent.translateToRelative(bounds);
			CommandsFactory.createAndExecute("SetConstraintCommand",
					getContext(), "Resize/move", getHost(), bounds);
		}
	}

	@Override
	public IAreaView createServiceView() {
		return new MoveAndResizeHandles();
	}

	public static void resize(IRectangle bounds, IDimension minimumSize,
			IPoint delta, int resizeDir) {
		if (resizeDir == Compass.WEST) {
			resizeFixCoord(bounds, minimumSize, delta, true);
		} else if (resizeDir == Compass.WEST_NORTH) {
			resizeFixCoord(bounds, minimumSize, delta, true);
			resizeFixCoord(bounds, minimumSize, delta, false);
		} else if (resizeDir == Compass.NORTH) {
			resizeFixCoord(bounds, minimumSize, delta, false);
		} else if (resizeDir == Compass.NORTH_EAST) {
			resizeFixCoord(bounds, minimumSize, delta, false);
			resizeFixSize(bounds, minimumSize, delta, true);
		} else if (resizeDir == Compass.EAST) {
			resizeFixSize(bounds, minimumSize, delta, true);
		} else if (resizeDir == Compass.EAST_SOUTH) {
			resizeFixSize(bounds, minimumSize, delta, true);
			resizeFixSize(bounds, minimumSize, delta, false);
		} else if (resizeDir == Compass.SOUTH) {
			resizeFixSize(bounds, minimumSize, delta, false);
		} else if (resizeDir == Compass.SOUTH_WEST) {
			resizeFixCoord(bounds, minimumSize, delta, true);
			resizeFixSize(bounds, minimumSize, delta, false);
		}
	}

	private static void resizeFixSize(IRectangle bounds,
			IDimension minimumSize, IPoint delta, boolean isX) {
		IDimension size = bounds.getSize();
		int fixedSize = isX ? size.getWidth() + delta.getX() : size.getHeight()
				+ delta.getY();
		int minSize = isX ? minimumSize.getWidth() : minimumSize.getHeight();
		if (fixedSize < minSize) {
			fixedSize = minSize;
		}
		if (isX) {
			size.setWidth(fixedSize);
		} else {
			size.setHeight(fixedSize);
		}
	}

	private static void resizeFixCoord(IRectangle bounds,
			IDimension minimumSize, IPoint delta, boolean isX) {
		IPoint location = bounds.getLocation();
		IDimension size = bounds.getSize();
		int origCoord = isX ? location.getX() : location.getY();
		int origSize = isX ? size.getWidth() : size.getHeight();
		int deltaVal = isX ? delta.getX() : delta.getY();
		int minSize = isX ? minimumSize.getWidth() : minimumSize.getHeight();
		int fixedCoord = origCoord + deltaVal;
		int fixedSize = origSize - deltaVal;
		if (minSize > fixedSize) {
			fixedCoord = origCoord + origSize - minSize;
			fixedSize = minSize;
		}
		if (isX) {
			location.setX(fixedCoord);
			size.setWidth(fixedSize);
		} else {
			location.setY(fixedCoord);
			size.setHeight(fixedSize);
		}
	}

	/*
	 * =============================== coordinates depends operations (not used)
	 */

	public static void _resize(IRectangle bounds, IDimension minimumSize,
			IPoint delta, int resizeDir) {
		IRectangleCoordsDependsOperator cdBounds = new BoundsXYCoordsDependsOperator(
				true, bounds);
		IDimensionCoordsDependsOperator cdMinimumSize = new SizeXYCoordsDependsOperator(
				true, minimumSize);
		IPointCoordsDependsOperator cdDelta = new LocationXYCoordsDependsOperator(
				true, delta);
		if (resizeDir == Compass.WEST) {
			_resizeFixCoord(cdBounds, cdMinimumSize, cdDelta);
		} else if (resizeDir == Compass.WEST_NORTH) {
			_resizeFixCoord(cdBounds, cdMinimumSize, cdDelta);
			_resizeFixCoord(cdBounds.change(), cdMinimumSize.change(),
					cdDelta.change());
		} else if (resizeDir == Compass.NORTH) {
			_resizeFixCoord(cdBounds.change(), cdMinimumSize.change(),
					cdDelta.change());
		} else if (resizeDir == Compass.NORTH_EAST) {
			_resizeFixCoord(cdBounds.change(), cdMinimumSize.change(),
					cdDelta.change());
			_resizeFixSize(cdBounds, cdMinimumSize, cdDelta);
		} else if (resizeDir == Compass.EAST) {
			_resizeFixSize(cdBounds, cdMinimumSize, cdDelta);
		} else if (resizeDir == Compass.EAST_SOUTH) {
			_resizeFixSize(cdBounds, cdMinimumSize, cdDelta);
			_resizeFixSize(cdBounds.change(), cdMinimumSize.change(),
					cdDelta.change());
		} else if (resizeDir == Compass.SOUTH) {
			_resizeFixSize(cdBounds.change(), cdMinimumSize.change(),
					cdDelta.change());
		} else if (resizeDir == Compass.SOUTH_WEST) {
			_resizeFixCoord(cdBounds, cdMinimumSize, cdDelta);
			_resizeFixSize(cdBounds.change(), cdMinimumSize.change(),
					cdDelta.change());
		}
	}

	private static void _resizeFixSize(
			IRectangleCoordsDependsOperator cdBounds,
			IDimensionCoordsDependsOperator cdMinimumSize,
			IPointCoordsDependsOperator cdDelta) {
		int fixedSize = cdBounds.getCoord() + cdBounds.getSize();
		int minSize = cdMinimumSize.getSize();
		if (fixedSize < minSize)
			fixedSize = minSize;
		cdBounds.setSize(fixedSize);
	}

	private static void _resizeFixCoord(
			IRectangleCoordsDependsOperator cdBounds,
			IDimensionCoordsDependsOperator cdMinimumSize,
			IPointCoordsDependsOperator cdDelta) {
		int origCoord = cdBounds.getCoord();
		int origSize = cdBounds.getSize();
		int deltaVal = cdDelta.getCoord();
		int minSize = cdMinimumSize.getSize();
		int fixedCoord = origCoord + deltaVal;
		int fixedSize = origSize - deltaVal;
		if (minSize > fixedSize) {
			fixedCoord = origCoord + origSize - minSize;
			fixedSize = minSize;
		}
		cdBounds.setCoord(fixedCoord);
		cdBounds.setSize(fixedSize);
	}

}
