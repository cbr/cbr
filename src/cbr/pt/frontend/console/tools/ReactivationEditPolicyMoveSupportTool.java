package cbr.pt.frontend.console.tools;

import java.util.ArrayList;
import java.util.List;

import cbr.geometry.IPoint;
import cbr.pt.frontend.console.CBRContext;
import cbr.pt.frontend.console.IAreaView;
import cbr.pt.frontend.console.editpolicy.AbstractEditPolicy;

public class ReactivationEditPolicyMoveSupportTool extends AbstractTool {

	protected IAreaView view;

	protected List<String> supportedPoliciesNames = new ArrayList<String>();

	public ReactivationEditPolicyMoveSupportTool(CBRContext context) {
		super(context);
	}

	protected boolean isSupported(AbstractEditPolicy editPolicy) {
		return true;
	}

	public void addSupportedPolicyName(String policyName) {
		if (!supportedPoliciesNames.contains(policyName))
			supportedPoliciesNames.add(policyName);
	}

	@Override
	public boolean handleMouseMoved(IAreaView view, IPoint location) {
		boolean reactivate = reactivateEditPolicies(view);
		boolean setLocation = setLocationViewPolicies(view, location);
		return reactivate || setLocation;
	}

	@Override
	public boolean handleMouseEntered(IAreaView view, IPoint location) {
		boolean reactivate = reactivateEditPolicies(view);
		boolean setLocation = setLocationViewPolicies(view, location);
		return reactivate || setLocation;
	}

	@Override
	public boolean handleMouseExited(IAreaView view, IPoint location) {
		boolean reactivate = reactivateEditPolicies(view);
		boolean setLocation = setLocationViewPolicies(view, location);
		return reactivate || setLocation;
	}

	@Override
	public boolean handleMouseDragged(IAreaView view, IPoint location) {
		return false;
	}

	@Override
	public boolean handleMousePressed(IAreaView view, IPoint location) {
		return false;
	}

	@Override
	public boolean handleMouseReleased(IAreaView view, IPoint location) {
		return false;
	}

	@Override
	public boolean handleMouseClicked(IAreaView view, IPoint location) {
		boolean reactivate = reactivateEditPolicies(view);
		boolean click = clickViewPolicies(view, location);
		return reactivate || click;
	}

	@Override
	public void unselect() {
		deactivateActivePolicies();
		super.unselect();
	}

	@Override
	public void select() {
		super.select();
	}

	protected boolean reactivateEditPolicies(IAreaView view) {
		if (this.view != view) {
			boolean deactivateResult = deactivateActivePolicies();
			boolean activateResult = activateViewPolicies(view);
			return activateResult || deactivateResult;
		}
		return false;
	}

	private boolean deactivateActivePolicies() {
		if (view == null)
			return false;
		boolean isNeedRepaint = false;
		for (String supportedPolicyName : supportedPoliciesNames) {
			AbstractEditPolicy supportedEditPolicy = view
					.getEditPolicy(supportedPolicyName);
			if (supportedEditPolicy != null && isSupported(supportedEditPolicy)) {
				deactivateViewPolicy(supportedEditPolicy);
				if (!isNeedRepaint)
					isNeedRepaint = true;
			}
		}
		return isNeedRepaint;
	}

	private boolean activateViewPolicies(IAreaView view) {
		if (view == null)
			return false;
		this.view = view;
		boolean isNeedRepaint = false;
		for (String supportedPolicyName : supportedPoliciesNames) {
			AbstractEditPolicy supportedEditPolicy = view
					.getEditPolicy(supportedPolicyName);
			if (supportedEditPolicy != null && isSupported(supportedEditPolicy)) {
				activateViewPolicy(supportedEditPolicy);
				if (!isNeedRepaint)
					isNeedRepaint = true;
			}
		}
		return isNeedRepaint;
	}

	private boolean setLocationViewPolicies(IAreaView view, IPoint location) {
		if (view == null)
			return false;
		this.view = view;
		boolean isNeedRepaint = false;
		for (String supportedPolicyName : supportedPoliciesNames) {
			AbstractEditPolicy supportedEditPolicy = view
					.getEditPolicy(supportedPolicyName);
			if (supportedEditPolicy != null && isSupported(supportedEditPolicy)) {
				setLocationViewPolicy(supportedEditPolicy, location);
				if (!isNeedRepaint)
					isNeedRepaint = true;
			}
		}
		return isNeedRepaint;
	}

	private boolean clickViewPolicies(IAreaView view, IPoint location) {
		if (view == null)
			return false;
		this.view = view;
		boolean isNeedRepaint = false;
		for (String supportedPolicyName : supportedPoliciesNames) {
			AbstractEditPolicy supportedEditPolicy = view
					.getEditPolicy(supportedPolicyName);
			if (supportedEditPolicy != null && isSupported(supportedEditPolicy)) {
				clickViewPolicy(supportedEditPolicy, location);
				if (!isNeedRepaint)
					isNeedRepaint = true;
			}
		}
		return isNeedRepaint;
	}

	protected void setLocationViewPolicy(AbstractEditPolicy policy,
			IPoint location) {
		policy.move(location);
	}

	protected void clickViewPolicy(AbstractEditPolicy policy, IPoint location) {
		policy.click(location);
	}

	protected void activateViewPolicy(AbstractEditPolicy policy) {
		policy.activate();
	}

	protected void deactivateViewPolicy(AbstractEditPolicy policy) {
		policy.deactivate();
	}

}
