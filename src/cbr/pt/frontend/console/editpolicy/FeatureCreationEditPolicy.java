package cbr.pt.frontend.console.editpolicy;

import cbr.Compass;
import cbr.geometry.IPoint;
import cbr.pt.frontend.command.CommandsFactory;
import cbr.pt.frontend.console.IAreaView;
import cbr.pt.frontend.console.IFeatureLocator;
import cbr.pt.frontend.console.IFeatureLocatorProvider;
import cbr.pt.frontend.console.IFeatureView;

public class FeatureCreationEditPolicy extends
		MoveSupportReactivatedCreationEditPolicy {

	public static final String EDIT_POLICY_FEATURE_CREATION = "Feature creation edit policy";

	private int side = Compass.UNKNOWN;

	public FeatureCreationEditPolicy() {
		super(EDIT_POLICY_FEATURE_CREATION);
	}

	@Override
	public void move(IPoint location) {
		updateFeedback(location);
	}

	@Override
	public void click(IPoint location) {
		tryToCommit(location);
	}

	protected void tryToCommit(IPoint location) {
		updateFeedback(location);
		CommandsFactory.createAndExecute("CreateFeatureCommand", getContext(),
				"Create feature", getHost(), getCurrentClass(),
				getFeedbackView().getBounds(), side);
	}

	private void updateFeedback(IPoint location) {
		IAreaView view = getFeedbackView();
		IPoint centerLocation = location.copy();
		IAreaView host = getHost();
		if (host instanceof IFeatureLocatorProvider) {
			@SuppressWarnings("unchecked")
			IFeatureLocator locator = ((IFeatureLocatorProvider) host)
					.getLocator((Class<? extends IFeatureView>) super
							.getCurrentClass());
			if (locator != null) {
				side = locator.locate(centerLocation, Compass.UNKNOWN);
			}
		}

		view.translateFromCenter(centerLocation);
		view.setLocation(centerLocation);
	}

}
