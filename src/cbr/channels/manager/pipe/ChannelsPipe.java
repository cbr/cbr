package cbr.channels.manager.pipe;

import java.util.Set;

import cbr.IChannelPathFinderAlgorythm;
import cbr.IPathBuilderAlgorythm;
import cbr.channels.IChannel;
import cbr.wires.IWire;

/**
 * 
 * Алгоритм работы<br>
 * <ul>
 * <li>Добавлние соединений на стадию поиск пути или на стадию построения</li>
 * <li>Запуск process()</li>
 * <li>Получение готовых соединений, т.е. тех, что изменились. getCompletedWires
 * </li>
 * <li>Чистка clean()</li>
 * </ul>
 * 
 * @author Alexander Strakh
 * 
 */
public class ChannelsPipe {

	private ChannelsPipePathFinderStage pathFinder;

	private ChannelsPipePathBuilderStage pathBuilder;

	public ChannelsPipe() {
		pathBuilder = new ChannelsPipePathBuilderStage();
		pathFinder = new ChannelsPipePathFinderStage(pathBuilder);
	}

	public void process() {
		pathFinder.process();
		pathBuilder.process();
	}

	public void clean() {
		pathFinder.clean();
		pathBuilder.clean();
	}

	public Set<IWire> getCompletedWires() {
		return pathBuilder.getCompletedWires();
	}

	public void addToBuildPathStageIvalidWire(IWire wire) {
		pathBuilder.addInvalidWire(wire);
	}

	public void addToBuildPathStage(IChannel channel) {
		pathBuilder.add(channel);
	}

	public void addToFindPathStage(IChannel channel) {
		pathFinder.add(channel);
	}

	public void removeChannelFromPipe(IChannel channel) {
		pathBuilder.removeChannelFromQueue(channel);
	}

	public void configurePathFinderStage(IChannelPathFinderAlgorythm algorythm) {
		pathFinder.setAlgorythm(algorythm);
	}

	public void configurePathFinderStage(IPathBuilderAlgorythm algorythm) {
		pathBuilder.setAlgorythm(algorythm);
	}

}
