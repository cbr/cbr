package cbr.pt.frontend.console.views;

import cbr.grid.ICell;
import cbr.grid.IObstaclesBoundedGrid;
import cbr.pt.frontend.console.CBRGraphics;
import cbr.pt.frontend.console.CBRZone;
import cbr.pt.frontend.console.IAreaView;
import cbr.pt.frontend.console.editpolicy.HighlightEditPolicy;
import cbr.pt.frontend.console.editpolicy.MovableAndResizableAreaEditPolicy;
import cbr.pt.frontend.console.views.editpolicy.ModelSpecificExcludingSelectionEditPolicy;
import cbr.pt.router.CBRouter;

public class Zone extends CBRZone {

	public Zone() {
		addEditPolicy(new HighlightEditPolicy());
		addEditPolicy(new ModelSpecificExcludingSelectionEditPolicy());
		addEditPolicy(new MovableAndResizableAreaEditPolicy());
	}

	public Model getModel() {
		return (Model) getParent();
	}

	public IObstaclesBoundedGrid getGrid() {
		Model model = getModel();
		return model == null ? null : model.getGrid();
	}

	public CBRouter getRouter() {
		Model model = getModel();
		return model == null ? null : model.getRouter();
	}

	@Override
	public void setParent(IAreaView parent) {
		super.setParent(parent);
		updateGridByConstraintChanges();
	}
	
	public void updateGridByConstraintChanges() {
		if (getRouter() != null) {
			getRouter().uddateGridByContainerConstraintChanges(this);
		}		
	}

	@Override
	public void postPaint(CBRGraphics g) {
		super.postPaint(g);
		IObstaclesBoundedGrid grid = getGrid();
		if (grid != null) {
			ICell[][] cells = grid.getCells();
			for (int i = 0; i < grid.getColumnsCount(); i++) {
				for (int j = 0; j < grid.getRowsCount(); j++) {
					g.drawRect(cells[i][j].getX(), cells[i][j].getY(),
							cells[i][j].getWidth(), cells[i][j].getHeight());
				}
			}
		}
	}

	@Override
	public void boundsChanged() {
		super.boundsChanged();
		if (getRouter() != null) {
			getRouter().uddateGridByContainerConstraintChanges(this);
		}
		// getGrid().setBounds((IRectangle) getBounds().copy());
	}

}
