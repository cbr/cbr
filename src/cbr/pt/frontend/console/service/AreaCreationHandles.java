package cbr.pt.frontend.console.service;

import java.awt.Color;

import cbr.pt.frontend.console.AbstractView;
import cbr.pt.frontend.console.CBRGraphics;

public class AreaCreationHandles extends AbstractView {

	@Override
	public void prePaint(CBRGraphics graphics) {
		graphics.setAlpha(0.2f);
		graphics.setColor(Color.white);
		graphics.fillRect(getBounds());
		graphics.setAlpha(1f);
		graphics.setColor(Color.gray);
		graphics.drawRect(getBounds());
	}
}
