package cbr.pt.frontend.command;

import java.util.ArrayList;
import java.util.List;

import cbr.pt.frontend.console.CBRContext;

public class CompoundCommand extends AbstractCommand {

	private List<ICommand> commands = new ArrayList<ICommand>();

	public CompoundCommand(CBRContext context, String description) {
		super(context, description);
	}

	public void addCommand(ICommand command) {
		commands.add(command);
	}

	@Override
	public void execute() {
		for (ICommand command : commands) {
			if (command == null)
				throw new UnsupportedOperationException(
						"Null command not executed!");
			command.execute();
		}
	}

	@Override
	public boolean canExecute() {
		for (int i = 0; i < commands.size(); i++) {
			if (!commands.get(i).canExecute())
				return false;
		}
		return true;
	}

	@Override
	public void undo() {
		for (int i = commands.size() - 1; i >= 0; i--) {
			commands.get(i).undo();
		}
	}

	@Override
	public boolean canUndo() {
		for (int i = commands.size() - 1; i >= 0; i--) {
			if (!commands.get(i).canUndo())
				return false;
		}
		return true;
	}

	@Override
	public void dispose() {
		for (ICommand command : commands) {
			command.dispose();
		}
		commands.clear();
		super.dispose();
	}

	@Override
	public void disconnectFromContext() {
		for (ICommand command : commands) {
			if (command == null)
				throw new UnsupportedOperationException(
						"Null command not executed!");
			command.disconnectFromContext();
		}
		super.disconnectFromContext();
	}

	@Override
	public void connectToContext(CBRContext context) {
		super.connectToContext(context);
		for (ICommand command : commands) {
			if (command == null)
				throw new UnsupportedOperationException(
						"Null command not executed!");
			command.connectToContext(context);
		}
	}

}
