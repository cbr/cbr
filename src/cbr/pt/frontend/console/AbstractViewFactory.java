package cbr.pt.frontend.console;

import cbr.serialize.xml.XMLSerializer;

public abstract class AbstractViewFactory {

	private CBRContext context;

	public void setContext(CBRContext context) {
		this.context = context;
	}

	public CBRContext getContext() {
		return context;
	}

	public CBRModel createModelViewWithId(int x, int y, int width, int height) {
		CBRModel view = createModelViewWithId();
		view.getLocation().setX(x);
		view.getLocation().setY(y);
		view.getSize().setWidth(width);
		view.getSize().setHeight(height);
		return view;
	}

	public IIdentifiedView createAreaViewWithId(int x, int y, int width,
			int height) {
		IIdentifiedView view = createAreaViewWithId();
		view.getLocation().setX(x);
		view.getLocation().setY(y);
		view.getSize().setWidth(width);
		view.getSize().setHeight(height);
		return view;
	}

	public IWireView createWireViewWithId(int sourceId, int targetId) {
		IWireView view = createWireViewWithId();
		view.setSourceId(sourceId);
		view.setTargetId(targetId);
		return view;
	}

	public IFeatureView createFeatureViewWithId(int x, int y, int width,
			int height) {
		IFeatureView view = createFeatureView();
		view.getLocation().setX(x);
		view.getLocation().setY(y);
		view.getSize().setWidth(width);
		view.getSize().setHeight(height);
		return view;
	}

	public IIdentifiedView createAreaViewWithId() {
		IIdentifiedView view = createAreaView();
		view.setId(getContext().getUniqueId());
		view.setContext(getContext());
		return view;
	}

	public IWireView createWireViewWithId() {
		IWireView view = createWireView();
		view.setId(getContext().getUniqueId());
		view.setContext(getContext());
		return view;
	}

	public IFeatureView createFeatureViewWithId() {
		IFeatureView view = createFeatureView();
		view.setId(getContext().getUniqueId());
		view.setContext(getContext());
		return view;
	}

	public CBRModel createModelViewWithId() {
		CBRModel view = createModelView();
		view.setContext(getContext());
		return view;
	}

	public abstract IIdentifiedView createAreaView();

	public abstract IWireView createWireView();

	public abstract IFeatureView createFeatureView();

	public abstract CBRModel createModelView();

	public abstract IAreaView createCreationFeedback(
			Class<? extends IAreaView> clazz);

	public abstract IAreaView create(Class<? extends IAreaView> clazz);

	public abstract XMLSerializer createSerializer();

}
