package cbr.pt.frontend.command;

import cbr.pt.frontend.console.CBRContext;
import cbr.pt.frontend.console.IAreaView;
import cbr.pt.frontend.console.IIdentified;

public class DeleteAreaCommand extends AbstractCommand {

	private IAreaView view;

	private IAreaView parent;

	private int viewId = UNDEFINED;

	protected DeleteAreaCommand(CBRContext context, String description,
			IAreaView view) {
		super(context, description);
		this.view = view;
		fillParentFiled();
	}

	private void fillParentFiled() {
		this.parent = view.getParent();
	}

	@Override
	public boolean canExecute() {
		return parent != null && view != null;
	}

	@Override
	public void execute() {
		parent.removeChild(view);
	}

	@Override
	public void undo() {
		parent.addChild(view);
	}

	@Override
	public void disconnectFromContext() {
		if (view instanceof IIdentified) {
			viewId = ((IIdentified) view).getId();
		} else
			throw new UnsupportedOperationException(
					"Errors due to disconnect from context for DeleteAreaCommand. View: \""
							+ view.getClass().getSimpleName()
							+ "\" should be IIdentified.");
		super.disconnectFromContext();
	}

	@Override
	public void connectToContext(CBRContext context) {
		super.connectToContext(context);
		if (viewId == UNDEFINED)
			throw new UnsupportedOperationException(
					"Errors due to context connection. Undefined view id for DeleteAreaCommand.");
		else {
			view = findModelById(viewId);
			if (view != null) {
				viewId = UNDEFINED;
				fillParentFiled();
			}
		}
	}

}
