package cbr.serialize.xml;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cbr.ReflectionUtils;
import cbr.serialize.xml.annotations.XMLAttribute;
import cbr.serialize.xml.annotations.XMLElement;
import cbr.serialize.xml.annotations.XMLNode;
import cbr.serialize.xml.annotations.XMLReference;
import cbr.serialize.xml.annotations.XMLTransient;

public class XMLSerializerUtils {

	public static XMLSerializableObject createXMLSerializableObject(
			Object object) throws IllegalAccessException,
			IllegalArgumentException, InvocationTargetException {
		List<Class<?>> classesXMLNode = ReflectionUtils
				.sortInterfacesByInheritance(ReflectionUtils
						.getAllTypeAnnotatedInterfacesWith(object,
								XMLNode.class));
		if (classesXMLNode.isEmpty()) {
			if (object instanceof List<?>) {
				Map<String, XMLSerializableObject> elements = new HashMap<String, XMLSerializableObject>();
				List<?> list = (List<?>) object;
				for (int i = 0; i < list.size(); i++) {
					Object listObj = list.get(i);
					if (listObj == null)
						continue;
					XMLSerializableObject xmlSerializableObject = createXMLSerializableObject(listObj);
					if (xmlSerializableObject == null)
						continue;
					elements.put(i + "", xmlSerializableObject);
				}
				if (elements.size() > 0) {
					return new XMLSerializableObject("list",
							new HashMap<String, String>(0), elements);
				}
			}
			return null;
		}
		String name = classesXMLNode.get(0).getAnnotation(XMLNode.class)
				.value();
		if (name.isEmpty()) {
			name = classesXMLNode.get(0).getCanonicalName();
		}

		List<String> transientFields = new ArrayList<String>();
		List<Class<?>> classesWithTransientAnnotations = ReflectionUtils
				.getAllTypeAnnotatedInterfacesWith(object, XMLTransient.class);
		for (Class<?> clazz : classesWithTransientAnnotations) {
			for (String fieldName : clazz.getAnnotation(XMLTransient.class)
					.value()) {
				if (!transientFields.contains(fieldName)) {
					transientFields.add(fieldName);
				}
			}
		}

		Map<String, Method> attributesMethods = new HashMap<String, Method>();
		Map<String, Method> elementsMethods = new HashMap<String, Method>();
		Collections.reverse(classesXMLNode);
		for (Class<?> clazz : classesXMLNode) {
			putNames(clazz, transientFields, elementsMethods, true);
			putNames(clazz, transientFields, attributesMethods, false);
		}
		Map<String, String> attributes = new HashMap<String, String>();
		Object[] args = new Object[0];
		for (String key : attributesMethods.keySet()) {
			Method method = attributesMethods.get(key);
			Object retObject = method.invoke(object, args);
			if (retObject != null) {
				String retObjectString = retObject.toString();
				if (!retObjectString.isEmpty()) {
					attributes.put(key, retObjectString);
				}
			}
		}
		Map<String, XMLSerializableObject> elements = new HashMap<String, XMLSerializableObject>();
		for (String key : elementsMethods.keySet()) {
			Method method = elementsMethods.get(key);
			Object retObject = method.invoke(object, args);
			if (retObject != null) {
				XMLSerializableObject xmlSerializableObject = null;
				if (method.isAnnotationPresent(XMLReference.class)) {
					String fieldName = method.getAnnotation(XMLReference.class)
							.value();
					String refStr = ReflectionUtils.invokeLastGetMethod(
							retObject, fieldName).toString();
					Map<String, String> attributesForRef = new HashMap<String, String>(
							1);
					attributesForRef.put(fieldName, refStr);
					xmlSerializableObject = new XMLSerializableObject(
							"reference", attributesForRef);
				} else {
					xmlSerializableObject = createXMLSerializableObject(retObject);
				}
				if (xmlSerializableObject != null) {
					elements.put(key, xmlSerializableObject);
				}
			}
		}
		return new XMLSerializableObject(name, attributes, elements);
	}

	public static void putNames(Class<?> clazz, List<String> transFeilds,
			Map<String, Method> map, boolean isElement) {
		for (Method method : ReflectionUtils
				.getAllAnnotatedMethodsOnlyForCurrentClassWith(clazz,
						isElement ? XMLElement.class : XMLAttribute.class)) {
			String valueName = isElement ? method.getAnnotation(
					XMLElement.class).value() : method.getAnnotation(
					XMLAttribute.class).value();
			if (valueName.isEmpty()) {
				String methodName = method.getName();
				String withoutGetMethodName = methodName.substring(3,
						methodName.length());
				valueName = withoutGetMethodName.length() == 1 ? withoutGetMethodName
						.toLowerCase() : (withoutGetMethodName.substring(0, 1)
						.toLowerCase() + withoutGetMethodName.substring(1,
						withoutGetMethodName.length()));
			}
			if (!transFeilds.contains(valueName)) {
				map.put(valueName, method);
			}
		}
	}

}
