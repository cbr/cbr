package cbr.geometry.operators;

import cbr.geometry.IDimension;

public class SizeXYCoordsDependsOperator extends
		AbstractXYCoordsDependsOperator implements
		IDimensionCoordsDependsOperator {

	private IDimension location;

	public SizeXYCoordsDependsOperator(boolean isX, IDimension location) {
		super(isX);
		this.location = location;
	}

	@Override
	public void setSize(int coord) {
		if (isX)
			location.setWidth(coord);
		else
			location.setHeight(coord);
	}

	@Override
	public int getSize() {
		return isX ? location.getWidth() : location.getHeight();
	}

	@Override
	public SizeXYCoordsDependsOperator change() {
		super.change();
		return this;
	}

}
