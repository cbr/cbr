package cbr.serialize.xml;

import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.lang.reflect.InvocationTargetException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

public class XMLSerializer {

	private XMLSerializationMapping mapping = new XMLSerializationMapping();

	private IXMLSerializationStrategy serializationStrategy;

	public XMLSerializer(IXMLSerializationStrategy startegy) {
		this.serializationStrategy = startegy;
	}

	public void addMapping(String name, Class<?> iface, Class<?> clazz) {
		mapping.addMapping(name, iface, clazz);
	}

	public boolean isNameOfClass(String name) {
		return mapping.isNameOfClass(name);
	}

	public String serializeToString(Object object)
			throws ParserConfigurationException, TransformerException,
			IllegalAccessException, IllegalArgumentException,
			InvocationTargetException {
		XMLSerializableObject xsObject = XMLSerializerUtils
				.createXMLSerializableObject(object);
		return serializeXSObjectToString(xsObject);
	}

	public Object deserializeFromString(String string)
			throws IllegalAccessException, IllegalArgumentException,
			InvocationTargetException, ParserConfigurationException,
			TransformerException, SAXException, IOException,
			InstantiationException {
		XMLSerializableObject xsObject = deserializeXSObjectFromString(string);
		return xsObject.construct(mapping, serializationStrategy);
	}

	private XMLSerializableObject deserializeXSObjectFromString(String string)
			throws ParserConfigurationException, SAXException, IOException {
		DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory
				.newInstance();
		DocumentBuilder documentBuilder = documentBuilderFactory
				.newDocumentBuilder();
		Document document = documentBuilder.parse(new InputSource(
				new StringReader(string)));
		return XMLSerializableObject.createXSObject(
				document.getDocumentElement(), serializationStrategy, mapping);
	}

	private String serializeXSObjectToString(XMLSerializableObject xsObject)
			throws ParserConfigurationException, TransformerException {
		DocumentBuilderFactory docFactory = DocumentBuilderFactory
				.newInstance();
		DocumentBuilder docBuilder;
		docBuilder = docFactory.newDocumentBuilder();
		Document document = docBuilder.newDocument();
		xsObject.serialize(document, serializationStrategy);
		TransformerFactory transformerFactory = TransformerFactory
				.newInstance();
		Transformer transformer = transformerFactory.newTransformer();

		transformer.setOutputProperty(OutputKeys.INDENT, "yes");
		transformer.setOutputProperty(
				"{http://xml.apache.org/xslt}indent-amount", "2");
		DOMSource source = new DOMSource(document);

		StringWriter writer = new StringWriter();
		StreamResult result = new StreamResult(writer);
		transformer.transform(source, result);
		return writer.getBuffer().toString();
	}

}
