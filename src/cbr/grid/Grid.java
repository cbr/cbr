package cbr.grid;

import java.util.ArrayList;
import java.util.List;

import cbr.channels.manager.AbstractChannelsManager;
import cbr.geometry.IPoint;
import cbr.geometry.IRectangle;

public class Grid<T, I> implements IGrid<T, I> {

	public static final int UNKNOWN = -1;

	private int columnsCount;

	private int rowsCount;

	private AbstractChannelsManager<T, I> channelsManager;

	protected ICell[][] grid;

	public Grid() {
		setColumnsAndRowsCount(1, 1);
	}

	public Grid(int x, int y, int width, int height) {
		this();
		grid[0][0].setX(x);
		grid[0][0].setY(y);
		grid[0][0].setWidth(width);
		grid[0][0].setHeight(height);
	}

	private void updateGrid() {
		if (grid == null) {
			grid = new ICell[columnsCount][rowsCount];
			for (int i = 0; i < columnsCount; i++) {
				for (int j = 0; j < rowsCount; j++) {
					grid[i][j] = new Cell(i, j, this);
				}
			}
		} else if (columnsCount > grid.length) {
			if (grid[0].length == rowsCount) {
				ICell[][] newGrid = new ICell[columnsCount][rowsCount];
				for (int i = 0; i < grid.length; i++) {
					newGrid[i] = grid[i];
				}
				for (int i = grid.length; i < columnsCount; i++) {
					newGrid[i] = new ICell[rowsCount];
				}
				grid = newGrid;
			} else if (grid[0].length < rowsCount) {
				ICell[][] newGrid = new ICell[columnsCount][rowsCount];
				for (int i = 0; i < grid.length; i++) {
					for (int j = 0; i < grid[i].length; j++) {
						newGrid[i][j] = grid[i][j];
					}
				}
				grid = newGrid;
			} else {
				ICell[][] newGrid = new ICell[columnsCount][rowsCount];
				for (int i = 0; i < grid.length; i++) {
					for (int j = 0; j < rowsCount; j++) {
						newGrid[i][j] = grid[i][j];
					}
				}
				grid = newGrid;
			}
		} else if (columnsCount < grid.length || columnsCount == grid.length) {
			if (grid[0].length < rowsCount) {
				for (int i = 0; i < grid.length; i++) {
					ICell[] newColumn = new ICell[rowsCount];
					for (int j = 0; j < grid[i].length; j++) {
						newColumn[j] = grid[i][j];
					}
					grid[i] = newColumn;
				}
			}
		}
	}

	public void updateSides() {
		for (int i = 0; i < columnsCount; i++) {
			for (int j = 0; j < rowsCount; j++) {
				ICell cell = grid[i][j];
				if (i == 0) {
					if (cell.getWestSide() == null) {
						cell.setWestSide(new Side(null, cell));
					} else {
						cell.getWestSide().setNeighbour1(null);
						cell.getWestSide().setNeighbour2(cell);
					}
					if (cell.getEastSide() == null) {
						cell.setEastSide(new Side(cell, null));
					} else {
						cell.getEastSide().setNeighbour1(cell);
					}
				} else if (i == columnsCount - 1) {
					if (cell.getWestSide() == null) {
						cell.setWestSide((grid[i - 1][j].getEastSide()));
						cell.getWestSide().setNeighbour2(cell);
						// cell.setWestSide(new Side(grid[i - 1][j], cell));
					} else {
						if (cell.getWestSide() != grid[i - 1][j].getEastSide()) {
							cell.setWestSide(grid[i - 1][j].getEastSide());
						}
						cell.getWestSide().setNeighbour1(grid[i - 1][j]);
						cell.getWestSide().setNeighbour2(cell);
					}
					if (cell.getEastSide() == null) {
						cell.setEastSide(new Side(cell, null));
					} else {
						cell.getEastSide().setNeighbour1(cell);
						cell.getEastSide().setNeighbour2(null);
					}
				} else if (i > 0) {
					if (cell.getWestSide() == null) {
						cell.setWestSide((grid[i - 1][j].getEastSide()));
						cell.getWestSide().setNeighbour2(cell);
						// cell.setWestSide(new Side(grid[i - 1][j], cell));
					} else {
						if (cell.getWestSide() != grid[i - 1][j].getEastSide()) {
							cell.setWestSide(grid[i - 1][j].getEastSide());
						}
						cell.getWestSide().setNeighbour1(grid[i - 1][j]);
						cell.getWestSide().setNeighbour2(cell);
					}
					if (cell.getEastSide() == null) {
						cell.setEastSide(new Side(cell, null));
					} else {
						cell.getEastSide().setNeighbour1(cell);
					}
				}

				if (j == 0) {
					if (cell.getNorthSide() == null) {
						cell.setNorthSide(new Side(null, cell));
					} else {
						cell.getNorthSide().setNeighbour1(null);
						cell.getNorthSide().setNeighbour2(cell);
					}
					if (cell.getSouthSide() == null) {
						cell.setSouthSide(new Side(cell, null));
					} else {
						cell.getSouthSide().setNeighbour1(cell);
					}
				} else if (j == rowsCount - 1) {
					if (cell.getNorthSide() == null) {
						cell.setNorthSide(grid[i][j - 1].getSouthSide());
						cell.getNorthSide().setNeighbour2(cell);
						// cell.setNorthSide(new Side(grid[i][j - 1], cell));
					} else {
						if (cell.getNorthSide() != grid[i][j - 1]
								.getSouthSide()) {
							cell.setNorthSide(grid[i][j - 1].getSouthSide());
						}
						cell.getNorthSide().setNeighbour1(grid[i][j - 1]);
						cell.getNorthSide().setNeighbour2(cell);
					}
					if (cell.getSouthSide() == null) {
						cell.setSouthSide(new Side(cell, null));
					} else {
						cell.getSouthSide().setNeighbour1(cell);
						cell.getSouthSide().setNeighbour2(null);
					}
				} else {
					if (cell.getNorthSide() == null) {
						cell.setNorthSide(grid[i][j - 1].getSouthSide());
						cell.getNorthSide().setNeighbour2(cell);
						// cell.setNorthSide(new Side(grid[i][j - 1], cell));
					} else {
						if (cell.getNorthSide() != grid[i][j - 1]
								.getSouthSide()) {
							cell.setNorthSide(grid[i][j - 1].getSouthSide());
						}
						cell.getNorthSide().setNeighbour1(grid[i][j - 1]);
						cell.getNorthSide().setNeighbour2(cell);
					}
					if (cell.getSouthSide() == null) {
						cell.setSouthSide(new Side(cell, null));
					} else {
						cell.getSouthSide().setNeighbour1(cell);
					}
				}
			}
		}
		// checkSides(grid, getColumnsCount(), getRowsCount());
	}

	public static final void assertCell(ICell[][] cells, int i, int j) {
		throw new UnsupportedOperationException("Assert for cell: (" + i + ", "
				+ j + ")");
	}

	public static final void checkSides(ICell[][] cells, int columnsCount,
			int rowsCount) {
		for (int i = 0; i < columnsCount; i++) {
			for (int j = 0; j < rowsCount; j++) {
				ICell cell = cells[i][j];
				// single cell
				if (columnsCount == 1 && rowsCount == 1) {
					if (cell.getWestCell() != null
							|| cell.getNorthCell() != null
							|| cell.getEastCell() != null
							|| cell.getSouthCell() != null) {
						assertCell(cells, i, j);
					}
					// horizontal line cells
				} else if (columnsCount == 1) {
					if (j == 0) {
						if (cell.getWestCell() != null
								|| cell.getNorthCell() != null
								|| cell.getEastCell() != null
								|| cell.getSouthCell() == null) {
							assertCell(cells, i, j);
						}
					} else if (j + 1 == rowsCount) {
						if (cell.getWestCell() != null
								|| cell.getNorthCell() == null
								|| cell.getEastCell() != null
								|| cell.getSouthCell() != null) {
							assertCell(cells, i, j);
						}
					} else if (cell.getWestCell() != null
							|| cell.getNorthCell() == null
							|| cell.getEastCell() != null
							|| cell.getSouthCell() == null) {
						assertCell(cells, i, j);
					}
					// vertical line cells
				} else if (rowsCount == 1) {
					if (i == 0) {
						if (cell.getWestCell() != null
								|| cell.getNorthCell() != null
								|| cell.getEastCell() == null
								|| cell.getSouthCell() != null) {
							assertCell(cells, i, j);
						}
					} else if (i + 1 == rowsCount) {
						if (cell.getWestCell() == null
								|| cell.getNorthCell() != null
								|| cell.getEastCell() != null
								|| cell.getSouthCell() != null) {
							assertCell(cells, i, j);
						}
					} else if (cell.getWestCell() == null
							|| cell.getNorthCell() != null
							|| cell.getEastCell() == null
							|| cell.getSouthCell() != null) {
						assertCell(cells, i, j);
					}
					// grid cells
				} else {
					if (i == 0 && j == 0) {
						if (cell.getWestCell() != null
								|| cell.getNorthCell() != null
								|| cell.getEastCell() == null
								|| cell.getSouthCell() == null) {
							assertCell(cells, i, j);
						}
					} else if (i + 1 == columnsCount && j == 0) {
						if (cell.getWestCell() == null
								|| cell.getNorthCell() != null
								|| cell.getEastCell() != null
								|| cell.getSouthCell() == null) {
							assertCell(cells, i, j);
						}
					} else if (i + 1 == columnsCount && j + 1 == rowsCount) {
						if (cell.getWestCell() == null
								|| cell.getNorthCell() == null
								|| cell.getEastCell() != null
								|| cell.getSouthCell() != null) {
							assertCell(cells, i, j);
						}
					} else if (i == 0 && j + 1 == rowsCount) {
						if (cell.getWestCell() != null
								|| cell.getNorthCell() == null
								|| cell.getEastCell() == null
								|| cell.getSouthCell() != null) {
							assertCell(cells, i, j);
						}
					} else if (i == 0) {
						if (cell.getWestCell() != null
								|| cell.getNorthCell() == null
								|| cell.getEastCell() == null
								|| cell.getSouthCell() == null) {
							assertCell(cells, i, j);
						}
					} else if (j == 0) {
						if (cell.getWestCell() == null
								|| cell.getNorthCell() != null
								|| cell.getEastCell() == null
								|| cell.getSouthCell() == null) {
							assertCell(cells, i, j);
						}
					} else if (i + 1 == columnsCount) {
						if (cell.getWestCell() == null
								|| cell.getNorthCell() == null
								|| cell.getEastCell() != null
								|| cell.getSouthCell() == null) {
							assertCell(cells, i, j);
						}
					} else if (j + 1 == rowsCount) {
						if (cell.getWestCell() == null
								|| cell.getNorthCell() == null
								|| cell.getEastCell() == null
								|| cell.getSouthCell() != null) {
							assertCell(cells, i, j);
						}
					} else if (cell.getWestCell() == null
							|| cell.getNorthCell() == null
							|| cell.getEastCell() == null
							|| cell.getSouthCell() == null) {
						assertCell(cells, i, j);
					}
				}
			}
		}
	}

	private void setColumnsCount(int columnsCount, boolean update) {
		if (columnsCount < 1)
			throw new UnsupportedOperationException(
					"Columns count can't be less than zero!");
		this.columnsCount = columnsCount;
		if (update)
			updateGrid();
	}

	private void setRowsCount(int rowsCount, boolean update) {
		if (rowsCount < 1)
			throw new UnsupportedOperationException(
					"Rows count can't be less than zero!");
		this.rowsCount = rowsCount;
		if (update)
			updateGrid();
	}

	@Override
	public void setColumnsCount(int columnsCount) {
		setColumnsCount(columnsCount, true);
	}

	@Override
	public void setRowsCount(int rowsCount) {
		setRowsCount(rowsCount, true);
	}

	@Override
	public void setColumnsAndRowsCount(int columnsCount, int rowsCount) {
		setColumnsCount(columnsCount, false);
		setRowsCount(rowsCount, false);
		updateGrid();
	}

	@Override
	public int getColumnsCount() {
		return columnsCount;
	}

	@Override
	public int getRowsCount() {
		return rowsCount;
	}

	@Override
	public ICell getCell(int columnNumber, int rowNumber) {
		return columnNumber < 0 || rowNumber < 0
				|| columnNumber >= getColumnsCount()
				|| rowNumber >= getRowsCount() ? null
				: grid[columnNumber][rowNumber];
	}

	@Override
	public boolean setCell(int columnNumber, int rowNumber, ICell cell) {
		if (columnNumber >= 0 && rowNumber >= 0
				&& columnNumber < getColumnsCount()
				&& rowNumber < getRowsCount()) {
			grid[columnNumber][rowNumber] = cell;
			return true;
		}
		return false;
	}

	/**
	 * 
	 * TODO: переделать на алгоритм поиска с делением на середины<br>
	 * 
	 * @param x
	 * @return
	 */
	protected int getColumnNumberContainsX(int x) {
		for (int i = 0; i < grid.length; i++) {
			if (x >= grid[i][0].getX()
					&& x < grid[i][0].getX() + grid[i][0].getWidth()) {
				return i;
			}
		}
		return UNKNOWN;
	}

	/**
	 * 
	 * TODO: переделать на алгоритм поиска с делением на середины<br>
	 * 
	 * @param y
	 * @return
	 */
	protected int getRowNumberContainsY(int y) {
		for (int i = 0; i < grid[0].length; i++) {
			if (y >= grid[0][i].getY()
					&& y < grid[0][i].getY() + grid[0][i].getHeight()) {
				return i;
			}
		}
		return UNKNOWN;
	}

	/**
	 * 
	 * Возвращает ячейку, которая содержит точку point.
	 * 
	 * @param point
	 * @return {@link ICell} containing point
	 */
	protected ICell getCellContainsPoint(IPoint point) {
		return getCell(getColumnNumberContainsX(point.getX()),
				getRowNumberContainsY(point.getY()));
	}

	/**
	 * 
	 * Делит строку горизонтальной линией, проходящей через y.<br>
	 * 
	 * @param y
	 * @return
	 */
	protected boolean divideRowsByY(int y) {
		int rowNumber = getRowNumberContainsY(y);
		if (rowNumber == UNKNOWN)
			return false;
		ICell cell = getCell(0, rowNumber);
		if (cell.getY() == y)
			return false;
		setRowsCount(getRowsCount() + 1);
		moveRowsDown(rowNumber + 1, 1);
		for (int i = 0; i < columnsCount; i++) {
			ICell[] dividedCells = grid[i][rowNumber].divideByY(y);
			grid[i][rowNumber] = dividedCells[0];
			grid[i][rowNumber + 1] = dividedCells[1];
		}
		return true;
	}

	/**
	 * 
	 * Объединяет сосоедние строки, коорые находятся по координатам y1, y2<br>
	 * 
	 * @param y
	 * @return
	 */
	protected boolean unionRowsByYCoords(int y1, int y2) {
		return unionRows(getRowNumberContainsY(y1), getRowNumberContainsY(y2));
	}

	protected boolean unionRows(int index1, int index2) {
		if (index1 > index2) {
			int temp = index1;
			index1 = index2;
			index2 = temp;
		}
		if (index1 < 0 || index2 < 0 || index1 >= getRowsCount()
				|| index2 >= getRowsCount() || index1 + 1 != index2)
			return false;
		for (int i = 0; i < columnsCount; i++) {
			grid[i][index1] = grid[i][index1].union(grid[i][index2]);
		}
		moveRowsUp(index2 + 1, 1);
		setRowsCount(getRowsCount() - 1);
		return true;
	}

	/**
	 * 
	 * Делит столбцы, вертикальной линией, проходящей через x.<br>
	 * 
	 * @param x
	 * @return true - если было выполнено деление
	 */
	protected boolean divideColumnsByX(int x) {
		int columnNumber = getColumnNumberContainsX(x);
		if (columnNumber == UNKNOWN)
			return false;
		ICell cell = getCell(columnNumber, 0);
		if (cell.getX() == x)
			return false;
		setColumnsCount(getColumnsCount() + 1);
		moveColumnsRight(columnNumber + 1, 1);
		for (int i = 0; i < rowsCount; i++) {
			ICell[] dividedCells = grid[columnNumber][i].divideByX(x);
			grid[columnNumber][i] = dividedCells[0];
			grid[columnNumber + 1][i] = dividedCells[1];
		}
		return true;
	}

	/**
	 * 
	 * Объединяет столбцы, которые содержат x1 и x2.<br>
	 * 
	 * Объединение происходит только, если x1 и x2 лежат<br>
	 * на соседних столбйах.
	 * 
	 * @param x
	 * @return true если было объединение
	 */
	protected boolean unionColumnsByXCoords(int x1, int x2) {
		return unionColumns(getColumnNumberContainsX(x1),
				getColumnNumberContainsX(x2));
	}

	protected boolean unionColumns(int index1, int index2) {
		if (index1 > index2) {
			int temp = index1;
			index1 = index2;
			index2 = temp;
		}
		if (index1 < 0 || index2 < 0 || index1 >= getColumnsCount()
				|| index2 >= getColumnsCount() || index1 + 1 != index2)
			return false;
		for (int i = 0; i < rowsCount; i++) {
			grid[index1][i] = grid[index1][i].union(grid[index2][i]);
		}
		moveColumnsLeft(index2 + 1, 1);
		setColumnsCount(getColumnsCount() - 1);
		return true;
	}

	/**
	 * 
	 * Перемещает все столбцы, начиная c fromColumnNumber вправо на shift.
	 * 
	 * TODO: оnметить как удаленные те столбцы, которые вышли за границу
	 * 
	 * @param fromColumnNumber
	 */
	private void moveColumnsRight(int fromColumnNumber, int shift) {
		for (int i = columnsCount - shift - 1; i >= fromColumnNumber; i--) {
			for (int j = 0; j < rowsCount; j++) {
				ICell cell = grid[i][j];
				if (cell != null) {
					grid[i + shift][j] = cell;
					cell.setColumn(i + shift);
				}
			}
		}
	}

	/**
	 * 
	 * Перемещает все столбцы, начиная c fromColumnNumber и заканчивая<br>
	 * самым последним правым столбцом влево на shift.
	 * 
	 * TODO: пометить как удаленные те столбцы, которые вышли за границу
	 * 
	 * @param fromColumnNumber
	 */
	private void moveColumnsLeft(int fromColumnNumber, int shift) {
		if (fromColumnNumber - shift < 0)
			fromColumnNumber += shift;
		for (int i = fromColumnNumber; i < columnsCount; i++) {
			for (int j = 0; j < rowsCount; j++) {
				ICell cell = grid[i][j];
				if (cell != null) {
					grid[i - shift][j] = cell;
					cell.setColumn(i - shift);
				}
			}
		}
	}

	/**
	 * 
	 * Перемещает все строки вниз, начиная с fromRowNumber на shift.
	 * 
	 * @param fromRowNumber
	 * @param shift
	 */
	protected void moveRowsDown(int fromRowNumber, int shift) {
		for (int i = 0; i < columnsCount; i++) {
			for (int j = rowsCount - shift - 1; j >= fromRowNumber; j--) {
				ICell cell = grid[i][j];
				if (cell != null) {
					grid[i][j + shift] = cell;
					cell.setRow(j + shift);
				}
			}
		}
	}

	/**
	 * 
	 * Перемещает все строки вверх, начиная с fromRowNumber на shift<br>
	 * и заканчивая самым нижним столбцом.
	 * 
	 * @param fromRowNumber
	 * @param shift
	 */
	protected void moveRowsUp(int fromRowNumber, int shift) {
		if (fromRowNumber - shift < 0)
			fromRowNumber += shift;
		for (int i = 0; i < columnsCount; i++) {
			for (int j = fromRowNumber; j < rowsCount; j++) {
				ICell cell = grid[i][j];
				if (cell != null) {
					grid[i][j - shift] = cell;
					cell.setRow(j - shift);
				}
			}
		}
	}

	protected void setTopBounds(int y) {
		if (y > grid[0][0].getY()) {
			shrinkTop(y);
		} else if (y < grid[0][0].getY()) {
			expandTop(y);
		}
	}

	protected void setBottomBounds(int y) {
		if (y > grid[0][getRowsCount() - 1].getY()
				+ grid[0][getRowsCount() - 1].getHeight()) {
			expandBottom(y);
		} else if (y < grid[0][getRowsCount() - 1].getY()
				+ grid[0][getRowsCount() - 1].getHeight()) {
			shrinkBottom(y);
		}
	}

	/**
	 * 
	 * Устанавливает левую границу сетки
	 * 
	 */
	protected void setLeftBounds(int x) {
		if (x > grid[0][0].getX()) {
			shrinkLeft(x);
		} else if (x < grid[0][0].getX()) {
			expandLeft(x);
		}
	}

	/**
	 * 
	 * Устанавливает правую границу сетки
	 * 
	 */
	protected void setRightBounds(int x) {
		if (x > grid[getColumnsCount() - 1][0].getX()
				+ grid[getColumnsCount() - 1][0].getWidth()) {
			expandRight(x);
		} else if (x < grid[getColumnsCount() - 1][0].getX()
				+ grid[getColumnsCount() - 1][0].getWidth()) {
			shrinkRight(x);
		}
	}

	protected void shrinkBottom(int y) {
		if (divideRowsByY(y)) {
			setRowsCount(getRowsCount() - 1);
		}
	}

	protected void shrinkRight(int x) {
		if (divideColumnsByX(x)) {
			setColumnsCount(getColumnsCount() - 1);
		}
	}

	protected void expandRight(int x) {
		boolean isContainsObstacle = false;
		for (int i = 0; i < getRowsCount(); i++) {
			if (grid[getColumnsCount() - 1][i].isObstacle()) {
				isContainsObstacle = true;
				break;
			}
		}
		if (isContainsObstacle) {
			setColumnsCount(getColumnsCount() + 1);
			for (int i = 0; i < getRowsCount(); i++) {
				grid[getColumnsCount() - 1][i]
						.setX(grid[getColumnsCount() - 2][i].getX()
								+ grid[getColumnsCount() - 2][i].getWidth());
				grid[getColumnsCount() - 1][i].setWidth(x
						- grid[getColumnsCount() - 2][i].getX());
				grid[getColumnsCount() - 1][i]
						.setY(grid[getColumnsCount() - 2][i].getY());
				grid[getColumnsCount() - 1][i]
						.setHeight(grid[getColumnsCount() - 2][i].getHeight());
			}
		} else {
			for (int i = 0; i < getRowsCount(); i++) {
				grid[getColumnsCount() - 1][i].setWidth(x
						- grid[getColumnsCount() - 1][i].getX());
			}
		}
	}

	protected void expandBottom(int y) {
		boolean isContainsObstacle = false;
		for (int i = 0; i < getColumnsCount(); i++) {
			if (grid[i][getRowsCount() - 1].isObstacle()) {
				isContainsObstacle = true;
				break;
			}
		}

		if (isContainsObstacle) {
			setRowsCount(getRowsCount() + 1);
			for (int i = 0; i < getColumnsCount(); i++) {
				grid[i][getRowsCount() - 1].setY(grid[i][getRowsCount() - 2]
						.getY() + grid[i][getRowsCount() - 2].getHeight());
				grid[i][getRowsCount() - 1].setHeight(y
						- grid[i][getRowsCount() - 2].getY());
				grid[i][getRowsCount() - 1].setX(grid[i][getRowsCount() - 2]
						.getX());
				grid[i][getRowsCount() - 1]
						.setWidth(grid[i][getRowsCount() - 2].getWidth());
			}
		} else {
			for (int i = 0; i < getColumnsCount(); i++) {
				grid[i][getRowsCount() - 1].setHeight(y
						- grid[i][getRowsCount() - 1].getY());
			}
		}
	}

	/**
	 * 
	 * Срезает слева сетку до указанных границ по x
	 * 
	 */
	protected void shrinkLeft(int x) {
		divideColumnsByX(x);
		int columnNumber = getColumnNumberContainsX(x);
		if (columnNumber != UNKNOWN) {
			moveColumnsLeft(columnNumber, columnNumber);
			setColumnsCount(getColumnsCount() - columnNumber);
		}
	}

	protected void shrinkTop(int y) {
		divideRowsByY(y);
		int rowNumber = getRowNumberContainsY(y);
		if (rowNumber != UNKNOWN) {
			moveRowsUp(rowNumber, rowNumber);
			setRowsCount(getRowsCount() - rowNumber);
		}
	}

	/**
	 * 
	 * Расширяет сетку слева до указанных границ по x
	 * 
	 */
	protected void expandTop(int y) {
		boolean isContainsObstacle = false;
		for (int i = 0; i < getColumnsCount(); i++) {
			if (grid[i][0].isObstacle()) {
				isContainsObstacle = true;
				break;
			}
		}
		if (isContainsObstacle) {
			setRowsCount(getRowsCount() + 1);
			moveRowsDown(0, 1);
			for (int i = 0; i < getColumnsCount(); i++) {
				grid[i][0].setY(y);
				grid[i][0].setHeight(grid[i][1].getY() - y);
				grid[i][0].setX(grid[i][1].getX());
				grid[i][0].setWidth(grid[i][1].getWidth());
			}
		} else {
			for (int i = 0; i < getColumnsCount(); i++) {
				grid[i][0].setHeight(grid[i][0].getY() - y
						+ grid[i][0].getHeight());
				grid[i][0].setY(y);
			}
		}
		// TODO: notify about expansion
	}

	/**
	 * 
	 * Расширяет сетку слева до указанных границ по x
	 * 
	 */
	protected void expandLeft(int x) {
		boolean isContainsObstacle = false;
		for (int i = 0; i < getRowsCount(); i++) {
			if (grid[0][i].isObstacle()) {
				isContainsObstacle = true;
				break;
			}
		}
		if (isContainsObstacle) {
			setColumnsCount(getColumnsCount() + 1);
			moveColumnsRight(0, 1);
			for (int i = 0; i < getRowsCount(); i++) {
				grid[0][i].setX(x);
				grid[0][i].setWidth(grid[1][i].getX() - x);
				grid[0][i].setY(grid[1][i].getY());
				grid[0][i].setHeight(grid[1][i].getHeight());
			}
		} else {
			for (int i = 0; i < getRowsCount(); i++) {
				grid[0][i].setWidth(grid[0][i].getX() - x
						+ grid[0][i].getWidth());
				grid[0][i].setX(x);
			}
		}
		// TODO: notify about expansion
	}

	protected List<ICell> getCellsInRegion(IRectangle bounds) {
		int startColumn = getColumnNumberContainsX(bounds.getX());
		int startRow = getRowNumberContainsY(bounds.getY());
		int endColumn = getColumnNumberContainsX(bounds.getX()
				+ bounds.getWidth());
		int endRow = getRowNumberContainsY(bounds.getY() + bounds.getHeight());
		List<ICell> cells = new ArrayList<ICell>();
		if(endColumn < 0 )
			endColumn = startColumn + 1;
		if(endRow < 0 )
			endRow = startRow + 1;
		for (int i = startColumn; i < endColumn; i++) {
			for (int j = startRow; j < endRow; j++) {
				cells.add(grid[i][j]);
			}
		}
		return cells;
	}

	public ICell[][] getCells() {
		return grid;
	}

	public ICell getCellByPoint(int x, int y) {
		for (int i = 0; i < columnsCount; i++) {
			for (int j = 0; j < rowsCount; j++) {
				ICell cell = grid[i][j];
				int cellX = cell.getX();
				int cellY = cell.getY();
				int cellX2 = cellX + cell.getWidth();
				int cellY2 = cellY + cell.getHeight();
				if (x >= cellX && x < cellX2 && y >= cellY && y < cellY2)
					return cell;
			}
		}
		return null;
	}

	@Override
	public int[] getColumnRow(ICell cell) {
		for (int i = 0; i < columnsCount; i++) {
			for (int j = 0; j < rowsCount; j++) {
				if (grid[i][j] == cell)
					return new int[] { i, j };
			}
		}
		return null;
	}

	@Override
	public AbstractChannelsManager<T, I> getChannelsManager() {
		return channelsManager;
	}

	@Override
	public void setChannelsManager(AbstractChannelsManager<T, I> channelsManager) {
		this.channelsManager = channelsManager;
	}

}
