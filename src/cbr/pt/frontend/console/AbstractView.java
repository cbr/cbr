package cbr.pt.frontend.console;

import java.awt.Color;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cbr.geometry.Dimension;
import cbr.geometry.Geometry;
import cbr.geometry.IDimension;
import cbr.geometry.IPoint;
import cbr.geometry.IRectangle;
import cbr.geometry.ITranslatable;
import cbr.geometry.Point;
import cbr.pt.frontend.CBRApp;
import cbr.pt.frontend.console.editpolicy.AbstractEditPolicy;

public class AbstractView extends CallbackedBounded implements IAreaView {

	private boolean isVisible = true;

	private Color color = new Color(90, 90, 90);

	private List<IAreaView> childrens;

	private IAreaView parent;

	private CBRContext context;

	private int order = 0;

	private IDimension minimumSize = new Dimension(40, 40);

	private List<IAreaView> changeBoundsListeners = new ArrayList<IAreaView>();

	private Map<String, AbstractEditPolicy> editPolicies = new HashMap<String, AbstractEditPolicy>();

	public AbstractView() {
		childrens = new ArrayList<IAreaView>();
	}

	public Color getColor() {
		return color;
	}

	public void setColor(Color color) {
		this.color = color;
	}

	@Override
	public boolean addChild(IAreaView child) {
		boolean isAdded = addChild(-1, child);
		if (isAdded)
			repaint();
		return isAdded;
	}

	@Override
	public boolean removeChild(IAreaView child) {
		if (childrens.contains(child)) {
			boolean isRemoved = childrens.remove(child);
			if (isRemoved) {
				child.dispose();
				CBRContext context = getContext();
				if (context != null) {
					context.fireChildRemoved(child, this);
				}
				repaint();
			}
			return isRemoved;
		}
		return false;
	}

	@Override
	public void paint(CBRGraphics graphics) {
		if (!isVisible())
			return;
		graphics.setColor(getColor());
		prePaint(graphics);
		if (!getChildrens().isEmpty()) {
			if (useLocalCoordinates()) {
				graphics.translateTo(getLocation());
			}
			paintChildrens(graphics);
			if (useLocalCoordinates()) {
				graphics.translateFrom(getLocation());
			}
		}
		graphics.setColor(getColor());
		postPaint(graphics);
	}

	public void paintChildrens(CBRGraphics g) {
		for (IAreaView view : getChildrens()) {
			view.paint(g);
		}
	}

	public void prePaint(CBRGraphics g) {

	}

	public void postPaint(CBRGraphics g) {

	}

	@Override
	public List<IAreaView> getChildrens() {
		return childrens;
	}

	@Override
	public void translateTo(ITranslatable translatable) {
		if (useLocalCoordinates())
			getBounds().translateTo(translatable);
	}

	@Override
	public void translateFrom(ITranslatable translatable) {
		if (useLocalCoordinates())
			getBounds().translateFrom(translatable);
	}

	@Override
	public boolean isInBoundsOverboarded(IPoint point,
			List<Class<? extends IAreaView>> overboardList) {
		if (isVisible()) {
			if (Geometry.isContains(point.getX(), point.getY(), getLocation()
					.getX(), getLocation().getY(), getSize().getWidth(),
					getSize().getHeight()))
				return true;
			translateFrom(point);
			for (IAreaView areaView : getChildrens()) {
				for (Class<? extends IAreaView> overboardClass : overboardList) {
					if (overboardClass.isAssignableFrom(areaView.getClass())
							&& areaView.isInBoundsOverboarded(point,
									overboardList)) {
						translateTo(point);
						return true;
					}
				}
			}
			translateTo(point);
		}
		return false;
	}

	@Override
	public boolean isInBounds(IPoint point) {
		return isVisible()
				&& Geometry.isContains(point.getX(), point.getY(),
						getLocation().getX(), getLocation().getY(), getSize()
								.getWidth(), getSize().getHeight());
	}

	@Override
	public IAreaView getContainsPointView(IPoint point,
			List<Class<? extends IAreaView>> opaqueList,
			List<Class<? extends IAreaView>> overboardList) {
		if (opaqueList.contains(getClass()))
			return null;
		translateFrom(point);
		List<IAreaView> childrens = getChildrens();
		IAreaView containsView = null;
		for (int i = 0; i < childrens.size(); i++) {
			IAreaView curView = childrens.get(i);
			if (containsView == null
					&& curView.isInBoundsOverboarded(point, overboardList)) {
				containsView = curView;
			} else if (containsView != null
					&& curView.getOrder() > containsView.getOrder()
					&& curView.isInBoundsOverboarded(point, overboardList)) {
				containsView = curView;
			}
		}
		if (containsView != null) {
			containsView = containsView.getContainsPointView(point, opaqueList,
					overboardList);
		}
		translateTo(point);
		return containsView == null ? this : containsView;
	}

	@Override
	public boolean useLocalCoordinates() {
		return true;
	}

	@Override
	public void setParent(IAreaView parent) {
		this.parent = parent;
	}

	@Override
	public IAreaView getParent() {
		return parent;
	}

	@Override
	public void repaint() {
		if (getContext() != null)
			getContext().repaint();
	}

	@Override
	public boolean isVisible() {
		return isVisible;
	}

	@Override
	public void setVisible(boolean isVisible) {
		this.isVisible = isVisible;
	}

	@Override
	public CBRContext getContext() {
		return context;
	}

	@Override
	public void dispose() {
		super.dispose();
		for (IAreaView view : new ArrayList<IAreaView>(getChildrens())) {
			removeChild(view);
		}
		context = null;
		parent = null;
	}

	@Override
	public void setContext(CBRContext context) {
		this.context = context;
		for (IAreaView view : getChildrens()) {
			view.setContext(context);
		}
		for (AbstractEditPolicy editPolicy : editPolicies.values()) {
			editPolicy.setContext(context);
		}
	}

	@Override
	public void translateToAbsolute(ITranslatable translatable) {
		if (getParent() != null)
			getParent().translateToAbsolute(translatable);
		translateTo(translatable);
	}

	@Override
	public void translateToRelative(ITranslatable translatable) {
		if (getParent() != null)
			getParent().translateToRelative(translatable);
		translateFrom(translatable);
	}

	@Override
	public IPoint getCenter() {
		IPoint location = getLocation().copy();
		translateToCenter(location);
		return location;
	}

	@Override
	public void setCenter(IPoint point) {
		IPoint location = point.copy();
		translateFromCenter(location);
		setLocation(location);
	}

	@Override
	public IPoint getCenterVector() {
		return new Point(getSize().getWidth() / 2, getSize().getHeight() / 2);
	}

	@Override
	public void translateToCenter(ITranslatable translatable) {
		getCenterVector().translateTo(translatable);
	}

	@Override
	public void translateFromCenter(ITranslatable translatable) {
		getCenterVector().translateFrom(translatable);
	}

	@Override
	public IAreaView getRoot() {
		return getParent() == null ? this : getParent().getRoot();
	}

	protected IIdentifiedView createUnconnectedAreaView(int x, int y,
			int width, int height) {
		return getContext().getFactory().createAreaViewWithId(x, y, width,
				height);
	}

	protected IFeatureView createUnconnectedFeatureView(int x, int y,
			int width, int height) {
		return getContext().getFactory().createFeatureViewWithId(x, y, width,
				height);
	}

	protected IWireView createUnconnectedWireView(int sourceId, int targetId) {
		return getContext().getFactory().createWireViewWithId(sourceId,
				targetId);
	}

	protected IIdentifiedView createChildAreaView(int x, int y, int width,
			int height) {
		IIdentifiedView view = createUnconnectedAreaView(x, y, width, height);
		addChild(view);
		return view;
	}

	protected IFeatureView createChildFeatureView(int x, int y, int width,
			int height) {
		IFeatureView view = createUnconnectedFeatureView(x, y, width, height);
		addChild(view);
		return view;
	}

	protected IWireView createChildWireView(int sourceId, int targetId) {
		IWireView view = createUnconnectedWireView(sourceId, targetId);
		addChild(view);
		return view;
	}

	protected int createChildAreaViewId(int x, int y, int width, int height) {
		return createChildAreaView(x, y, width, height).getId();
	}

	protected int createChildFeatureViewId(int x, int y, int width, int height) {
		return createChildFeatureView(x, y, width, height).getId();
	}

	protected int createChildWireViewId(int sourceId, int targetId) {
		return createChildWireView(sourceId, targetId).getId();
	}

	@Override
	public void setChildrens(List<IAreaView> childrensList) {
		this.childrens = childrensList;
		for (IAreaView view : childrens) {
			view.setParent(this);
			view.setContext(getContext());
		}
	}

	protected boolean addChild(int index, IAreaView child) {
		if (childrens.contains(child)) {
			return false;
		}
		if (index == -1) {
			childrens.add(child);
		} else {
			childrens.add(index, child);
		}
		child.setParent(this);
		child.setContext(getContext());
		CBRApp.logger().info(
				"Add model " + child.getClass().getSimpleName()
						+ " as child to " + getClass().getSimpleName());
		child.recursiveByAddChildAction();
		CBRContext context = getContext();
		if (context != null) {
			context.fireChildAdded(child, this);
		}
		return true;
	}

	@Override
	public void recursiveByAddChildAction() {
		for (IAreaView child : childrens) {
			child.recursiveAfterAddRootAction();
		}
	}

	@Override
	public void recursiveAfterAddRootAction() {
	}

	@Override
	public int getOrder() {
		return order;
	}

	@Override
	public void setOrder(int order) {
		this.order = order;
	}

	@Override
	protected void boundsChanged() {
		for (IAreaView view : changeBoundsListeners) {
			IRectangle bounds = getBounds().copy();
			IAreaView parent = getParent();
			if (parent != null)
				parent.translateToAbsolute(bounds);
			IAreaView viewParent = view.getParent();
			if (viewParent != null)
				viewParent.translateToRelative(bounds);
			view.setBounds(bounds);
		}
		repaint();
	}

	@Override
	public IDimension getMinimumSize() {
		return minimumSize;
	}

	@Override
	public void addEditPolicy(AbstractEditPolicy editPolicy) {
		if (!editPolicies.containsKey(editPolicy.getName())) {
			editPolicy.setContext(getContext());
			editPolicy.setHost(this);
			editPolicies.put(editPolicy.getName(), editPolicy);
		}
	}

	@Override
	public void removeEditPolicy(AbstractEditPolicy editPolicy) {
		editPolicies.remove(editPolicy.getName());
		editPolicy.setContext(null);
		editPolicy.setHost(null);
	}

	@Override
	public AbstractEditPolicy getEditPolicy(String editPolicyName) {
		return editPolicies.get(editPolicyName);
	}

	@Override
	public void removeChangeBoundsListener(IAreaView selectionFeedback) {
		changeBoundsListeners.remove(selectionFeedback);
	}

	@Override
	public void addChangeBoundsListener(IAreaView selectionFeedback) {
		if (!changeBoundsListeners.contains(selectionFeedback)) {
			changeBoundsListeners.add(selectionFeedback);
			for (IAreaView view : changeBoundsListeners) {
				IRectangle bounds = getBounds().copy();
				IAreaView parent = getParent();
				if (parent != null)
					parent.translateToAbsolute(bounds);
				IAreaView viewParent = view.getParent();
				if (viewParent != null)
					viewParent.translateToRelative(bounds);
				view.setBounds(bounds);
			}
		}
	}

	@Override
	public IAreaView findModelById(int id) {
		for (IAreaView child : getChildrens()) {
			IAreaView model = child.findModelById(id);
			if (model != null)
				return model;
		}
		return null;
	}

}
