package cbr.pt.frontend.console.views.editpolicy;

import cbr.pt.frontend.console.editpolicy.WireCreationEditPolicy;
import cbr.pt.frontend.console.views.Connection;

public class ConnectionCreationEditPolicy extends WireCreationEditPolicy {

	public ConnectionCreationEditPolicy() {
		addSupportedClasses(Connection.class);
	}

}
