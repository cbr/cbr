package cbr.pt.frontend.console.views;

import cbr.pt.frontend.console.IWireView;
import cbr.serialize.xml.annotations.XMLNode;

@XMLNode("connection")
public interface IConnection extends IWireView {

}
