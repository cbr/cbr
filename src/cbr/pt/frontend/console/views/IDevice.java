package cbr.pt.frontend.console.views;

import cbr.pt.frontend.console.IIdentifiedAnchorView;
import cbr.serialize.xml.annotations.XMLNode;

@XMLNode("device")
public interface IDevice extends IIdentifiedAnchorView {

}
