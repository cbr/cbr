package cbr.pt.frontend.console.service;

import java.awt.Color;

import cbr.pt.frontend.console.AbstractView;
import cbr.pt.frontend.console.CBRGraphics;

public class AreaHighlightHandles extends AbstractView {

	@Override
	public void prePaint(CBRGraphics graphics) {
		graphics.setAlpha(0.1f);
		graphics.setColor(Color.green);
		graphics.fillRect(getBounds());
		graphics.setAlpha(1f);
	}
}
