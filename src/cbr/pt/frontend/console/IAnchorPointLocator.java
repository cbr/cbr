package cbr.pt.frontend.console;

public interface IAnchorPointLocator {

	public int locate(IConnector connector);

}
