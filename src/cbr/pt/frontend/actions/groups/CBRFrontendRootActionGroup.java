package cbr.pt.frontend.actions.groups;

import cbr.pt.frontend.CBRActionGroup;
import cbr.pt.frontend.CBRAppContext;

public class CBRFrontendRootActionGroup extends CBRActionGroup {

	public CBRFrontendRootActionGroup(CBRAppContext appContext) {
		super(appContext, "root");
		addChildActionGroup(new CBRFileActionGroup(appContext));
		addChildActionGroup(new CBREditActionGroup(appContext));
		addChildActionGroup(new CBRModelActionGroup(appContext));
	}

}
