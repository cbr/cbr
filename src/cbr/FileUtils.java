package cbr;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;

public class FileUtils {

	public static String readAllFromTextFile(String path) throws IOException {
		return readStream(new FileInputStream(path));
	}

	public static void writeAllToTextFile(String path, String content)
			throws IOException {
		FileWriter writer = new FileWriter(new File(path), false);
		writer.write(content);
		writer.close();
	}

	public static String readStream(InputStream is) throws IOException {
		StringBuilder sb = new StringBuilder(512);
		Reader r = new InputStreamReader(is, "UTF-8");
		int c = 0;
		while ((c = r.read()) != -1) {
			sb.append((char) c);
		}
		return sb.toString();
	}

}
