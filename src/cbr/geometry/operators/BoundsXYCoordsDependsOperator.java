package cbr.geometry.operators;

import cbr.geometry.IRectangle;

public class BoundsXYCoordsDependsOperator extends
		AbstractXYCoordsDependsOperator implements
		IRectangleCoordsDependsOperator {

	private IRectangle bounds;

	public BoundsXYCoordsDependsOperator(boolean isX, IRectangle bounds) {
		super(isX);
	}

	@Override
	public void setCoord(int coord) {
		if (isX)
			bounds.setX(coord);
		else
			bounds.setY(coord);
	}

	@Override
	public int getCoord() {
		return isX ? bounds.getX() : bounds.getY();
	}

	@Override
	public void setSize(int coord) {
		if (isX)
			bounds.setWidth(coord);
		else
			bounds.setHeight(coord);
	}

	@Override
	public int getSize() {
		return isX ? bounds.getWidth() : bounds.getHeight();
	}

	@Override
	public BoundsXYCoordsDependsOperator change() {
		super.change();
		return this;
	}

}
