package cbr.pt.frontend.console;

import java.awt.Color;

import cbr.Compass;
import cbr.geometry.IDimension;

public class FeatureView extends AreaView implements IFeatureView {

	public static final int ORDER_FEATURE = AreaView.ORDER_AREAVIEW + 1;

	public final static int defaultWidth = 20;

	public final static int defaultHeight = 20;

	private int staticWidth = defaultWidth;

	private int staticHeight = defaultHeight;

	private int side = Compass.UNKNOWN;

	public FeatureView() {
		this(-1);
	}

	@Override
	public IDimension getMinimumSize() {
		return new cbr.geometry.Dimension(defaultWidth, defaultHeight);
	}

	public FeatureView(int id) {
		this(id, 0, 0, defaultWidth, defaultHeight);
	}

	public FeatureView(int id, int x, int y, int width, int height) {
		super(-1, 0, 0, width, height);
		setOrder(ORDER_FEATURE);
		staticWidth = width;
		staticHeight = height;
		getSize().setWidth(staticWidth);
		getSize().setHeight(staticHeight);
		setColor(new Color(0, 0, 90));
	}

	@Override
	public IAnchorPointLocator createLocator() {
		return new CentralPointLocator(this);
	}

	@Override
	public int getSide() {
		return side;
	}

	@Override
	public void setSide(int side) {
		this.side = side;
	}

}
