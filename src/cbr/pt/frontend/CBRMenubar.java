package cbr.pt.frontend;

import java.util.ArrayList;
import java.util.List;

import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

public class CBRMenubar extends JMenuBar implements IAppContextProvider {

	private static final long serialVersionUID = 1L;

	private CBRAppContext appContext;

	public CBRMenubar(CBRAppContext appContext) {
		this.appContext = appContext;
		addRootMenu(appContext.getActionGroup());
	}

	@Override
	public CBRAppContext getAppContext() {
		return appContext;
	}

	@Override
	public void setAppContext(CBRAppContext appContext) {
		this.appContext = appContext;
	}

	private void addRootMenu(CBRActionGroup rootGroup) {
		for (CBRActionGroup group : rootGroup.getChildActionGroups()) {
			JMenu menu = createMenu(group);
			if (menu != null)
				add(menu);
		}
	}

	private List<JMenuItem> createActions(List<CBRAction> actions) {
		List<JMenuItem> menuItems = new ArrayList<JMenuItem>();
		for (CBRAction action : actions) {
			if(action.isCheckAction()) {
				menuItems.add(new JMenuItem(action));
			} else {
				menuItems.add(new JMenuItem(action));
			}
		}
		return menuItems;
	}

	private JMenu createMenu(CBRActionGroup group) {
		List<List<JMenuItem>> groupedItems = new ArrayList<List<JMenuItem>>();
		List<JMenuItem> localMenuItems = createActions(group.getActions());
		if (!localMenuItems.isEmpty()) {
			groupedItems.add(localMenuItems);
		}
		for (CBRActionGroup localGroup : group.getActionGroups()) {
			List<JMenuItem> groupItems = createActions(localGroup.getActions());
			if (!groupItems.isEmpty()) {
				groupedItems.add(groupItems);
			}
		}
		List<JMenu> subMenus = new ArrayList<JMenu>();
		for (CBRActionGroup childGroup : group.getChildActionGroups()) {
			JMenu childMenu = createMenu(childGroup);
			if (childMenu != null) {
				subMenus.add(childMenu);
			}
		}
		if (subMenus.isEmpty() && groupedItems.isEmpty())
			return null;
		JMenu menu = new JMenu(group.getName());
		for (int i = 0; i < groupedItems.size(); i++) {
			if (i != 0)
				menu.addSeparator();
			List<JMenuItem> itemsList = groupedItems.get(i);
			for (JMenuItem item : itemsList) {
				menu.add(item);
			}
		}
		if (!subMenus.isEmpty())
			menu.addSeparator();
		for (int i = 0; i < subMenus.size(); i++) {
			if (i != 0)
				menu.addSeparator();
			menu.add(subMenus.get(i));
		}
		return menu;
	}

}
