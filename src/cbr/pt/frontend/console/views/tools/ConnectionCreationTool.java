package cbr.pt.frontend.console.views.tools;

import cbr.pt.frontend.console.CBRContext;
import cbr.pt.frontend.console.tools.WireCreationTool;
import cbr.pt.frontend.console.views.Connection;
import cbr.pt.frontend.console.views.Port;
import cbr.pt.frontend.console.views.Zone;

public class ConnectionCreationTool extends WireCreationTool {

	public ConnectionCreationTool(CBRContext context) {
		super(context, Connection.class);
		addToOpaqueList(Zone.class);
		addToOpaqueList(Connection.class);
		addToOverboardList(Port.class);
	}

}
