package cbr.pt.frontend.console.tools;

import cbr.pt.frontend.console.CBRContext;
import cbr.pt.frontend.console.editpolicy.HighlightEditPolicy;
import cbr.pt.frontend.console.editpolicy.MovableAndResizableAreaEditPolicy;
import cbr.pt.frontend.console.editpolicy.MovableAreaEditPolicy;
import cbr.pt.frontend.console.editpolicy.SelectionEditPolicy;
import cbr.pt.frontend.console.views.IPort;

public class DefaultEditTool extends ReactivationEditPolicyDragSupportTool {

	public DefaultEditTool(CBRContext context) {
		super(context);
		addSupportedPolicyName(MovableAreaEditPolicy.EDIT_POLICY_MOVE);
		addSupportedPolicyName(MovableAndResizableAreaEditPolicy.EDIT_POLICY_MOVE_RESIZE);
		addSupportedPolicyName(SelectionEditPolicy.EDIT_POLICY_SELECTION);
		addSupportedPolicyName(HighlightEditPolicy.EDIT_POLICY_HIGHLIGHT);
		addToOverboardList(IPort.class);
	}

}
