package cbr.serialize.xml;

import java.util.HashMap;
import java.util.Map;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import cbr.ReflectionUtils;

public class XMLSerializableObject {

	private String name;

	private Map<String, String> attributes;

	private Map<String, XMLSerializableObject> childrens;

	public XMLSerializableObject(String name, Map<String, String> attributes,
			Map<String, XMLSerializableObject> childrens) {
		this.name = name;
		this.attributes = attributes;
		this.childrens = childrens;
	}

	public XMLSerializableObject(String name, Map<String, String> attributes) {
		this(name, attributes, new HashMap<String, XMLSerializableObject>());
	}

	public void serialize(Document document, IXMLSerializationStrategy strategy) {
		serialize(document, strategy, document, null);
	}

	private void serialize(Document document,
			IXMLSerializationStrategy strategy, Node node,
			String parentFieldName) {
		try {
			Integer.parseInt(parentFieldName);
			parentFieldName = null;
		} catch (NumberFormatException e) {
		}
		Element element = strategy.serialize(document, name, parentFieldName,
				attributes);
		Element appendToElement = strategy.getAppendToElement(element, name,
				parentFieldName);
		for (String childFieldName : childrens.keySet()) {
			XMLSerializableObject xsChildObject = childrens.get(childFieldName);
			xsChildObject.serialize(document, strategy, appendToElement,
					childFieldName);
		}
		node.appendChild(element);
	}

	public Object construct(XMLSerializationMapping mapping,
			IXMLSerializationStrategy strategy) throws InstantiationException,
			IllegalAccessException {
		Class<?> clazz = mapping.getClass(name);
		Object newInstance = clazz.newInstance();
		for (String attrName : attributes.keySet()) {
			String attrValueStr = attributes.get(attrName);
			Class<?> retType = ReflectionUtils.getGetMethodReturnType(
					newInstance, attrName);
			Object attrValue = strategy.parseObjectFromString(retType,
					attrValueStr);
			if (attrValue != null) {
				ReflectionUtils.invokeLastSetMethod(newInstance, attrValue,
						attrName);
			}
		}
		for (String fieldName : childrens.keySet()) {
			XMLSerializableObject xsObject = childrens.get(fieldName);
			if (xsObject.name.equals("reference")) {
				Object proxiedObject = ReflectionUtils.invokeLastGetMethod(
						newInstance, fieldName);
				String refField = xsObject.attributes.keySet().iterator()
						.next();
				String refValueStr = xsObject.attributes.get(refField);
				Class<?> type = ReflectionUtils.getGetMethodReturnType(
						proxiedObject, refField);
				Object refValue = strategy.parseObjectFromString(type,
						refValueStr);
				ReflectionUtils.invokeLastSetMethod(proxiedObject, refValue,
						refField);
			} else {
				Object fieldValue = xsObject.construct(mapping, strategy);
				if (fieldValue != null) {
					if (name.equals("list")) {
						ReflectionUtils.invokeListAddMethod(newInstance,
								fieldValue);
					} else {
						ReflectionUtils.invokeLastSetMethod(newInstance,
								fieldValue, fieldName);
					}
				}
			}
		}
		return newInstance;
	}

	public static XMLSerializableObject createXSObject(Node node,
			IXMLSerializationStrategy strategy, XMLSerializationMapping mapping) {
		if (node.getNodeType() == Node.ELEMENT_NODE) {
			NodeList nodeList = node.getChildNodes();
			for (int i = 0; i < nodeList.getLength(); i++) {
				Node childNode = nodeList.item(i);
				if (childNode.getNodeType() == Node.ELEMENT_NODE
						&& childNode.getNodeName().equals("reference")) {
					NamedNodeMap namedNodeMap = childNode.getAttributes();
					for (int j = 0; j < namedNodeMap.getLength(); j++) {
						Node attrNode = namedNodeMap.item(j);
						if (attrNode.getNodeType() == Node.ATTRIBUTE_NODE) {
							Map<String, String> attrs = new HashMap<String, String>(
									1);
							attrs.put(attrNode.getNodeName(),
									attrNode.getNodeValue());
							XMLSerializableObject xsObject = new XMLSerializableObject(
									"reference", attrs);
							return xsObject;
						}
					}
				}
			}

			XMLSerializableObject xsObject = mapping.isNameOfClass(node
					.getNodeName()) ? strategy.createXSObject(node) : null;
			int listCounter = 0;
			for (int i = 0; i < nodeList.getLength(); i++) {
				Node childNode = nodeList.item(i);
				XMLSerializableObject childXSObject = createXSObject(childNode,
						strategy, mapping);
				if (childXSObject == null)
					continue;
				if (xsObject == null) {
					return childXSObject;
				} else {
					String fieldName = node.getNodeName().equals("list") ? ""
							+ listCounter++ : strategy.getFieldName(childNode);
					if (fieldName != null && !fieldName.isEmpty()) {
						xsObject.childrens.put(fieldName, childXSObject);
					}
				}
			}
			return xsObject;
		}
		return null;
	}

	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer(name);
		sb.append("{attributes =");
		for (String attr : attributes.keySet()) {
			sb.append(" ");
			sb.append(attr);
		}
		sb.append("}{childs =");
		for (String child : childrens.keySet()) {
			sb.append(" ");
			sb.append(child);
		}
		sb.append("}");
		return sb.toString();
	}
}
