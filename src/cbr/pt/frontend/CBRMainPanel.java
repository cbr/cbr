package cbr.pt.frontend;

import java.awt.Dimension;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.List;

import javax.swing.BoxLayout;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.xml.sax.SAXException;

import cbr.pt.frontend.actions.CBRModelActionUtils;

public class CBRMainPanel extends JPanel {

	private static final long serialVersionUID = 1L;

	public CBRMainPanel(final CBRAppContext appContext) {
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));

		List<String> innerModels = Config.getInnerModels();
		JList<String> list = new JList<String>(
				innerModels.toArray(new String[innerModels.size()]));
		list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		list.setVisibleRowCount(-1);
		JScrollPane listScroller = new JScrollPane(list);
		listScroller.setPreferredSize(new Dimension(200, 100));
		add(listScroller);
		String autoLoadModelName = appContext.getConfig().autoLoadModel;
		if (autoLoadModelName != null) {
			list.setSelectedValue(autoLoadModelName, true);
		} else {
			list.setSelectedIndex(-1);
		}
		list.addListSelectionListener(new ListSelectionListener() {
			@Override
			public void valueChanged(ListSelectionEvent e) {
				CBRModelActionUtils.disposeModel(appContext);
				Config config = appContext.getConfig();
				try {
					CBRModelActionUtils.loadModel(config,
							Config.getModelPath(config.autoLoadModel));
				} catch (IllegalAccessException e1) {
					e1.printStackTrace();
				} catch (IllegalArgumentException e1) {
					e1.printStackTrace();
				} catch (InvocationTargetException e1) {
					e1.printStackTrace();
				} catch (InstantiationException e1) {
					e1.printStackTrace();
				} catch (ParserConfigurationException e1) {
					e1.printStackTrace();
				} catch (TransformerException e1) {
					e1.printStackTrace();
				} catch (SAXException e1) {
					e1.printStackTrace();
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}
		});

		List<String> innerTests = Config.getInnerTests();
		JList<String> testList = new JList<String>(
				innerTests.toArray(new String[innerTests.size()]));
		testList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		testList.setVisibleRowCount(-1);
		JScrollPane testListScroller = new JScrollPane(testList);
		testListScroller.setPreferredSize(new Dimension(200, 100));
		add(testListScroller);
		testList.setSelectedIndex(-1);
		testList.addListSelectionListener(new ListSelectionListener() {
			@Override
			public void valueChanged(ListSelectionEvent e) {
				Config config = appContext.getConfig();
				CBRModelActionUtils.runTest(appContext,
						Config.getTestPath(config.autoLoadModel));
			}
		});
	}

}