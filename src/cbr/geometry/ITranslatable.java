package cbr.geometry;

public interface ITranslatable {

	public void translateTo(ITranslatable translatable);

	public void translateFrom(ITranslatable translatable);

	public IPoint getLocation();

}
