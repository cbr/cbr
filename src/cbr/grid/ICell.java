package cbr.grid;

import java.util.Set;

import cbr.channels.IChannel;
import cbr.geometry.IRectangle;
import cbr.obstacles.IObstacle;

/**
 * 
 * @author Alexander Strakh
 * 
 */
public interface ICell extends IRectangle {

	public static final int COUNT_OF_SIDES = 4;

	public ISide getSide(int index);

	/**
	 * @return true, если ячейка связана с препятсвием
	 */
	public boolean isObstacle();

	/*public boolean isDirtyToBuildPath();

	public void dirtyToBuildPath();

	public void cleanedAfterBuildPath();*/

	/**
	 * 
	 * @return возвращает препятствия если оно есть в яейке
	 */
	public Set<IObstacle> getObstalces();

	public Set<IChannel> getChannels();

	public void addChannel(IChannel channel);

	public void removeChannel(IChannel channel);

	public void addObstacle(IObstacle obstacle);

	public void removeObstacle(IObstacle obstacle);

	public boolean contains(IObstacle obstacle);

	public ICell[] divideByX(int x);

	public ICell[] divideByY(int y);

	public ICell union(ICell iCell);

	public ISide getWestSide();

	public ISide getNorthSide();

	public ISide getEastSide();

	public ISide getSouthSide();

	public void setWestSide(ISide side);

	public void setNorthSide(ISide side);

	public void setEastSide(ISide side);

	public void setSouthSide(ISide side);

	public ICell getWestCell();

	public ICell getNorthCell();

	public ICell getEastCell();

	public ICell getSouthCell();

	public IGrid getGrid();

	public int getColumn();

	public int getRow();

	public int[] getColumnRow();

	public void setColumn(int column);

	public void setRow(int row);

}
