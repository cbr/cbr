package cbr.pt.frontend.console;

public class IdentifiedAbstractView extends AbstractView implements
		IIdentifiedView {

	private int id;

	public IdentifiedAbstractView(int id) {
		this.id = id;
	}

	@Override
	public int getId() {
		return id;
	}

	@Override
	public void setId(int id) {
		if (id != this.id) {
			this.id = id;
			idChanged();
		}
	}

	protected void idChanged() {
	}

	@Override
	public IAreaView findModelById(int id) {
		if (getId() == id)
			return this;
		return super.findModelById(id);
	}

}
