package cbr.pt.frontend.console;

import java.awt.Color;
import java.awt.Graphics;
import java.util.ArrayList;

import cbr.pt.frontend.Config;

public class CBRConsole extends AbstractView {

	public static final int ORDER_CONSOLE = IAreaView.ORDER_MIN + 1;

	private CBRModel model;

	private FeedbackLayer feedbackLayer;

	private WireLayer wireLayer;

	private ServiceLayer serviceLayer;

	public CBRConsole(Config config) {
		setOrder(ORDER_CONSOLE);
		setContext(config.getContext());
		setColor(Color.black);
		wireLayer = new WireLayer();
		addChild(wireLayer);
		feedbackLayer = new FeedbackLayer();
		addChild(feedbackLayer);
		serviceLayer = new ServiceLayer();
		addChild(serviceLayer);
	}

	public ServiceLayer getServiceLayer() {
		return serviceLayer;
	}

	public FeedbackLayer getFeedbackLayer() {
		return feedbackLayer;
	}

	public WireLayer getWireLayer() {
		return wireLayer;
	}

	public boolean addBeforeLayers(IAreaView view) {
		return addChild(getChildrens().indexOf(wireLayer), view);
	}

	public void setCBRModel(CBRModel model) {
		if (model != null) {
			removeChild(this.model);
		}
		this.model = model;
		addBeforeLayers(model);
		model.setLocation(getLocation().copy());
		model.setSize(getSize().copy());
		model.updateWire();
	}

	public CBRModel getCBRModel() {
		if (model == null) {
			model = getContext().getFactory().createModelView();
			addChild(model);
			model.setLocation(getLocation().copy());
			model.setSize(getSize().copy());
		}
		return model;
	}

	public void paint(Graphics graphics) {
		paint(getContext().getGraphics(graphics));
	}

	@Override
	public void prePaint(CBRGraphics g) {
		g.fillRect(getBounds());
	}

	@Override
	public boolean useLocalCoordinates() {
		return true;
	}

	@Override
	protected void boundsChanged() {
		for (IAreaView view : getChildrens()) {
			view.setLocation(getLocation().copy());
			view.setSize(getSize().copy());
		}
	}

	@Override
	public void dispose() {
		for (IAreaView view : (new ArrayList<IAreaView>(getChildrens()))) {
			if (view instanceof ILayer) {
				((ILayer) view).clear();
			} else {
				removeChild(view);
			}
		}
		model = null;
		getContext().repaint();
	}

	public void switchOffRouter() {
		WireLayer layer = getWireLayer();
		if (layer != null) {
			layer.switchOffRouter();
		}
	}

	public boolean isCBRModelExists() {
		return model != null;
	}

}
