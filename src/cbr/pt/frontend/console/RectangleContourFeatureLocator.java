package cbr.pt.frontend.console;

import cbr.Compass;
import cbr.geometry.IPoint;
import cbr.geometry.IRectangle;

public class RectangleContourFeatureLocator extends AbstractFeatureLocator {

	public RectangleContourFeatureLocator(IAreaView owner) {
		super(owner);
	}

	@Override
	protected int localLocate(IPoint location, int side) {
		IRectangle bounds = getOwner().getBounds();
		if (side == Compass.UNKNOWN) {
			int[] deltas = new int[] { location.getX() - bounds.getX(),
					location.getY() - bounds.getY(),
					bounds.getX() + bounds.getWidth() - location.getX(),
					bounds.getY() + bounds.getHeight() - location.getY() };
			side = Compass.WEST;
			for (int i = 1; i < deltas.length; i++) {
				if (deltas[i] < deltas[side])
					side = i;
			}
		}
		if (side == Compass.WEST) {
			if (location.getY() < bounds.getY()) {
				location.setY(bounds.getY());
			} else if (location.getY() >= bounds.getY() + bounds.getHeight()) {
				location.setY(bounds.getY() + bounds.getHeight() - 1);
			}
			location.setX(bounds.getX());
		} else if (side == Compass.EAST) {
			if (location.getY() < bounds.getY()) {
				location.setY(bounds.getY());
			} else if (location.getY() >= bounds.getY() + bounds.getHeight()) {
				location.setY(bounds.getY() + bounds.getHeight() - 1);
			}
			location.setX(bounds.getX() + bounds.getWidth() - 1);
		} else if (side == Compass.NORTH) {
			if (location.getX() < bounds.getX()) {
				location.setX(bounds.getX());
			} else if (location.getX() >= bounds.getX() + bounds.getWidth()) {
				location.setX(bounds.getX() + bounds.getWidth() - 1);
			}
			location.setY(bounds.getY());
		} else if (side == Compass.SOUTH) {
			if (location.getX() < bounds.getX()) {
				location.setX(bounds.getX());
			} else if (location.getX() >= bounds.getX() + bounds.getWidth()) {
				location.setX(bounds.getX() + bounds.getWidth() - 1);
			}
			location.setY(bounds.getY() + bounds.getHeight() - 1);
		}
		return side;
	}
}
