package cbr.pt.frontend;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;

import javax.swing.JPanel;
import javax.swing.JTextArea;

import cbr.pt.frontend.console.logger.Logger;

public class CBRInfoPanel extends JPanel {

	private static final long serialVersionUID = 1L;

	public static final int logSize = 10;

	private JTextArea textArea;

	private LogMessage[] strings;

	private Logger logger;

	Font font = new Font("Verdana", Font.BOLD, 12);

	public CBRInfoPanel(CBRAppContext appContext) {
		textArea = new JTextArea();
		textArea.setRows(logSize);
		textArea.setEditable(false);
		textArea.setFont(font);
		setLayout(new BorderLayout());
		add(textArea, BorderLayout.CENTER);
		strings = new LogMessage[logSize];
	}

	public void writeMessage(String message, Color color) {
		int index = -1;
		for (int i = 0; i < strings.length; i++) {
			if (strings[i] == null) {
				index = i;
				break;
			}
		}
		if (index == -1) {
			for (int i = 0; i < strings.length - 1; i++) {
				strings[i] = strings[i + 1];
			}
			index = strings.length - 1;
		}
		strings[index] = new LogMessage(message, color);
		textArea.setText(toString());
		repaint();
	}

	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < strings.length; i++) {
			if (strings[i] != null) {
				sb.append(strings[i] + "\n");
			}
		}
		return sb.toString();
	}

	public Logger getLogger() {
		if (logger == null)
			logger = new Logger(this);
		return logger;
	}

}

class LogMessage {

	private String message;

	private Color color;

	public LogMessage(String message, Color color) {
		super();
		this.message = message;
		this.color = color;
	}

	public String getMessage() {
		return message;
	}

	public Color getColor() {
		return color;
	}

	@Override
	public String toString() {
		return message;
	}

}
