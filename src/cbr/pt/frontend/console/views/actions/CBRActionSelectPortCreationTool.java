package cbr.pt.frontend.console.views.actions;

import cbr.pt.frontend.CBRAppContext;
import cbr.pt.frontend.actions.CBRActionCreationTool;
import cbr.pt.frontend.console.views.tools.PortCreationTool;

public class CBRActionSelectPortCreationTool extends CBRActionCreationTool {

	private static final long serialVersionUID = 1L;

	public final static String ACTION_NAME = "Create port";

	public final static String ACTION_TIP = "Create port";

	public final static String ACTION_ICON = "create_port.png";

	public CBRActionSelectPortCreationTool(CBRAppContext appContext) {
		super(appContext, ACTION_NAME, ACTION_TIP, ACTION_ICON,
				new PortCreationTool(appContext.getConfig().getContext()));
	}

}
