package cbr.pt.frontend.command;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import cbr.geometry.IPoint;
import cbr.geometry.IRectangle;
import cbr.geometry.Rectangle;
import cbr.pt.frontend.console.CBRContext;
import cbr.pt.frontend.console.IAreaView;
import cbr.pt.frontend.console.IIdentifiedAnchor;

public class CommandsFactory {

	public static void createAndExecute(String name, CBRContext context,
			String description, Object... args) {
		context.getCommandStack().execute(
				createCommand(name, context, description, args));
	}

	public static ICommand createCommand(String name, CBRContext context,
			String description, Object... args) {
		if (name.equals(SetConstraintCommand.class.getSimpleName())) {
			return new SetConstraintCommand(context, description,
					(IAreaView) args[0], (IRectangle) args[1]);
		} else if (name.equals(SetFeatureConstraintCommand.class
				.getSimpleName())) {
			return new SetFeatureConstraintCommand(context, description,
					(IAreaView) args[0], (IRectangle) args[1], (int) args[2]);
		} else if (name.equals(CreateAreaCommand.class.getSimpleName())) {
			return new CreateAreaCommand(context, description,
					(IAreaView) args[0], (Class<? extends IAreaView>) args[1],
					(IRectangle) args[2]);
		} else if (name.equals(CreateFeatureCommand.class.getSimpleName())) {
			return new CreateFeatureCommand(context, description,
					(IAreaView) args[0], (Class<? extends IAreaView>) args[1],
					(IRectangle) args[2], (int) args[3]);
		} else if (name.equals(CreateWireCommand.class.getSimpleName())) {
			return new CreateWireCommand(context, description,
					(IAreaView) args[0], (Class<? extends IAreaView>) args[1],
					(IIdentifiedAnchor) args[2], (IIdentifiedAnchor) args[3],
					(IPoint) args[4], (IPoint) args[5]);
		} else if (name.equals(DeleteCommand.class.getSimpleName())) {
			return new DeleteCommand(context, description, (IAreaView) args[0]);
		} else if (name.equals(CompoundCommand.class.getSimpleName())) {
			return new CompoundCommand(context, description);
		}
		return null;
	}

	public static ICommand parseCommand(String name, CBRContext context,
			Node commandNode) {
		if (name.equals(SetConstraintCommand.class.getSimpleName())) {
			NodeList nodeList = commandNode.getChildNodes();
			int id = -1;
			Integer x = null;
			Integer y = null;
			Integer width = null;
			Integer height = null;
			for (int i = 0; i < nodeList.getLength(); i++) {
				Node node = nodeList.item(i);
				if (node.getNodeType() == Node.ELEMENT_NODE) {
					if (node.getNodeName().equalsIgnoreCase("id")) {
						id = Integer.parseInt(node.getTextContent());
					} else if (node.getNodeName().equalsIgnoreCase("x")) {
						x = Integer.parseInt(node.getTextContent());
					} else if (node.getNodeName().equalsIgnoreCase("y")) {
						y = Integer.parseInt(node.getTextContent());
					} else if (node.getNodeName().equalsIgnoreCase("width")) {
						width = Integer.parseInt(node.getTextContent());
					} else if (node.getNodeName().equalsIgnoreCase("height")) {
						height = Integer.parseInt(node.getTextContent());
					}
				}
			}
			IAreaView model = context.findModelById(id);
			if (model == null)
				throw new UnsupportedOperationException(
						"Can't find model by id: " + id);
			else
				return createCommand(name, context, "Set constraint", model,
						new Rectangle(x == null ? model.getBounds().getX() : x,
								y == null ? model.getBounds().getY() : y,
								width == null ? model.getBounds().getWidth()
										: width, height == null ? model
										.getBounds().getHeight() : height));
		} else if (name.equals(DeleteCommand.class.getSimpleName())) {
			NodeList nodeList = commandNode.getChildNodes();
			int id = -1;
			for (int i = 0; i < nodeList.getLength(); i++) {
				Node node = nodeList.item(i);
				if (node.getNodeType() == Node.ELEMENT_NODE) {
					if (node.getNodeName().equalsIgnoreCase("id")) {
						id = Integer.parseInt(node.getTextContent());
					}
				}
			}
			IAreaView model = context.findModelById(id);
			if (model == null)
				throw new UnsupportedOperationException(
						"Can't find model by id: " + id);
			else
				return createCommand(name, context, "Delete model", model);
		}
		return null;
	}

}
