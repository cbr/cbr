package cbr.pt.frontend.console.views;

import cbr.pt.frontend.console.AbstractViewFactory;
import cbr.pt.frontend.console.CBRModel;
import cbr.pt.frontend.console.IAreaView;
import cbr.pt.frontend.console.IFeatureView;
import cbr.pt.frontend.console.IIdentifiedView;
import cbr.pt.frontend.console.IWireView;
import cbr.pt.frontend.console.views.serialize.xml.CBRViewSerializer;
import cbr.serialize.xml.XMLSerializer;

public class ViewFactory extends AbstractViewFactory {

	@Override
	public IIdentifiedView createAreaView() {
		return new Device();
	}

	@Override
	public IWireView createWireView() {
		return new Connection();
	}

	@Override
	public IFeatureView createFeatureView() {
		return new Port();
	}

	@Override
	public CBRModel createModelView() {
		return new Model();
	}

	@Override
	public IAreaView createCreationFeedback(Class<? extends IAreaView> clazz) {
		IAreaView feedback = null;
		if (clazz.equals(Device.class)) {
			feedback = createAreaView();
		} else if (clazz.equals(Port.class)) {
			feedback = createFeatureView();
		} else if (clazz.equals(Connection.class)) {
			feedback = createWireView();
		} else if (clazz.equals(Zone.class)) {
			feedback = new Zone();
		}
		return feedback;
	}

	@Override
	public IAreaView create(Class<? extends IAreaView> clazz) {
		IAreaView feedback = null;
		if (clazz.equals(Device.class)) {
			feedback = createAreaViewWithId();
		} else if (clazz.equals(Port.class)) {
			feedback = createFeatureViewWithId();
		} else if (clazz.equals(Connection.class)) {
			feedback = createWireViewWithId();
		} else if (clazz.equals(Zone.class)) {
			feedback = new Zone();
		}
		return feedback;
	}

	@Override
	public XMLSerializer createSerializer() {
		return new CBRViewSerializer();
	}

}
