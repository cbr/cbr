package cbr;

public interface IDisposable {

	public void dispose();

}
