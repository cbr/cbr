package cbr.pt.frontend.command;

import cbr.geometry.IPoint;
import cbr.pt.frontend.console.CBRContext;
import cbr.pt.frontend.console.IAreaView;
import cbr.pt.frontend.console.IConnector;
import cbr.pt.frontend.console.IIdentified;
import cbr.pt.frontend.console.IIdentifiedAnchor;
import cbr.pt.frontend.console.IIdentifiedAnchorView;
import cbr.pt.frontend.console.IWireView;

public class CreateWireCommand extends AbstractCommand {

	private IAreaView parent;

	private IAreaView child;

	private IIdentifiedAnchor sourceAnchor;

	private IIdentifiedAnchor targetAnchor;

	private IPoint sourceLocation;

	private IPoint targetLocation;

	private Class<? extends IAreaView> clazz;

	private int parentId = UNDEFINED;

	private int sourceAnchorId = UNDEFINED;

	private int targetAnchorId = UNDEFINED;

	protected CreateWireCommand(CBRContext context, String description,
			IAreaView parent, Class<? extends IAreaView> clazz,
			IIdentifiedAnchor sourceAnchor, IIdentifiedAnchor targetAnchor,
			IPoint sourceLocation, IPoint targetLocation) {
		super(context, description);
		this.clazz = clazz;
		this.parent = parent;
		this.sourceLocation = sourceLocation;
		this.targetLocation = targetLocation;
		this.targetAnchor = targetAnchor;
		this.sourceAnchor = sourceAnchor;
	}

	protected IWireView getInstance() {
		if (child == null) {
			child = getContext().getFactory().create(clazz);
		}
		return (IWireView) child;
	}

	@Override
	public void execute() {
		IWireView wire = getInstance();
		IConnector sourceConnector = wire.getSourceConnector();
		IConnector targetConnector = wire.getTargetConnector();
		IIdentifiedAnchorView sourceAnchor = (IIdentifiedAnchorView) this.sourceAnchor;
		IIdentifiedAnchorView targetAnchor = (IIdentifiedAnchorView) this.targetAnchor;
		IPoint sourceLocation = this.sourceLocation.copy();
		IPoint targetLocation = this.targetLocation.copy();
		sourceAnchor.translateToRelative(sourceLocation);
		targetAnchor.translateToRelative(targetLocation);
		sourceConnector.setLocation(sourceLocation);
		targetConnector.setLocation(targetLocation);
		sourceConnector.connect(sourceAnchor);
		targetConnector.connect(targetAnchor);
		parent.addChild(wire);
		wire.update();
	}

	@Override
	public void undo() {
		getInstance().getSourceConnector().disconnect();
		getInstance().getTargetConnector().disconnect();
		parent.removeChild(getInstance());
	}

	@Override
	public void dispose() {
		parent = null;
		clazz = null;
		child = null;
		sourceAnchor = null;
		targetAnchor = null;
		sourceLocation = null;
		targetLocation = null;
	}

	@Override
	public void disconnectFromContext() {
		if (parent instanceof IIdentified) {
			parentId = ((IIdentified) parent).getId();
			sourceAnchorId = ((IIdentified) sourceAnchor).getId();
			targetAnchorId = ((IIdentified) targetAnchor).getId();
		} else
			throw new UnsupportedOperationException(
					"Errors due to disconnect from context for CreateWireCommand. Parent: \""
							+ parent.getClass().getSimpleName()
							+ "\" should be IIdentified.");
		super.disconnectFromContext();
	}

	@Override
	public void connectToContext(CBRContext context) {
		super.connectToContext(context);
		if (parentId == UNDEFINED || sourceAnchorId == UNDEFINED
				|| targetAnchorId == UNDEFINED)
			throw new UnsupportedOperationException(
					"Errors due to context connection. Undefined parentId, sourceAnchorId or targetAnchorId id for CreateWireCommand.");
		else {
			parent = findModelById(parentId);
			if (parent != null)
				parentId = UNDEFINED;
			sourceAnchor = (IIdentifiedAnchor) findModelById(targetAnchorId);
			if (sourceAnchor != null)
				sourceAnchorId = UNDEFINED;
			targetAnchor = (IIdentifiedAnchor) findModelById(sourceAnchorId);
			if (targetAnchor != null)
				targetAnchorId = UNDEFINED;
		}
	}

}
