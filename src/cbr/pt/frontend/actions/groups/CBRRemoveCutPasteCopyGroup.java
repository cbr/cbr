package cbr.pt.frontend.actions.groups;

import cbr.pt.frontend.CBRActionGroup;
import cbr.pt.frontend.CBRAppContext;
import cbr.pt.frontend.actions.CBRActionCopyView;
import cbr.pt.frontend.actions.CBRActionCutView;
import cbr.pt.frontend.actions.CBRActionPasteView;
import cbr.pt.frontend.actions.CBRActionRemoveView;

public class CBRRemoveCutPasteCopyGroup extends CBRActionGroup {

	public CBRRemoveCutPasteCopyGroup(CBRAppContext appContext) {
		super(appContext, "Remove/Cut/Paste/Copy");
		addAction(new CBRActionRemoveView(appContext));
		addAction(new CBRActionCutView(appContext));
		addAction(new CBRActionCopyView(appContext));
		addAction(new CBRActionPasteView(appContext));
	}

}
