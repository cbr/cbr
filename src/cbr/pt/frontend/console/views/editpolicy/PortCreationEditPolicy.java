package cbr.pt.frontend.console.views.editpolicy;

import cbr.pt.frontend.console.editpolicy.FeatureCreationEditPolicy;
import cbr.pt.frontend.console.views.Port;

public class PortCreationEditPolicy extends FeatureCreationEditPolicy {

	public PortCreationEditPolicy() {
		addSupportedClasses(Port.class);
	}

}
