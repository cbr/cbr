package cbr.pt.frontend.console.views;

import cbr.pt.frontend.console.WireView;
import cbr.pt.frontend.console.editpolicy.HighlightEditPolicy;
import cbr.pt.frontend.console.views.editpolicy.ModelSpecificExcludingSelectionEditPolicy;

public class Connection extends WireView implements IConnection {

	public Connection() {
		addEditPolicy(new HighlightEditPolicy());
		addEditPolicy(new ModelSpecificExcludingSelectionEditPolicy());
	}

}
