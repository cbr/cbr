package cbr.pt.frontend.console;

import cbr.geometry.IPoint;

public interface IFeatureLocator {

	public int locate(IPoint location, int side);

}
