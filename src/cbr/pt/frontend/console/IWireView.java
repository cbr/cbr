package cbr.pt.frontend.console;

import java.util.List;

import cbr.geometry.IPoint;
import cbr.serialize.xml.annotations.XMLElement;
import cbr.serialize.xml.annotations.XMLNode;
import cbr.serialize.xml.annotations.XMLTransient;

@XMLNode("wireView")
@XMLTransient("bounds")
public interface IWireView extends IIdentifiedView {

	@XMLElement
	public IConnector getSourceConnector();

	@XMLElement
	public IConnector getTargetConnector();

	public void setSourceConnector(IConnector sourceConnector);

	public void setTargetConnector(IConnector targetConnector);

	@XMLElement
	public List<IPoint> getPath();

	public void update();

	public void setSourceId(int id);

	public void setTargetId(int id);

}
