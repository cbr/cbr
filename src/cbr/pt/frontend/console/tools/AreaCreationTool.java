package cbr.pt.frontend.console.tools;

import cbr.geometry.IPoint;
import cbr.pt.frontend.console.CBRContext;
import cbr.pt.frontend.console.IAreaView;
import cbr.pt.frontend.console.editpolicy.AbstractEditPolicy;
import cbr.pt.frontend.console.editpolicy.AreaCreationEditPolicy;

public class AreaCreationTool extends ReactivationEditPolicyDragSupportTool {

	private Class<? extends IAreaView> clazz;

	public AreaCreationTool(CBRContext context, Class<? extends IAreaView> clazz) {
		super(context);
		this.clazz = clazz;
		addSupportedPolicyName(AreaCreationEditPolicy.EDIT_POLICY_AREA_CREATION);
	}

	@Override
	protected boolean isSupported(AbstractEditPolicy editPolicy) {
		return editPolicy instanceof AreaCreationEditPolicy
				&& ((AreaCreationEditPolicy) editPolicy).isSupported(clazz);
	}

	@Override
	protected void finishDrag(AbstractEditPolicy policy, IPoint location) {
		if (policy instanceof AreaCreationEditPolicy)
			((AreaCreationEditPolicy) policy).setCurrentClass(clazz);
		super.finishDrag(policy, location);
	}

}
