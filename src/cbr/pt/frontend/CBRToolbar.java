package cbr.pt.frontend;

import java.util.List;

import javax.swing.JToolBar;

public class CBRToolbar extends JToolBar implements IAppContextProvider {

	private static final long serialVersionUID = 1L;

	private CBRAppContext appContext;

	public CBRToolbar(CBRAppContext appContext) {
		this.appContext = appContext;
		addActions(appContext.getActionGroup());
	}

	@Override
	public CBRAppContext getAppContext() {
		return appContext;
	}

	@Override
	public void setAppContext(CBRAppContext appContext) {
		this.appContext = appContext;
	}

	private void addActions(CBRActionGroup actionGroup) {
		boolean actionsAdded = false;
		for (CBRAction action : actionGroup.getActions()) {
			if (action.isCheckAction()) {
				add(new CBRToggleButton(action, action.getTip()));
			} else {
				add(new CBRButton(action, action.getTip()));
			}
			if (!actionsAdded)
				actionsAdded = true;
		}
		List<CBRActionGroup> groups = actionGroup.getActionGroups();
		for (int i = 0; i < groups.size(); i++) {
			CBRActionGroup childActionGroup = groups.get(i);
			if (!childActionGroup.isEmpty()) {
				if (i != 0)
					addSeparator();
				addActions(childActionGroup);
				if (!actionsAdded)
					actionsAdded = true;
			}
		}
		List<CBRActionGroup> childGroups = actionGroup.getChildActionGroups();
		for (int i = 0; i < childGroups.size(); i++) {
			CBRActionGroup childActionGroup = childGroups.get(i);
			if (!childActionGroup.isEmpty()) {
				if (i != 0)
					addSeparator();
				addActions(childActionGroup);
			}
		}
	}

}
