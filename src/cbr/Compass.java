package cbr;

public class Compass {

	public static final int NORMAL_DIRECTIONS_COUNT = 4;

	public static final int ZERO = 0;

	public static final int WEST = ZERO;
	public static final int NORTH = WEST + 1;
	public static final int EAST = NORTH + 1;
	public static final int SOUTH = EAST + 1;

	public static final int WEST_NORTH = WEST + NORMAL_DIRECTIONS_COUNT;
	public static final int NORTH_EAST = WEST_NORTH + 1;
	public static final int EAST_SOUTH = NORTH_EAST + 1;
	public static final int SOUTH_WEST = EAST_SOUTH + 1;

	public static final int UNKNOWN = -1;

	public static final String STR_WEST = "west";
	public static final String STR_NORTH = "north";
	public static final String STR_EAST = "east";
	public static final String STR_SOUTH = "south";
	public static final String STR_UNKNOWN = "unknown";

	public static String side(int side) {
		if (side == WEST)
			return STR_WEST;
		if (side == NORTH)
			return STR_NORTH;
		if (side == EAST)
			return STR_EAST;
		if (side == SOUTH)
			return STR_SOUTH;
		return STR_UNKNOWN;
	}

	public static boolean isDeterminedSide(int index) {
		return !side(index).equals(STR_UNKNOWN);
	}

	public static int side(String side) {
		if (side.equalsIgnoreCase(STR_WEST))
			return WEST;
		if (side.equalsIgnoreCase(STR_NORTH))
			return NORTH;
		if (side.equalsIgnoreCase(STR_EAST))
			return EAST;
		if (side.equalsIgnoreCase(STR_SOUTH))
			return SOUTH;
		return UNKNOWN;
	}

}
