package cbr.pt.frontend.console;

import cbr.geometry.Bounded;
import cbr.geometry.IDimension;
import cbr.geometry.IPoint;
import cbr.geometry.IRectangle;

public class CallbackedBounded extends Bounded {

	@Override
	public void setBounds(IRectangle bounds) {
		if (!bounds.equals(getBounds())) {
			super.setBounds(bounds);
			boundsChanged();
		}
	}

	@Override
	public void setSize(IDimension size) {
		if (!size.equals(getSize())) {
			super.setSize(size);
			boundsChanged();
		}
	}

	@Override
	public void setLocation(IPoint location) {
		if (!location.equals(getLocation())) {
			super.setLocation(location);
			boundsChanged();
		}
	}

	protected void boundsChanged() {

	}

}
