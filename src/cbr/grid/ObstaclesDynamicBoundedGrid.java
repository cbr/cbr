package cbr.grid;

import java.util.List;

import cbr.geometry.IDimension;
import cbr.geometry.IPoint;
import cbr.geometry.IRectangle;
import cbr.geometry.Rectangle;
import cbr.obstacles.IObstacle;

public class ObstaclesDynamicBoundedGrid<T, I> extends BoundedGrid<T, I>
		implements IObstaclesBoundedGrid<T, I> {

	public ObstaclesDynamicBoundedGrid(int x, int y, int width, int height) {
		super(x, y, width, height);
	}

	public ObstaclesDynamicBoundedGrid() {
		this(0, 0, 0, 0);
		updateSides();
	}

	public ObstaclesDynamicBoundedGrid(Rectangle bounds) {
		this(bounds.getLocation(), bounds.getSize());
	}

	public ObstaclesDynamicBoundedGrid(IPoint location, IDimension size) {
		this(location.getX(), location.getY(), size.getWidth(), size
				.getHeight());
	}

	@Override
	public boolean addObstacle(IObstacle obstacle) {
		int xLeft = obstacle.getX();
		int yTop = obstacle.getY();
		int xRight = xLeft + obstacle.getWidth();
		int yBottom = yTop + obstacle.getHeight();
		boolean isContainsLeftTop = isContains(xLeft, yTop);
		boolean isContainsRightTop = isContains(xRight, yTop);
		boolean isContainsLeftBottom = isContains(xLeft, yBottom);
		boolean isContainsRightBottom = isContains(xRight, yBottom);
		if (isContainsLeftTop && isContainsLeftBottom && isContainsRightTop
				&& isContainsRightBottom) {
			if (divideColumnsByX(xLeft) | divideRowsByY(yTop)
					| divideColumnsByX(xRight) | divideRowsByY(yBottom)) {
				updateSides();
			}
		} else if (isContainsLeftBottom && isContainsRightBottom) {
			if (divideColumnsByX(xLeft) | divideColumnsByX(xRight)
					| divideRowsByY(yBottom)) {
				updateSides();
			}
		} else if (isContainsRightTop && isContainsRightBottom) {
			if (divideRowsByY(yTop) | divideColumnsByX(xRight)
					| divideRowsByY(yBottom)) {
				updateSides();
			}
		} else if (isContainsLeftTop && isContainsRightTop) {
			if (divideColumnsByX(xLeft) | divideRowsByY(yTop)
					| divideColumnsByX(xRight)) {
				updateSides();
			}
		} else if (isContainsLeftTop && isContainsLeftBottom) {
			if (divideColumnsByX(xLeft) | divideRowsByY(yTop)
					| divideRowsByY(yBottom)) {
				updateSides();
			}
		} else if (isContainsRightBottom) {
			if (divideColumnsByX(xRight) | divideRowsByY(yBottom)) {
				updateSides();
			}
		} else if (isContainsLeftBottom) {
			if (divideColumnsByX(xLeft) | divideRowsByY(yBottom)) {
				updateSides();
			}
		} else if (isContainsLeftTop) {
			if (divideColumnsByX(xLeft) | divideRowsByY(yTop)) {
				updateSides();
			}
		} else if (isContainsRightTop) {
			if (divideRowsByY(yTop) | divideColumnsByX(xRight)) {
				updateSides();
			}
		} else {
			int gridLeftX = getX();
			int gridRightX = gridLeftX + getWidth();
			int gridTopY = getY();
			int gridBottomY = gridTopY + getHeight();
			if (xLeft > gridLeftX && xLeft < gridRightX && xRight > gridLeftX
					&& xRight < gridRightX && yTop < gridTopY
					&& yBottom >= gridBottomY) {
				if (divideColumnsByX(xLeft) | divideColumnsByX(xRight)) {
					updateSides();
				}
			} else if (yTop > gridTopY && yTop < gridBottomY
					&& yBottom > gridTopY && yTop < gridBottomY
					&& xLeft < gridLeftX && xRight >= gridRightX) {
				if (divideRowsByY(yTop) | divideRowsByY(yBottom)) {
					updateSides();
				}
			} else if (xLeft <= gridLeftX && xRight > gridLeftX
					&& xRight < gridRightX && yTop < gridTopY
					&& yBottom >= gridBottomY) {
				if (divideColumnsByX(xRight)) {
					updateSides();
				}
			} else if (yTop <= gridTopY && yBottom > gridTopY
					&& yBottom < gridBottomY && xLeft < gridLeftX
					&& xRight >= gridRightX) {
				if (divideRowsByY(yBottom)) {
					updateSides();
				}
			} else if (xRight <= gridRightX && xLeft > gridLeftX
					&& xLeft < gridRightX && yTop < gridTopY
					&& yBottom >= gridBottomY) {
				if (divideColumnsByX(xLeft)) {
					updateSides();
				}
			} else if (yBottom <= gridBottomY && yTop > gridTopY
					&& yTop < gridBottomY && xLeft < gridLeftX
					&& xRight >= gridRightX) {
				if (divideRowsByY(yTop)) {
					updateSides();
				}
			}
		}
		List<ICell> cells = getCellsInRegion(obstacle);
		for (ICell cell : cells) {
			cell.addObstacle(obstacle);
		}
		return !cells.isEmpty();
	}

	/**
	 * 
	 * Если ячейки содержат два и более препятствия, то объединнеи не требуется
	 * 
	 */
	private boolean isNeedUnionColumns(int column1, int column2) {
		ICell[][] cells = getCells();
		for (int i = 0; i < getRowsCount(); i++) {
			if (cells[column1][i].isObstacle()
					|| cells[column2][i].isObstacle()) {
				for (IObstacle obstacle : cells[column1][i].getObstalces()) {
					if (!cells[column2][i].contains(obstacle)) {
						return false;
					}
				}
			}
		}
		return true;
	}

	private boolean isNeedUnionRows(int row1, int row2) {
		ICell[][] cells = getCells();
		for (int i = 0; i < getColumnsCount(); i++) {
			if (cells[i][row1].isObstacle() || cells[i][row2].isObstacle()) {
				for (IObstacle obstacle : cells[i][row1].getObstalces()) {
					if (!cells[i][row2].contains(obstacle)) {
						return false;
					}
				}
			}
		}
		return true;
	}

	private void unionByX(int x) {
		int columnNumber = getColumnNumberContainsX(x);
		if (columnNumber > 0) {
			if (isNeedUnionColumns(columnNumber, columnNumber - 1)) {
				unionColumns(columnNumber, columnNumber - 1);
			}
		}
	}

	private void unionByY(int y) {
		int rowNumber = getRowNumberContainsY(y);
		if (rowNumber > 0) {
			if (isNeedUnionRows(rowNumber, rowNumber - 1)) {
				unionRows(rowNumber, rowNumber - 1);
			}
		}
	}

	@Override
	public boolean removeObstacle(IObstacle obstacle) {
		List<ICell> cells = getCellsInRegion(obstacle);
		if (cells.size() > 0) {
			for (ICell cell : cells) {
				cell.removeObstacle(obstacle);
			}
			int xLeft = obstacle.getX();
			unionByX(xLeft);
			unionByX(xLeft + obstacle.getWidth());
			int yTop = obstacle.getY();
			unionByY(yTop);
			unionByY(yTop + obstacle.getHeight());
			updateSides();
			return true;
		}
		return false;
	}

	/*
	 * TODO: Вместо удаления и добавления реализовать: 1) Объединение только в
	 * случае, если ребро вышло за границы 2) Изменение размеров ячеек на
	 * границы смещенного ребра => замена ячеек путей и запуск орисовки
	 */
	@Override
	public boolean updateObstacle(IObstacle obstacle, IRectangle updatedBounds) {
		removeObstacle(obstacle);
		obstacle.setX(updatedBounds.getX());
		obstacle.setY(updatedBounds.getY());
		obstacle.setWidth(updatedBounds.getWidth());
		obstacle.setHeight(updatedBounds.getHeight());
		addObstacle(obstacle);
		return true;
	}

}
