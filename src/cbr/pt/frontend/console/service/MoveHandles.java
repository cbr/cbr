package cbr.pt.frontend.console.service;

import java.awt.Color;

import cbr.pt.frontend.console.AbstractView;
import cbr.pt.frontend.console.CBRGraphics;

public class MoveHandles extends AbstractView {

	@Override
	public void prePaint(CBRGraphics graphics) {
		graphics.setColor(Color.red);
		graphics.drawRect(getBounds());
	}

}
