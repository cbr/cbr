package cbr.wires;

import java.util.List;

import cbr.IDisposable;
import cbr.geometry.IPoint;
import cbr.grid.ICell;
import cbr.grid.ISide;

public interface IWire extends IDisposable {

	public ISide getSourceSide();

	public ISide getTargetSide();

	public ICell getSourceCell();

	public ICell getTargetCell();

	public void setSourceSideIndex(int index);

	public void setTargetSideIndex(int index);

	public int getSourceSideIndex();

	public int getTargetSideIndex();

	public void setSourceCell(ICell cell);

	public void setTargetCell(ICell cell);

	public IPoint getSourcePoint();

	public IPoint getTargetPoint();

	public void setSourcePoint(IPoint point);

	public void setTargetPoint(IPoint point);

	public List<IPoint> getPath();

	public void setNeedUpdatePath();

	public void pathUpdated();

	public boolean isNeedUpdatePath();
	
	public boolean isValid();

}
