package cbr.pt.frontend.console.editpolicy;

import java.util.ArrayList;
import java.util.List;

import cbr.geometry.IPoint;
import cbr.geometry.IRectangle;
import cbr.pt.frontend.console.IAreaView;
import cbr.pt.frontend.console.service.AreaSelectionHandles;

public class SelectionEditPolicy extends
		MoveSupportReactivatedCreationEditPolicy {

	public static final String EDIT_POLICY_SELECTION = "Selection edit policy";

	private List<Class<? extends IAreaView>> notSelectedClasses = new ArrayList<Class<? extends IAreaView>>();

	public SelectionEditPolicy() {
		super(EDIT_POLICY_SELECTION);
	}

	public void addNotSelectedClass(Class<? extends IAreaView> clazz) {
		if (!notSelectedClasses.contains(clazz))
			notSelectedClasses.add(clazz);
	}

	public boolean isNotSelectedClass(Class<? extends IAreaView> clazz) {
		for (Class<? extends IAreaView> supClazz : notSelectedClasses) {
			if (supClazz == clazz)
				return true;
		}
		return false;
	}

	@Override
	public void click(IPoint location) {
		if (isNotSelectedClass(getHost().getClass())) {
			getContext().removeSelection();
		} else {
			if (getContext().getSelection() == null) {
				getContext()
						.setSelection(getHost(), new AreaSelectionHandles());
			} else if (getContext().getSelection() != getHost()) {
				getContext().removeSelection();
				getContext()
						.setSelection(getHost(), new AreaSelectionHandles());
			}
		}
	}

	@Override
	protected void updateViewBounds(IAreaView view) {
		super.updateViewBounds(view);
		IAreaView host = getHost();
		IRectangle bounds = host.getBounds().copy();
		IAreaView parent = host.getParent();
		if (parent != null)
			parent.translateToAbsolute(bounds);
		view.setBounds(bounds);
	}
}
