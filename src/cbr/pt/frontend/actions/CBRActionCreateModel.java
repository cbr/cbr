package cbr.pt.frontend.actions;

import java.awt.event.ActionEvent;
import java.lang.reflect.InvocationTargetException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import cbr.pt.frontend.CBRAction;
import cbr.pt.frontend.CBRAppContext;
import cbr.pt.frontend.Config;
import cbr.pt.frontend.console.CBRContext;

public class CBRActionCreateModel extends CBRAction {

	private static final long serialVersionUID = 1L;

	public final static String ACTION_NAME = "Create model";

	public final static String ACTION_TIP = "Create model";

	public final static String ACTION_ICON = "create.png";

	public CBRActionCreateModel(CBRAppContext appContext) {
		super(appContext, ACTION_NAME, ACTION_TIP, ACTION_ICON);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		try {
			if (CBRModelActionUtils.isNeedsToSaveAndTryToSave(getAppContext())) {
				CBRModelActionUtils.disposeModel(getAppContext());
				Config config = getAppContext().getConfig();
				CBRContext context = config.getContext();
				config.getContext().createNewModel();
				context.getCommandStack().markSaveLocation();
			}
		} catch (IllegalAccessException e1) {
			e1.printStackTrace();
		} catch (IllegalArgumentException e1) {
			e1.printStackTrace();
		} catch (InvocationTargetException e1) {
			e1.printStackTrace();
		} catch (ParserConfigurationException e1) {
			e1.printStackTrace();
		} catch (TransformerException e1) {
			e1.printStackTrace();
		}
	}

}
