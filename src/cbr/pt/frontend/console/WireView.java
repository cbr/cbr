package cbr.pt.frontend.console;

import java.awt.Color;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import cbr.geometry.IPoint;
import cbr.pt.frontend.console.views.Model;

public class WireView extends IdentifiedAbstractView implements IWireView {

	public static final int ORDER_WIREVIEW = FeatureView.ORDER_FEATURE + 1;

	public static final String EL_WIRE = "wire";

	public static final String EL_SOURCE_CONNECTOR = "sourceConnector";

	public static final String EL_TARGET_CONNECTOR = "targetConnector";

	private IConnector sourceConnector;

	private IConnector targetConnector;

	private List<IPoint> path = new ArrayList<IPoint>();

	public WireView() {
		this(-1, null, null);
	}

	public WireView(int id, IIdentifiedAnchor source, IIdentifiedAnchor target) {
		super(id);
		setOrder(ORDER_WIREVIEW);
		sourceConnector = new Connector(this);
		targetConnector = new Connector(this);
		sourceConnector.connect(source == null ? new ProxiedAnchorFinder(
				sourceConnector) : source);
		targetConnector.connect(target == null ? new ProxiedAnchorFinder(
				targetConnector) : target);
		setColor(Color.green);
	}

	public WireView(int id, int sourceId, int targetId) {
		super(id);
		sourceConnector = new Connector(this);
		targetConnector = new Connector(this);
		sourceConnector.connect(new ProxiedAnchorFinder(sourceConnector,
				sourceId));
		targetConnector.connect(new ProxiedAnchorFinder(targetConnector,
				targetId));
		setColor(Color.green);
	}

	@Override
	public void setSourceId(int id) {
		if (sourceConnector.getAnchor() instanceof ProxiedAnchorFinder) {
			sourceConnector.getAnchor().setId(id);
			update();
			return;
		}
		throw new UnsupportedOperationException("Not supported!");
	}

	@Override
	public void setTargetId(int id) {
		if (targetConnector.getAnchor() instanceof ProxiedAnchorFinder) {
			targetConnector.getAnchor().setId(id);
			update();
			return;
		}
		throw new UnsupportedOperationException("Not supported!");
	}

	@Override
	public IConnector getSourceConnector() {
		return sourceConnector;
	}

	@Override
	public void setSourceConnector(IConnector sourceConnector) {
		this.sourceConnector = sourceConnector;
		sourceConnector.setOwner(this);
	}

	@Override
	public IConnector getTargetConnector() {
		return targetConnector;
	}

	@Override
	public void setTargetConnector(IConnector targetConnector) {
		this.targetConnector = targetConnector;
		targetConnector.setOwner(this);
	}

	protected void drawHumanablePath(CBRGraphics g, List<IPoint> points) {
		for (int i = 1; i < points.size(); i++)
			g.drawLine(points.get(i - 1), points.get(i));
		g.fillRound(points.get(0), 8);
		g.fillRound(points.get(points.size() - 1), 8);
	}

	@Override
	public void prePaint(CBRGraphics g) {
		IConnector sourceConnector = getSourceConnector();
		IConnector targetConnector = getTargetConnector();
		final IPoint sourceLocation = sourceConnector.getLocation().copy();
		final IPoint targetLocation = targetConnector.getLocation().copy();
		if (sourceConnector.getAnchor() instanceof IIdentifiedAnchorView)
			((IIdentifiedAnchorView) sourceConnector.getAnchor())
					.translateToAbsolute(sourceLocation);
		if (targetConnector.getAnchor() instanceof IIdentifiedAnchorView)
			((IIdentifiedAnchorView) targetConnector.getAnchor())
					.translateToAbsolute(targetLocation);
		drawHumanablePath(g, new LinkedList<IPoint>() {
			private static final long serialVersionUID = 1L;
			{
				add(sourceLocation);
				addAll(getPath());
				add(targetLocation);
			}
		});
	}

	@Override
	public List<IPoint> getPath() {
		return path;
	}

	@Override
	public void update() {
		sourceConnector.update();
		targetConnector.update();
		WireLayer wiresLayer = getWireLayer();
		if (wiresLayer != null) {
			wiresLayer.updateWire(this);
		}
	}

	private WireLayer getWireLayer() {
		IAreaView areaView = getParent();
		if (areaView instanceof Model) {
			IAreaView console = areaView.getParent();
			if (console instanceof CBRConsole) {
				return ((CBRConsole) console).getWireLayer();
			}
		}
		return null;
	}

}
