package cbr.pt.frontend.console;

import cbr.serialize.xml.annotations.XMLAttribute;
import cbr.serialize.xml.annotations.XMLNode;

@XMLNode("identified")
public interface IIdentified {

	@XMLAttribute
	public int getId();

	public void setId(int id);

}
