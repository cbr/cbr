package cbr.obstacles;

import java.util.HashMap;
import java.util.Map;

import cbr.channels.manager.AbstractChannelsManager;
import cbr.grid.IObstaclesBoundedGrid;
import cbr.grid.ObstaclesDynamicBoundedGrid;

public abstract class AbstractObstaclesManager<T, I> {

	private IObstaclesBoundedGrid grid;

	private AbstractChannelsManager<I, T> channelsManager;

	private Map<T, IObstacle> constraintToObstacle = new HashMap<T, IObstacle>();

	private Map<IObstacle, T> obstacleToConstraint = new HashMap<IObstacle, T>();

	public AbstractObstaclesManager(
			AbstractChannelsManager<I, T> channelsManager) {
		this.channelsManager = channelsManager;
	}

	public AbstractChannelsManager<I, T> getChannelsManager() {
		return channelsManager;
	}

	public IObstacle getObstacleByConstraint(T object) {
		return constraintToObstacle.get(object);
	}

	public void addObstacleByConstraint(T object) {
		Obstacle obstacle = createNewObstacleByConstraint(object);
		// TODO: update grid, and set cells to obstacle
		constraintToObstacle.put(object, obstacle);
		obstacleToConstraint.put(obstacle, object);
		grid.addObstacle(obstacle);
	}

	public void removeObstacleByConstraint(T object) {
		// TODO: update grid with removing obstacle
		IObstacle obstacle = constraintToObstacle.remove(object);
		grid.removeObstacle(obstacle);
		obstacleToConstraint.remove(obstacle);
		obstacle.dispose();
	}

	public void updateObstacleByConstraintChanges(T object) {
		IObstacle obstacle = constraintToObstacle.get(object);
		IObstacle obstacleUpdated = createNewObstacleByConstraint(object);
		grid.updateObstacle(obstacle, obstacleUpdated);
	}

	public void uddateGridByContainerConstraintChanges(T object) {
		updateGridByContainerConstraintChangesLocal(object);
	}

	public void removeObstacle(Obstacle obstacle) {
		// for(Cell cell : obstacle.getCells()) {
		// channelsManager.remove(cell);
		// cell.dispose();
		// }
		return;
		/*
		 * for (Obstacle obstacle : constraintToObstacle .values()) { if
		 * (obstacle.isContaints(cell)) { // TODO: dispose this cell and update
		 * // wires with this cell channelsManager.remove(cell); cell.dispose();
		 * break; } }
		 */
	}

	public IObstaclesBoundedGrid getGrid() {
		return grid;
	}

	/**
	 * 
	 * Updates x,y,width,height of grid by constraint specific implementation
	 * 
	 * @param object
	 * @return
	 */
	abstract protected Obstacle updateGridByContainerConstraintChangesLocal(
			T object);

	/**
	 * 
	 * TODO: determine sides, cells and etc. parameters from Grid
	 * 
	 * @param object
	 * @return
	 */
	abstract protected Obstacle createNewObstacleByConstraint(T object);

	public void setGrid(ObstaclesDynamicBoundedGrid<I, T> grid) {
		this.grid = grid;
	}

	public boolean isContainsConstraint(T object) {
		return constraintToObstacle.containsKey(object);
	}

	public void dispose() {
		grid = null;
		for (IObstacle obstacle : constraintToObstacle.values()) {
			obstacle.dispose();
		}
		constraintToObstacle.clear();
		obstacleToConstraint.clear();
		constraintToObstacle = null;
		obstacleToConstraint = null;
	}

}
