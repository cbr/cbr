package cbr.geometry;

import cbr.serialize.xml.annotations.XMLElement;
import cbr.serialize.xml.annotations.XMLNode;

@XMLNode("sizeChanged")
public interface ISizeChanged {

	@XMLElement
	public IDimension getSize();

	public void setSize(IDimension size);

}
