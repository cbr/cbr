package cbr.grid;

import cbr.geometry.IRectangle;
import cbr.obstacles.IObstacle;

/**
 * 
 * Dynamic grid with obstacle support
 * 
 * @author Alexander Strakh
 * 
 */
public interface IObstaclesBoundedGrid<T, I> extends IBoundedGrid<T, I> {

	/**
	 * 
	 * Вызывается для добавления препятсвия с указанными координатами и высотой<br>
	 * 
	 * @param x
	 * @param y
	 * @param width
	 * @param height
	 * @return true, если препятствие было добавлено<br>
	 *         Препятствие может быть не добавлено, если оно выходит за<br>
	 *         границы сетки.<br>
	 */
	public boolean addObstacle(IObstacle obstalce);

	public boolean removeObstacle(IObstacle obstalce);

	public boolean updateObstacle(IObstacle obstalce, IRectangle updatedBounds);

	public ICell getCellByPoint(int x, int y);

	public ICell getCell(int x, int y);

	public ICell[][] getCells();

}
