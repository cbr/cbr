package cbr.pt.frontend.console;

public interface IModelObjectAddOrRemoveListener {

	public void addToModel(IAreaView view, IAreaView parent);

	public void removeFromModel(IAreaView view, IAreaView parent);

	public void initializeModel();

}
