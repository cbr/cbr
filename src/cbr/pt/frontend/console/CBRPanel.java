package cbr.pt.frontend.console;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;

import javax.swing.JPanel;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.xml.sax.SAXException;

import cbr.geometry.Dimension;
import cbr.pt.frontend.CBRApp;
import cbr.pt.frontend.CBRAppContext;
import cbr.pt.frontend.Config;
import cbr.pt.frontend.actions.CBRModelActionUtils;

public class CBRPanel extends JPanel {

	private static final long serialVersionUID = 1L;

	private CBRConsole console;

	private int bufferWidth;
	private int bufferHeight;
	private Image bufferImage;
	private Graphics bufferGraphics;
	private CBRAppContext appContext;

	public CBRPanel(CBRAppContext appContext) {
		this.appContext = appContext;
		Config config = appContext.getConfig();
		CBRContext context = config.getContext();
		context.setContainer(this);
		console = context.getConsole();
		context.createNewModel();
		addMouseListener(new MouseListener() {
			@Override
			public void mouseReleased(MouseEvent e) {
				Point tempPoint = e.getPoint();
				console.getContext().getActiveTool()
						.mouseReleased(tempPoint.x, tempPoint.y);
			}

			@Override
			public void mousePressed(MouseEvent e) {
				Point tempPoint = e.getPoint();
				console.getContext().getActiveTool()
						.mousePressed(tempPoint.x, tempPoint.y);
			}

			@Override
			public void mouseExited(MouseEvent e) {
				Point tempPoint = e.getPoint();
				console.getContext().getActiveTool()
						.mouseExited(tempPoint.x, tempPoint.y);
			}

			@Override
			public void mouseEntered(MouseEvent e) {
				Point tempPoint = e.getPoint();
				console.getContext().getActiveTool()
						.mouseEntered(tempPoint.x, tempPoint.y);
			}

			@Override
			public void mouseClicked(MouseEvent e) {
				Point tempPoint = e.getPoint();
				console.getContext().getActiveTool()
						.mouseClicked(tempPoint.x, tempPoint.y);
			}
		});
		addMouseMotionListener(new MouseMotionListener() {
			@Override
			public void mouseMoved(MouseEvent e) {
				Point tempPoint = e.getPoint();
				console.getContext().getActiveTool()
						.mouseMoved(tempPoint.x, tempPoint.y);
			}

			@Override
			public void mouseDragged(MouseEvent e) {
				Point tempPoint = e.getPoint();
				console.getContext().getActiveTool()
						.mouseDragged(tempPoint.x, tempPoint.y);
			}
		});
	}

	public void paintBuffer(Graphics g) {
		if (g instanceof Graphics2D) {
			((Graphics2D) g).setRenderingHint(
					RenderingHints.KEY_TEXT_ANTIALIASING,
					RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
			((Graphics2D) g).setRenderingHint(
					RenderingHints.KEY_STROKE_CONTROL,
					RenderingHints.VALUE_STROKE_NORMALIZE);
			((Graphics2D) g).setRenderingHint(RenderingHints.KEY_RENDERING,
					RenderingHints.VALUE_RENDER_QUALITY);
			((Graphics2D) g).setRenderingHint(
					RenderingHints.KEY_ALPHA_INTERPOLATION,
					RenderingHints.VALUE_ALPHA_INTERPOLATION_QUALITY);
			((Graphics2D) g).setRenderingHint(RenderingHints.KEY_DITHERING,
					RenderingHints.VALUE_DITHER_ENABLE);
			((Graphics2D) g).setRenderingHint(RenderingHints.KEY_INTERPOLATION,
					RenderingHints.VALUE_INTERPOLATION_BICUBIC);
		}
		Rectangle bounds = getBounds();
		console.setSize(new Dimension(bounds.width, bounds.height));
		console.paint(g);
	}

	@Override
	public void update(Graphics g) {
		paint(g);
	}

	private void resetBuffer() {
		bufferWidth = getSize().width;
		bufferHeight = getSize().height;
		if (bufferGraphics != null) {
			bufferGraphics.dispose();
			bufferGraphics = null;
		}
		if (bufferImage != null) {
			bufferImage.flush();
			bufferImage = null;
		}
		System.gc();
		bufferImage = createImage(bufferWidth, bufferHeight);
		bufferGraphics = bufferImage.getGraphics();
	}

	public void paint(Graphics g) {
		if (bufferWidth != getSize().width || bufferHeight != getSize().height
				|| bufferImage == null || bufferGraphics == null)
			resetBuffer();
		if (bufferGraphics != null) {
			bufferGraphics.clearRect(0, 0, bufferWidth, bufferHeight);
			paintBuffer(bufferGraphics);
			g.drawImage(bufferImage, 0, 0, this);
		}
	}

	public void init() {
		Config config = appContext.getConfig();
		if (config.autoLoadModel != null) {
			try {
				CBRModelActionUtils.disposeModel(appContext);
				CBRModelActionUtils.loadModel(config,
						Config.getModelPath(config.autoLoadModel));
			} catch (IllegalAccessException | IllegalArgumentException
					| InvocationTargetException | InstantiationException
					| ParserConfigurationException | TransformerException
					| SAXException | IOException e) {
				CBRApp.logger().err(e.getMessage());
				CBRApp.logger().warn("Due to errors create new empty model");
				config.getContext().createNewModel();
			}
		}
	}

}
