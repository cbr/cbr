package cbr.pt.frontend.console.views.serialize.xml;

import cbr.pt.frontend.console.IModel;
import cbr.pt.frontend.console.IZone;
import cbr.pt.frontend.console.views.Connection;
import cbr.pt.frontend.console.views.Device;
import cbr.pt.frontend.console.views.IConnection;
import cbr.pt.frontend.console.views.IDevice;
import cbr.pt.frontend.console.views.IPort;
import cbr.pt.frontend.console.views.Model;
import cbr.pt.frontend.console.views.Port;
import cbr.pt.frontend.console.views.Zone;
import cbr.pt.frontend.serialize.xml.CBRXMLSerializer;

public class CBRViewSerializer extends CBRXMLSerializer {

	public CBRViewSerializer() {
		addMapping("connection", IConnection.class, Connection.class);
		addMapping("device", IDevice.class, Device.class);
		addMapping("port", IPort.class, Port.class);
		addMapping("zone", IZone.class, Zone.class);
		addMapping("model", IModel.class, Model.class);
	}

}
