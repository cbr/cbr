package cbr.channels.manager.pipe;

import java.util.HashSet;
import java.util.Set;

import cbr.IPathBuilderAlgorythm;
import cbr.channels.IChannel;
import cbr.pt.frontend.CBRApp;
import cbr.wires.IWire;

public class ChannelsPipePathBuilderStage extends AbstractChannelsPipeStage {

	private IPathBuilderAlgorythm algorythm;

	private Set<IWire> completedWires = new HashSet<IWire>();

	private Set<IWire> invalidWires = new HashSet<IWire>();

	private static int count = 0;

	@Override
	protected void processChannel(IChannel channel) {
		for (IWire wire : channel.getWires()) {
			if (algorythm != null) {
				algorythm.process(channel, wire);
				CBRApp.logger().info("Path builder calls counter: " + ++count);
			}
			completedWires.add(wire);
		}
	}

	@Override
	public void clean() {
		super.clean();
		completedWires.clear();
		invalidWires.clear();
	}

	public Set<IWire> getCompletedWires() {
		return completedWires;
	}

	public void setAlgorythm(IPathBuilderAlgorythm algorythm) {
		this.algorythm = algorythm;
	}

	public void addInvalidWire(IWire wire) {
		invalidWires.add(wire);
	}

	@Override
	public void process() {
		super.process();
		for (IWire wire : invalidWires) {
			algorythm.process(null, wire);
			CBRApp.logger().info("Path builder calls counter: " + ++count);
			completedWires.add(wire);
		}
	}

}
