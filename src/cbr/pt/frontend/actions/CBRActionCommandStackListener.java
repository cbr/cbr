package cbr.pt.frontend.actions;

import cbr.pt.frontend.CBRAction;
import cbr.pt.frontend.CBRAppContext;
import cbr.pt.frontend.command.CommandStack;
import cbr.pt.frontend.command.ICommandStackListener;

public class CBRActionCommandStackListener extends CBRAction implements
		ICommandStackListener {

	private static final long serialVersionUID = 1L;

	public CBRActionCommandStackListener(CommandStack commandStack,
			CBRAppContext appContext, String name, String tip, String iconPath) {
		super(appContext, name, tip, iconPath);
		commandStack.addCommandStackListener(this);
	}

	@Override
	public void handleCommandStackChanged(CommandStack commandStack, int event) {
	}

}
