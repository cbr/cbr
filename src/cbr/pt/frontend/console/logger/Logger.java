package cbr.pt.frontend.console.logger;

import java.awt.Color;

import cbr.pt.frontend.CBRInfoPanel;

public class Logger {

	private CBRInfoPanel infoPanel;

	public Logger(CBRInfoPanel infoPanel) {
		this.infoPanel = infoPanel;
	}

	public void err(String message) {
		infoPanel.writeMessage(message, Color.RED);
	}

	public void warn(String message) {
		infoPanel.writeMessage(message, Color.YELLOW);
	}

	public void info(String message) {
		infoPanel.writeMessage(message, Color.GREEN);
	}

}
