package cbr.pt.frontend.actions;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.xml.sax.SAXException;

import cbr.pt.frontend.CBRApp;
import cbr.pt.frontend.CBRAppContext;
import cbr.pt.frontend.Config;
import cbr.pt.frontend.console.CBRContext;
import cbr.pt.frontend.console.IAreaView;
import cbr.pt.frontend.tests.CommandTest;
import cbr.pt.frontend.tests.TestUtils;

public class CBRModelActionUtils {

	public static JFileChooser fileChooser = new JFileChooser();

	public static void loadModel(Config config, String path)
			throws IllegalAccessException, IllegalArgumentException,
			InvocationTargetException, InstantiationException,
			ParserConfigurationException, TransformerException, SAXException,
			IOException {
		CBRApp.logger().info("Try to load model: " + path);
		config.filePath = path;
		CBRContext context = config.getContext();
		context.restoreCBRModel();
		context.repaint();
		context.getCommandStack().markSaveLocation();
	}

	protected static boolean saveModel(CBRAppContext appContext)
			throws IllegalAccessException, IllegalArgumentException,
			InvocationTargetException, ParserConfigurationException,
			TransformerException {
		Config config = appContext.getConfig();
		if (config.filePath == null) {
			if (fileChooser.showSaveDialog(appContext.getFrame()) == JFileChooser.APPROVE_OPTION) {
				File file = fileChooser.getSelectedFile();
				config.filePath = file.getAbsolutePath();
				CBRContext context = config.getContext();
				context.flush();
				context.getCommandStack().markSaveLocation();
				return true;
			}
		} // TODO: else if file path already set
		return false;
	}

	protected static boolean isNeedsToSave(CBRAppContext appContext) {
		if (appContext.getConfig().getContext().getCommandStack().isDirty()) {
			if (JOptionPane.YES_OPTION == JOptionPane.showConfirmDialog(
					appContext.getFrame(), "Would you like to save?", "Save",
					JOptionPane.YES_NO_OPTION)) {
				return true;
			}
		}
		return false;
	}

	protected static boolean isNeedsToSaveAndTryToSave(CBRAppContext appContext)
			throws IllegalAccessException, IllegalArgumentException,
			InvocationTargetException, ParserConfigurationException,
			TransformerException {
		boolean isNeedsToSaveDirtyModel = isNeedsToSave(appContext);
		boolean isDirtyModelSaveOpeartionFinished = false;
		if (isNeedsToSaveDirtyModel) {
			isDirtyModelSaveOpeartionFinished = saveModel(appContext);
		}
		return (isNeedsToSaveDirtyModel && isDirtyModelSaveOpeartionFinished)
				|| !isNeedsToSaveDirtyModel;
	}

	public static void disposeModel(CBRAppContext appContext) {
		Config config = appContext.getConfig();
		config.filePath = null;
		config.getContext().dispose();
	}

	public static IAreaView removeSelectFromSelectionAndSendToClipboard(
			CBRAppContext appContext) throws IllegalAccessException,
			IllegalArgumentException, InvocationTargetException,
			ParserConfigurationException, TransformerException {
		CBRContext context = appContext.getConfig().getContext();
		IAreaView selectedArea = context.getSelection();
		context.removeSelection();
		context.getClipboard().setClipboardContents(selectedArea);
		return selectedArea;
	}

	public static void runTest(CBRAppContext context, String path) {
		CBRApp.logger().info("Try to load test: " + path);
		Config config = context.getConfig();
		CommandTest test = TestUtils.loadTest(config.getContext(), path,
				config.filePath);
		if (test == null) {
			CBRApp.logger().err("Can't load test: " + path);
		} else {
			CBRApp.logger().info("Try to run test: " + path);
			test.run();
			CBRApp.logger().info("Test finished: " + path);
		}
	}

}
