package cbr.pt.frontend.console;

import java.awt.Container;
import java.awt.Graphics;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.xml.sax.SAXException;

import cbr.FileUtils;
import cbr.IDisposable;
import cbr.pt.frontend.CBRAppContext;
import cbr.pt.frontend.Config;
import cbr.pt.frontend.IAppContextProvider;
import cbr.pt.frontend.command.CommandStack;
import cbr.pt.frontend.console.clipboard.CBRClipboard;
import cbr.pt.frontend.console.tools.AbstractTool;
import cbr.pt.frontend.console.tools.DefaultEditTool;
import cbr.pt.frontend.console.tools.IToolChangeListener;
import cbr.serialize.xml.XMLSerializer;

public class CBRContext implements IDisposable {

	private IAreaView underMouseView;

	private IAreaView clickedView;

	private Config config;

	private Container container;

	private CBRConsole console;

	private AbstractViewFactory factory;

	private XMLSerializer serializer;

	private CommandStack commandStack;

	private AbstractTool activeTool;

	private AbstractTool defaultTool;

	private List<IToolChangeListener> toolChangeListeners = new ArrayList<IToolChangeListener>();

	private List<IModelObjectAddOrRemoveListener> modelObjectAddOrRemoveListeners = new ArrayList<IModelObjectAddOrRemoveListener>();

	private List<ISelectedListener> selectedListeners = new ArrayList<ISelectedListener>();

	private CBRGraphics graphics;

	private IAreaView selection;

	private IAreaView selectionFeedback;

	private CBRClipboard clipboard;

	public CBRContext(Config config, AbstractViewFactory factory) {
		this.config = config;
		this.factory = factory;
		factory.setContext(this);
		defaultTool = new DefaultEditTool(this);
		clipboard = new CBRClipboard(this);
	}

	public CommandStack getCommandStack() {
		if (commandStack == null) {
			commandStack = new CommandStack();
		}
		return commandStack;
	}

	public XMLSerializer getSerializer() {
		if (serializer == null) {
			serializer = getFactory().createSerializer();
		}
		return serializer;
	}

	public void setContainer(Container container) {
		this.container = container;
	}

	public IAreaView getUnderMouseView() {
		return underMouseView;
	}

	public void setUnderMouseView(IAreaView underMouseView) {
		this.underMouseView = underMouseView;
	}

	public IAreaView getClickedView() {
		return clickedView;
	}

	public void setClickedView(IAreaView clickedView) {
		this.clickedView = clickedView;
	}

	public Config getConfig() {
		return config;
	}

	public void repaint() {
		container.repaint();
	}

	public int getUniqueId() {
		return getMaxId(getConsole().getRoot(), 1);
	}

	private int getMaxId(IAreaView view, int id) {
		if (view instanceof IIdentified && id <= ((IIdentified) view).getId())
			id = ((IIdentified) view).getId() + 1;
		for (IAreaView childView : view.getChildrens()) {
			id = getMaxId(childView, id);
		}
		return id;
	}

	public AbstractViewFactory getFactory() {
		return factory;
	}

	public boolean isCBRModelExists() {
		return console == null ? false : getConsole().isCBRModelExists();
	}

	public CBRModel getCBRModel() {
		return getConsole().getCBRModel();
	}

	public void restoreCBRModel() throws IllegalAccessException,
			IllegalArgumentException, InvocationTargetException,
			InstantiationException, ParserConfigurationException,
			TransformerException, SAXException, IOException {
		Config config = getConfig();
		String content = FileUtils.readAllFromTextFile(config.filePath);
		Object model = getSerializer().deserializeFromString(content);
		if (model instanceof CBRModel) {
			setCBRModel((CBRModel) model);
		}
	}

	private void setCBRModel(CBRModel model) {
		selectDefaultTool();
		((CBRModel) model).setContext(this);
		getConsole().setCBRModel(model);
	}

	public void flush() throws IllegalAccessException,
			IllegalArgumentException, InvocationTargetException,
			ParserConfigurationException, TransformerException {
		String content = getSerializer().serializeToString(getCBRModel());
		try {
			FileUtils.writeAllToTextFile(getConfig().filePath, content);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public CBRAppContext getAppContext() {
		Container localContainer = container;
		while (localContainer != null) {
			if (localContainer instanceof IAppContextProvider)
				return ((IAppContextProvider) localContainer).getAppContext();
			localContainer = localContainer.getParent();
		}
		return null;
	}

	public CBRClipboard getClipboard() {
		return clipboard;
	}

	private void switchOffRouter() {
		if (console != null) {
			console.switchOffRouter();
		}
	}

	@Override
	public void dispose() {
		// TODO: remove old listeners
		switchOffRouter();
		removeSelection();
		selectDefaultTool();
		underMouseView = null;
		clickedView = null;
		if (console != null) {
			console.dispose();
		}
		if (commandStack != null) {
			commandStack.markSaveLocation();
			commandStack.clean();
		}
	}

	public CBRConsole getConsole() {
		if (console == null) {
			console = new CBRConsole(config);
		}
		return console;
	}

	public void selectDefaultTool() {
		setActiveTool(defaultTool);
	}

	public void setActiveTool(AbstractTool activeTool) {
		if (this.activeTool != activeTool) {
			if (this.activeTool != null)
				this.activeTool.unselect();
			this.activeTool = activeTool;
			fireToolChanged();
			this.activeTool.select();
		}
	}

	public AbstractTool getActiveTool() {
		if (activeTool == null)
			selectDefaultTool();
		return activeTool;
	}

	private void fireToolChanged() {
		for (IToolChangeListener toolChangeListener : toolChangeListeners) {
			toolChangeListener.toolChanged();
		}
	}

	public void addModelObjectAddOrRemoveListener(
			IModelObjectAddOrRemoveListener listener) {
		if (!modelObjectAddOrRemoveListeners.contains(listener))
			modelObjectAddOrRemoveListeners.add(listener);
	}

	public void removeModelObjectAddOrRemoveListener(
			IModelObjectAddOrRemoveListener listener) {
		if (!modelObjectAddOrRemoveListeners.contains(listener))
			modelObjectAddOrRemoveListeners.remove(listener);
	}

	public void addToolChangeListener(IToolChangeListener listener) {
		if (!toolChangeListeners.contains(listener))
			toolChangeListeners.add(listener);
	}

	public void addSelectedListener(ISelectedListener listener) {
		if (!selectedListeners.contains(listener))
			selectedListeners.add(listener);
	}

	public CBRGraphics getGraphics(Graphics graphics) {
		if (this.graphics == null) {
			this.graphics = new CBRGraphics(this);
		}
		this.graphics.setGraphics(graphics);
		return this.graphics;
	}

	public void createNewModel() {
		CBRModel model = getFactory().createModelView();
		setCBRModel(model);
	}

	public void removeSelection() {
		if (selection != null) {
			selection.removeChangeBoundsListener(selectionFeedback);
			getConsole().getServiceLayer().removeChild(selectionFeedback);
			selectionFeedback.dispose();
			selectionFeedback = null;
			selection = null;
			fireSelectionChanged();
		}
	}

	public IAreaView getSelection() {
		return selection;
	}

	public void setSelection(IAreaView host, IAreaView selectionFeedback) {
		if (selection != null) {
			removeSelection();
		}
		selection = host;
		this.selectionFeedback = selectionFeedback;
		getConsole().getServiceLayer().addChild(selectionFeedback);
		selection.addChangeBoundsListener(selectionFeedback);
		fireSelectionChanged();
	}

	private void fireSelectionChanged() {
		for (ISelectedListener listener : selectedListeners) {
			listener.selectionChanged();
		}
	}

	public void fireChildAdded(IAreaView child, AbstractView parent) {
		for (IModelObjectAddOrRemoveListener listener : modelObjectAddOrRemoveListeners) {
			listener.addToModel(child, parent);
		}
	}

	public void fireChildRemoved(IAreaView child, AbstractView parent) {
		for (IModelObjectAddOrRemoveListener listener : modelObjectAddOrRemoveListeners) {
			listener.removeFromModel(child, parent);
		}
	}

	public IAreaView findModelById(int id) {
		CBRModel model = getCBRModel();
		if (model != null)
			return model.findModelById(id);
		return null;
	}

}
