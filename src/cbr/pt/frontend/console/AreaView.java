package cbr.pt.frontend.console;

import java.util.HashMap;
import java.util.Map;

import cbr.geometry.IPoint;

public class AreaView extends IdentifiedAnchorProvidedAbstractView implements
		IFeatureLocatorProvider {

	public static final int ORDER_AREAVIEW = CBRZone.ORDER_ZONE + 1;

	private Map<Class<? extends IFeatureView>, IFeatureLocator> featureLocators = new HashMap<Class<? extends IFeatureView>, IFeatureLocator>();

	private IFeatureLocator defaultFeatureLocator;

	public AreaView() {
		this(-1, 0, 0, 0, 0);
	}

	public AreaView(int id, int x, int y, int width, int height) {
		super(id);
		setOrder(ORDER_AREAVIEW);
		getLocation().setX(x);
		getLocation().setY(y);
		getSize().setWidth(width);
		getSize().setHeight(height);
	}

	public void addFeatureLocator(Class<? extends IFeatureView> featureClass,
			IFeatureLocator locator) {
		featureLocators.put(featureClass, locator);
	}

	@Override
	public void prePaint(CBRGraphics g) {
		g.fillRect(getBounds());
	}

	@Override
	protected IAnchorPointLocator createLocator() {
		return new RectangleAnchorPointLocator(this);
	}

	@Override
	public IFeatureLocator getLocator(IFeatureView feature) {
		return getLocator(feature.getClass());
	}

	@Override
	public IFeatureLocator getLocator(Class<? extends IFeatureView> featureClass) {
		IFeatureLocator locator = featureLocators.get(featureClass);
		return locator == null ? getDefualtFeatureLocator() : locator;
	}

	private IFeatureLocator getDefualtFeatureLocator() {
		if (defaultFeatureLocator == null) {
			defaultFeatureLocator = createDefaultFeatureLocator();
		}
		return defaultFeatureLocator;
	}

	protected IFeatureLocator createDefaultFeatureLocator() {
		return new RectangleContourFeatureLocator(this);
	}

	@Override
	protected void boundsChanged() {
		for (IAreaView view : getChildrens()) {
			if (view instanceof IFeatureView) {
				IFeatureView featureView = (IFeatureView) view;
				IFeatureLocator featureLocator = getLocator(featureView);
				if (featureLocator != null) {
					int oldSide = featureView.getSide();
					IPoint location = featureView.getCenter();
					translateToAbsolute(location);
					int newSide = featureLocator.locate(location, oldSide);
					if (newSide != oldSide) {
						featureView.setSide(newSide);
					}
					translateToRelative(location);
					featureView.translateFromCenter(location);
					featureView.setLocation(location);
				}
			}
		}
		super.boundsChanged();
	}

}
