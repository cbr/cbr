package cbr.pt.router;

import cbr.channels.manager.AbstractChannelsManager;
import cbr.grid.IObstaclesBoundedGrid;
import cbr.obstacles.AbstractObstaclesManager;
import cbr.obstacles.Obstacle;
import cbr.pt.frontend.console.IAreaView;
import cbr.pt.frontend.console.IWireView;

public class CBRObstaclesManager extends
		AbstractObstaclesManager<IAreaView, IWireView> {

	public CBRObstaclesManager(
			AbstractChannelsManager<IWireView, IAreaView> channelsManager,
			IObstaclesBoundedGrid grid) {
		super(channelsManager);
	}

	@Override
	protected Obstacle updateGridByContainerConstraintChangesLocal(
			IAreaView object) {
		return null;
	}

	@Override
	protected Obstacle createNewObstacleByConstraint(IAreaView object) {
		return new Obstacle(object.getBounds().copy());
	}

}
