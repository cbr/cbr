package cbr.geometry;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "dimension")
public class Dimension implements IDimension {

	private int width;

	private int height;

	public Dimension(int width, int height) {
		this.width = width;
		this.height = height;
	}

	public Dimension() {
		this(0, 0);
	}

	@Override
	public int getWidth() {
		return width;
	}

	@Override
	public void setWidth(int width) {
		this.width = width;
	}

	@Override
	public int getHeight() {
		return height;
	}

	@Override
	public void setHeight(int height) {
		this.height = height;
	}

	@Override
	public String toString() {
		return "Dimension(" + width + ", " + height + ")";
	}

	@Override
	public boolean equals(Object o) {
		if (o instanceof IDimension) {
			IDimension p = (IDimension) o;
			return p.getWidth() == width && p.getHeight() == height;
		}
		return false;
	}

	@Override
	public IDimension copy() {
		return new Dimension(width, height);
	}

}
