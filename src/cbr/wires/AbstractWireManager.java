package cbr.wires;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import cbr.AbstractChannelBasedRouter;
import cbr.channels.manager.AbstractChannelsManager;
import cbr.grid.IObstaclesBoundedGrid;
import cbr.obstacles.AbstractObstaclesManager;

public abstract class AbstractWireManager<T, I> {

	private Set<IWire> invalidWires = new HashSet<IWire>();

	private AbstractChannelsManager<T, I> channelsManagers;

	private Map<T, IWire> constraintToWire = new HashMap<T, IWire>();

	private Map<IWire, T> wireToConstraint = new HashMap<IWire, T>();

	protected AbstractObstaclesManager<I, T> obstaclesManager;

	private IObstaclesBoundedGrid grid;

	protected AbstractChannelBasedRouter<T, I> cbr;

	public AbstractWireManager(AbstractChannelBasedRouter<T, I> cbr,
			AbstractChannelsManager<T, I> channelsManager) {
		this.channelsManagers = channelsManager;
		this.cbr = cbr;
	}

	public IObstaclesBoundedGrid getGrid() {
		return grid;
	}

	public AbstractObstaclesManager<I, T> getObstaclesManager() {
		return obstaclesManager;
	}

	public void setObstaclesManager(
			AbstractObstaclesManager<I, T> obstaclesManager) {
		this.obstaclesManager = obstaclesManager;
	}

	public void updateWireByConstraint(T object) {
		IWire wire = constraintToWire.get(object);
		if (wire == null)
			return;
		channelsManagers.updateWire(wire);
	}

	public void removeWireByConstraint(T object) {
		IWire wire = constraintToWire.remove(object);
		if (wire == null)
			return;
		wireToConstraint.remove(wire);
		channelsManagers.removeWire(wire);
		wire.dispose();
	}

	public void addNewWireByConstraint(T object) {
		assert (!constraintToWire.containsKey(object));
		IWire wire = createNewWireByConstraint(object);
		if (wire == null)
			return;
		refreshWireSourceAndTarget(wire, object);
		if (!wire.isValid())
			return;
		constraintToWire.put(object, wire);
		wireToConstraint.put(wire, object);
		channelsManagers.addWire(wire);
	}

	public T getWireConstraint(IWire wire) {
		return wireToConstraint.get(wire);
	}

	/*
	 * public void replaceWires(List<IWire> wires) { removeWire(wire); }
	 */

	/**
	 * 
	 * TODO: determine sides, cells and etc. parameters from Grid
	 * 
	 * 
	 * tests if wire have points to exists cell or in grid bounds
	 * 
	 * @param object
	 * @return
	 */
	abstract protected IWire createNewWireByConstraint(T object);

	abstract protected void refreshWireSourceAndTarget(IWire wire, T connection);

	public void removeWire(IWire wire) {
		Object constraint = wireToConstraint.remove(wire);
		if (constraint == null)
			return;
		constraintToWire.remove(constraint);
		channelsManagers.removeWire(wire);
		wire.dispose();
	}

	public void setGrid(IObstaclesBoundedGrid grid) {
		this.grid = grid;
	}

	public void dispose() {
		for (IWire wire : constraintToWire.values()) {
			wire.dispose();
		}
		constraintToWire.clear();
		wireToConstraint.clear();
		constraintToWire = null;
		wireToConstraint = null;
		channelsManagers = null;
		obstaclesManager = null;
		grid = null;
	}

	public void refreshIndigentAndInvalidWires(Set<IWire> indigentWires) {
		for(IWire invalidWire : invalidWires) {
			refreshWireSourceAndTarget(invalidWire, wireToConstraint.get(invalidWire));
			if(invalidWire.isValid()) {
				invalidWires.remove(invalidWire);
				channelsManagers.addWire(invalidWire);
			}
		}
		for (IWire wire : indigentWires) {
			refreshWireSourceAndTarget(wire, wireToConstraint.get(wire));
			if (wire.isValid()) {
				invalidWires.remove(wire);
				channelsManagers.addWire(wire);
			} else if (invalidWires.add(wire)) {
				channelsManagers.addToBuildPathStageIvalidWire(wire);
			}
		}
	}

}
