package cbr.channels;

import java.util.ArrayList;
import java.util.List;

import cbr.grid.ICell;
import cbr.wires.IWire;
import cbr.wires.Wire;

public class Channel implements IChannel {

	private List<IWire> wires = new ArrayList<IWire>();

	private List<IWire> invertedWires = new ArrayList<IWire>();

	private List<ICell> path = new ArrayList<ICell>();

	private ICell source;

	private ICell target;

	public Channel(ICell source, ICell target) {
		this.source = source;
		source.addChannel(this);
		this.target = target;
		target.addChannel(this);
	}

	@Override
	public List<IWire> getWires() {
		return wires;
	}

	@Override
	public boolean removeWire(IWire wire) {
		invertedWires.remove(wire);
		return wires.remove(wire);
	}

	@Override
	public void addWire(IWire wire) {
		if (!wires.contains(wire)) {
			wires.add(wire);
			if (source == wire.getTargetSide()) {
				invertedWires.add(wire);
			}
		}
	}

	@Override
	public ICell getSource() {
		return source;
	}

	@Override
	public ICell getTarget() {
		return target;
	}

	/**
	 * 
	 * Вызывать можно только для соединения {@link Wire},<br>
	 * которое находится внутри канала.<br>
	 * Возвращает true, если соединение находится внутри канала<br>
	 * в перевернутом виде.<br>
	 * Т.е. конец соединения находтся в<br>
	 * начале канала, а начало соединения в конце канала.<br>
	 * 
	 * @param wire
	 * @return
	 */
	@Override
	public boolean isInvertedWire(IWire wire) {
		return invertedWires.contains(wire);
	}

	@Override
	public boolean isEmpty() {
		return wires.isEmpty();
	}

	@Override
	public void dispose() {
		invertedWires.clear();
		invertedWires = null;
		for (IWire wire : wires) {
			ICell sourceCell = wire.getSourceCell();
			if (sourceCell != null)
				sourceCell.removeChannel(this);
			ICell targetCell = wire.getTargetCell();
			if (targetCell != null)
				targetCell.removeChannel(this);
		}
		wires.clear();
		wires = null;
		for (ICell cell : path)
			cell.removeChannel(this);
		path.clear();
		path = null;
		source = null;
		target = null;
	}

	@Override
	public boolean contains(ICell cell) {
		return path.contains(cell);
	}

	@Override
	public void setPath(List<ICell> path) {
		for (ICell cell : path)
			cell.removeChannel(this);
		this.path.clear();
		this.path.addAll(path);
		for (ICell cell : path)
			cell.addChannel(this);
	}

	@Override
	public List<ICell> getPath() {
		return path;
	}

}
