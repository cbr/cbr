package cbr.pt.frontend.serialize.xml;

import java.util.ArrayList;
import java.util.List;

import cbr.geometry.Bounded;
import cbr.geometry.Dimension;
import cbr.geometry.IBounded;
import cbr.geometry.IDimension;
import cbr.geometry.IPoint;
import cbr.geometry.IRectangle;
import cbr.geometry.Point;
import cbr.geometry.Rectangle;
import cbr.pt.frontend.console.AreaView;
import cbr.pt.frontend.console.CBRModel;
import cbr.pt.frontend.console.CBRZone;
import cbr.pt.frontend.console.Connector;
import cbr.pt.frontend.console.IConnector;
import cbr.pt.frontend.console.IIdentifiedView;
import cbr.pt.frontend.console.IModel;
import cbr.pt.frontend.console.IWireView;
import cbr.pt.frontend.console.IZone;
import cbr.pt.frontend.console.WireView;
import cbr.serialize.xml.XMLSerializer;

public class CBRXMLSerializer extends XMLSerializer {
	public CBRXMLSerializer() {
		super(new CBRXMLSerializationStrategy());
		addMapping("point", IPoint.class, Point.class);
		addMapping("dimension", IDimension.class, Dimension.class);
		addMapping("rectangle", IRectangle.class, Rectangle.class);
		addMapping("bounded", IBounded.class, Bounded.class);
		addMapping("identifiedView", IIdentifiedView.class, AreaView.class);
		addMapping("model", IModel.class, CBRModel.class);
		addMapping("zone", IZone.class, CBRZone.class);
		addMapping("list", List.class, ArrayList.class);
		addMapping("wireView", IWireView.class, WireView.class);
		addMapping("connector", IConnector.class, Connector.class);
	}
}
