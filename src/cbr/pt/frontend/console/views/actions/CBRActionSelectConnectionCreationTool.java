package cbr.pt.frontend.console.views.actions;

import cbr.pt.frontend.CBRAppContext;
import cbr.pt.frontend.actions.CBRActionCreationTool;
import cbr.pt.frontend.console.tools.IToolChangeListener;
import cbr.pt.frontend.console.views.tools.ConnectionCreationTool;

public class CBRActionSelectConnectionCreationTool extends
		CBRActionCreationTool implements IToolChangeListener {

	private static final long serialVersionUID = 1L;

	public final static String ACTION_NAME = "Create connection";

	public final static String ACTION_TIP = "Create connection";

	public final static String ACTION_ICON = "create_connection.png";

	public CBRActionSelectConnectionCreationTool(CBRAppContext appContext) {
		super(appContext, ACTION_NAME, ACTION_TIP, ACTION_ICON,
				new ConnectionCreationTool(appContext.getConfig().getContext()));
	}

}
