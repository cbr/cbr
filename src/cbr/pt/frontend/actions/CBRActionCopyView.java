package cbr.pt.frontend.actions;

import java.awt.event.ActionEvent;
import java.lang.reflect.InvocationTargetException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import cbr.pt.frontend.CBRAppContext;

public class CBRActionCopyView extends CBRActionSelectedListener {

	private static final long serialVersionUID = 1L;

	public final static String ACTION_NAME = "Copy view";

	public final static String ACTION_TIP = "Copy view";

	public final static String ACTION_ICON = "copy.png";

	public CBRActionCopyView(CBRAppContext appContext) {
		super(appContext, ACTION_NAME, ACTION_TIP, ACTION_ICON);
	}

	@Override
	public void actionPerformed(ActionEvent event) {
		try {
			CBRModelActionUtils
					.removeSelectFromSelectionAndSendToClipboard(getAppContext());
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (TransformerException e) {
			e.printStackTrace();
		}
	}

}
