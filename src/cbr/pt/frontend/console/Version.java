package cbr.pt.frontend.console;

public class Version {

	public static final String VERSION = "version";

	public static final String MAJOR = "major";

	public static final String MINOR = "minor";

	public static final String ALPHA = "alpha";

	public static final String BETA = "beta";

	public int major = 0;

	public int minor = 1;

	public boolean isAlpha = true;

	public boolean isBeta = false;

	public Version(int major, int minor, boolean isAlpha, boolean isBeta) {
		super();
		this.major = major;
		this.minor = minor;
		this.isAlpha = isAlpha;
		this.isBeta = isBeta;
	}

	public String toString() {
		return "version " + major + "." + minor
				+ (isAlpha ? "a" : isBeta ? "b" : "");
	}

}
