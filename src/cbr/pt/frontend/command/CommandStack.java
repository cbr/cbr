package cbr.pt.frontend.command;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

import cbr.IDisposable;

public class CommandStack implements IDisposable {

	public final static int DEFAULT_COMMAND_STACK_SIZE = 10;

	public static final int NOTIFY_COMMAND_STACK_CHANGED = 0;

	protected List<ICommandStackListener> listeners = new ArrayList<ICommandStackListener>();

	private Stack<ICommand> redoable = new Stack<ICommand>();

	private int saveLocation = 0;

	private Stack<ICommand> undoable = new Stack<ICommand>();

	private int undoLimit = DEFAULT_COMMAND_STACK_SIZE;

	public CommandStack() {
	}

	public void execute(ICommand command) {
		if (command == null || !command.canExecute())
			return;
		flushRedo();
		command.execute();
		if (getUndoLimit() > 0) {
			while (undoable.size() >= getUndoLimit()) {
				undoable.remove(0).dispose();
				if (saveLocation > -1)
					saveLocation--;
			}
		}
		if (saveLocation > undoable.size())
			saveLocation = -1;
		undoable.push(command);
		notifyListeners(NOTIFY_COMMAND_STACK_CHANGED);
	}

	public void undo() {
		ICommand command = undoable.pop();
		command.undo();
		redoable.push(command);
		notifyListeners(NOTIFY_COMMAND_STACK_CHANGED);
	}

	public void redo() {
		if (!canRedo())
			return;
		ICommand command = redoable.pop();
		command.redo();
		undoable.push(command);
		notifyListeners(NOTIFY_COMMAND_STACK_CHANGED);
	}

	public boolean isDirty() {
		return undoable.size() != saveLocation;
	}

	public void markSaveLocation() {
		saveLocation = undoable.size();
		notifyListeners(NOTIFY_COMMAND_STACK_CHANGED);
	}

	public int getUndoLimit() {
		return undoLimit;
	}

	protected void notifyListeners(int event) {
		for (int i = 0; i < listeners.size(); i++)
			listeners.get(i).handleCommandStackChanged(this, event);
	}

	public void addCommandStackListener(ICommandStackListener listener) {
		if (!listeners.contains(listener)) {
			listeners.add(listener);
		}
	}

	public boolean canRedo() {
		return !redoable.isEmpty();
	}

	public boolean canUndo() {
		if (undoable.size() == 0)
			return false;
		return undoable.lastElement().canUndo();
	}

	@Override
	public void dispose() {
		flushUndo();
		flushRedo();
		removeAllListeners();
	}

	private void flushRedo() {
		while (!redoable.isEmpty())
			redoable.pop().dispose();
	}

	private void flushUndo() {
		while (!undoable.isEmpty())
			undoable.pop().dispose();
	}

	public boolean removeListener(ICommandStackListener listener) {
		return listeners.remove(listener);
	}

	private void removeAllListeners() {
		listeners.clear();
	}

	public void setUndoLimit(int undoLimit) {
		this.undoLimit = undoLimit;
	}

	public void clean() {
		flushUndo();
		flushRedo();
	}
}
