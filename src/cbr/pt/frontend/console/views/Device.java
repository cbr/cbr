package cbr.pt.frontend.console.views;

import cbr.obstacles.IObstacle;
import cbr.obstacles.Obstacle;
import cbr.pt.frontend.console.AreaView;
import cbr.pt.frontend.console.editpolicy.HighlightEditPolicy;
import cbr.pt.frontend.console.editpolicy.MovableAndResizableAreaEditPolicy;
import cbr.pt.frontend.console.editpolicy.MovableAreaEditPolicy;
import cbr.pt.frontend.console.views.editpolicy.ModelSpecificExcludingSelectionEditPolicy;
import cbr.pt.frontend.console.views.editpolicy.PortCreationEditPolicy;
import cbr.pt.router.CBRouter;

public class Device extends AreaView implements IDevice {

	private IObstacle obstalce;

	public Device() {
		addEditPolicy(new HighlightEditPolicy());
		addEditPolicy(new ModelSpecificExcludingSelectionEditPolicy());
		addEditPolicy(new MovableAndResizableAreaEditPolicy());
		addEditPolicy(new MovableAreaEditPolicy());
		addEditPolicy(new PortCreationEditPolicy());
	}

	public IObstacle getObstacle() {
		if (obstalce == null) {
			obstalce = new Obstacle(getBounds().copy());
		}
		return obstalce;
	}

	@Override
	public void boundsChanged() {
		super.boundsChanged();
		getObstacle().setX(getBounds().getX());
		getObstacle().setY(getBounds().getY());
		getObstacle().setWidth(getBounds().getWidth());
		getObstacle().setHeight(getBounds().getHeight());
		CBRouter router = getRouter();
		if (router != null) {
			router.updateObstacleByConstraintChanges(this);
		}
	}

	@Override
	public void recursiveAfterAddRootAction() {
		CBRouter router = getRouter();
		if (router != null) {
			if (!router.getObstaclesManager().isContainsConstraint(this)) {
				getRouter().addObstacleByConstraint(this);
			}
		}
	}

	public Model getModel() {
		return (Model) getParent();
	}

	public CBRouter getRouter() {
		Model model = getModel();
		return model == null ? null : model.getRouter();
	}
}
