package cbr.pt.frontend.actions.groups;

import cbr.pt.frontend.CBRActionGroup;
import cbr.pt.frontend.CBRAppContext;

public class CBRFileActionGroup extends CBRActionGroup {

	public CBRFileActionGroup(CBRAppContext appContext) {
		super(appContext, "File");
		addActionGroup(new CBRNewOpenActionGroup(appContext));
		addActionGroup(new CBRSaveSaveAsActionGroup(appContext));
	}

}
