package cbr.pt.frontend.actions.groups;

import cbr.pt.frontend.CBRActionGroup;
import cbr.pt.frontend.CBRAppContext;
import cbr.pt.frontend.console.views.actions.CBRActionSelectConnectionCreationTool;
import cbr.pt.frontend.console.views.actions.CBRActionSelectDeviceCreationTool;
import cbr.pt.frontend.console.views.actions.CBRActionSelectPortCreationTool;
import cbr.pt.frontend.console.views.actions.CBRActionSelectZoneCreationTool;

public class CBRModelActionGroup extends CBRActionGroup {

	public CBRModelActionGroup(CBRAppContext appContext) {
		super(appContext, "Model");
		addAction(new CBRActionSelectZoneCreationTool(appContext));
		addAction(new CBRActionSelectDeviceCreationTool(appContext));
		addAction(new CBRActionSelectPortCreationTool(appContext));
		addAction(new CBRActionSelectConnectionCreationTool(appContext));
	}

}
