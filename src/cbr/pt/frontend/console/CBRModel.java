package cbr.pt.frontend.console;

import java.util.LinkedList;
import java.util.List;

public class CBRModel extends AbstractView implements IModel {

	public static final int ORDER_MODEL = CBRConsole.ORDER_CONSOLE + 1;

	public CBRModel() {
		setOrder(ORDER_MODEL);
	}

	@Override
	public void paintChildrens(CBRGraphics g) {
		for (IAreaView view : getChildrens()) {
			if (view instanceof AreaView) {
				view.paint(g);
			}
		}
		for (IAreaView view : getChildrens()) {
			if (view instanceof WireView) {
				view.paint(g);
			}
		}
		for (IAreaView view : getChildrens()) {
			if (view instanceof CBRZone) {
				view.paint(g);
			}
		}
	}

	public void updateWire() {
		updateWires(this);
	}

	private void updateWires(IAreaView view) {
		if (view instanceof IWireView) {
			((IWireView) view).update();
		}
		List<IAreaView> operationList = new LinkedList<IAreaView>(
				view.getChildrens());
		for (Object childView : operationList) {
			updateWires((IAreaView) childView);
		}
	}

}
