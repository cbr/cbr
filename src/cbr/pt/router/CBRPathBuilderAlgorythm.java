package cbr.pt.router;

import java.util.Collections;
import java.util.List;

import cbr.Compass;
import cbr.IPathBuilderAlgorythm;
import cbr.channels.IChannel;
import cbr.geometry.IPoint;
import cbr.geometry.Point;
import cbr.grid.ICell;
import cbr.grid.IObstaclesBoundedGrid;
import cbr.wires.IWire;

public class CBRPathBuilderAlgorythm implements IPathBuilderAlgorythm {

	// TODO: Optimization: if cell is dirty after dividing or some another
	// operations without path finder - in this case we should update only
	// points for old cell
	public CBRPathBuilderAlgorythm(IObstaclesBoundedGrid grid) {
	}

	/**
	 * 
	 * k - расстояние между соединениями, проходящими через ребро. k =
	 * длина_ребра/соединения_через_ребро,
	 * 
	 * Если ребро поворотное, то на входе k остается тот-же, а на выходе
	 * умножается на коэффициент рассчитанный в зависимости от диагонлаи текущей
	 * ячейки (диагональ является линией поворота для всех поворотных
	 * соединений)
	 * 
	 */
	@Override
	public void process(IChannel channel, IWire wire) {
		if (channel == null) {
			wire.getPath().clear();
			wire.setNeedUpdatePath();
		} else {

			List<ICell> path = channel.getPath();
			if (path.size() > 0) {
				List<IPoint> points = wire.getPath();
				points.clear();

				boolean isInverted = false;
				ICell pathSource = path.get(0);
				ICell wireTarget = wire.getTargetCell();
				if (pathSource.getColumn() == wireTarget.getColumn()
						&& pathSource.getRow() == wireTarget.getRow())
					isInverted = true;

				// Draw points
				int size = path.size();
				ICell curCell = null;
				ICell prevCell = null;
				ICell lastCell = path.get(path.size() - 1);
				for (int i = 0; i < size; i++) {
					curCell = path.get(i);
					/*
					 * points.add(curCell.getCenter()); if(true) continue;
					 */
					if (prevCell == null) {
						IPoint refPoint = isInverted ? wire.getTargetPoint()
								: wire.getSourcePoint();
						IPoint center = curCell.getCenter();
						int index = isInverted ? wire.getTargetSideIndex()
								: wire.getSourceSideIndex();
						if (index == Compass.WEST) {
							points.add(new Point(center.getX(), refPoint.getY()));
						} else if (index == Compass.NORTH) {
							points.add(new Point(refPoint.getX(), center.getY()));
						} else if (index == Compass.EAST) {
							points.add(new Point(center.getX(), refPoint.getY()));
						} else if (index == Compass.SOUTH) {
							points.add(new Point(refPoint.getX(), center.getY()));
						}
					} else if (curCell == lastCell) {
						IPoint refPoint = isInverted ? wire.getSourcePoint()
								: wire.getTargetPoint();
						IPoint center = curCell.getCenter();
						int index = isInverted ? wire.getSourceSideIndex()
								: wire.getTargetSideIndex();
						if (index == Compass.WEST) {
							points.add(new Point(center.getX(), refPoint.getY()));
						} else if (index == Compass.NORTH) {
							points.add(new Point(refPoint.getX(), center.getY()));
						} else if (index == Compass.EAST) {
							points.add(new Point(center.getX(), refPoint.getY()));
						} else if (index == Compass.SOUTH) {
							points.add(new Point(refPoint.getX(), center.getY()));
						}
					} else {
						IPoint refPoint = points.get(i - 1);
						IPoint center = curCell.getCenter();
						if (curCell.getEastSide() == prevCell.getWestSide()
								&& prevCell.getWestSide() != null) {
							points.add(new Point(center.getX(), refPoint.getY()));
						} else if (curCell.getWestSide() == prevCell
								.getEastSide()
								&& prevCell.getEastSide() != null) {
							points.add(new Point(center.getX(), refPoint.getY()));
						} else if (curCell.getNorthSide() == prevCell
								.getSouthSide()
								&& prevCell.getSouthSide() != null) {
							points.add(new Point(refPoint.getX(), center.getY()));
						} else {
							points.add(new Point(refPoint.getX(), center.getY()));
						}
					}
					// cell.cleanedAfterBuildPath();
					prevCell = curCell;
				}

				// Reverse in case of inverted source and target anchors in wire
				if (isInverted)
					Collections.reverse(points);
				wire.setNeedUpdatePath();
			} else {
				wire.getPath().clear();
				wire.setNeedUpdatePath();
			}
		}
	}

	public void _process(IChannel channel, IWire wire) {
		List<ICell> path = channel.getPath();
		if (path.size() > 0) {
			List<IPoint> points = wire.getPath();
			points.clear();

			boolean isInverted = false;
			ICell pathSource = path.get(0);
			ICell wireTarget = wire.getTargetCell();
			if (pathSource.getColumn() == wireTarget.getColumn()
					&& pathSource.getRow() == wireTarget.getRow())
				isInverted = true;

			// Draw points
			int size = path.size();
			ICell curCell = null;
			ICell prevCell = null;
			ICell lastCell = path.get(path.size() - 1);
			for (int i = 0; i < size; i++) {
				curCell = path.get(i);
				/*
				 * points.add(curCell.getCenter()); if(true) continue;
				 */
				if (prevCell == null) {
					IPoint refPoint = isInverted ? wire.getTargetPoint() : wire
							.getSourcePoint();
					IPoint center = curCell.getCenter();
					int index = isInverted ? wire.getTargetSideIndex() : wire
							.getSourceSideIndex();
					if (index == Compass.WEST) {
						points.add(new Point(center.getX(), refPoint.getY()));
					} else if (index == Compass.NORTH) {
						points.add(new Point(refPoint.getX(), center.getY()));
					} else if (index == Compass.EAST) {
						points.add(new Point(center.getX(), refPoint.getY()));
					} else if (index == Compass.SOUTH) {
						points.add(new Point(refPoint.getX(), center.getY()));
					}
				} else if (curCell == lastCell) {
					IPoint refPoint = isInverted ? wire.getSourcePoint() : wire
							.getTargetPoint();
					IPoint center = curCell.getCenter();
					int index = isInverted ? wire.getSourceSideIndex() : wire
							.getTargetSideIndex();
					if (index == Compass.WEST) {
						points.add(new Point(center.getX(), refPoint.getY()));
					} else if (index == Compass.NORTH) {
						points.add(new Point(refPoint.getX(), center.getY()));
					} else if (index == Compass.EAST) {
						points.add(new Point(center.getX(), refPoint.getY()));
					} else if (index == Compass.SOUTH) {
						points.add(new Point(refPoint.getX(), center.getY()));
					}
				} else {
					IPoint refPoint = points.get(i - 1);
					IPoint center = curCell.getCenter();
					if (curCell.getEastSide() == prevCell.getWestSide()
							&& prevCell.getWestSide() != null) {
						points.add(new Point(center.getX(), refPoint.getY()));
					} else if (curCell.getWestSide() == prevCell.getEastSide()
							&& prevCell.getEastSide() != null) {
						points.add(new Point(center.getX(), refPoint.getY()));
					} else if (curCell.getNorthSide() == prevCell
							.getSouthSide() && prevCell.getSouthSide() != null) {
						points.add(new Point(refPoint.getX(), center.getY()));
					} else {
						points.add(new Point(refPoint.getX(), center.getY()));
					}
				}
				// cell.cleanedAfterBuildPath();
				prevCell = curCell;
			}

			// Reverse in case of inverted source and target anchors in wire
			if (isInverted)
				Collections.reverse(points);
			wire.setNeedUpdatePath();
		}
	}

}
