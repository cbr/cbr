package cbr.pt.frontend;

import javax.swing.Action;
import javax.swing.JButton;
import javax.swing.border.EmptyBorder;

public class CBRButton extends JButton {

	private static final long serialVersionUID = 1L;

	public CBRButton(Action act, String tip) {
		super(act);
		setText(null);
		setBorder(new EmptyBorder(2, 2, 2, 2));
		setToolTipText(tip);
		if (act instanceof CBRAction) {
			setDisabledIcon(((CBRAction) act).getDisabledIcon());
		}
	}

}
