package cbr.pt.frontend.console;

import cbr.serialize.xml.annotations.XMLNode;

@XMLNode("identifiedView")
public interface IIdentifiedView extends IAreaView, IIdentified {

}
